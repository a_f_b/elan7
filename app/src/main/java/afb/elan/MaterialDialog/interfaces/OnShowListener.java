package afb.elan.MaterialDialog.interfaces;

import dev.shreyaspatil.MaterialDialog.interfaces.DialogInterface;

public interface OnShowListener {
    void onShow(DialogInterface dialogInterface);
}
