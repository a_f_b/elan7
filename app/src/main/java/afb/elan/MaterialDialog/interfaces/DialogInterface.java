package afb.elan.MaterialDialog.interfaces;

public interface DialogInterface {
    void cancel();
    void dismiss();
}
