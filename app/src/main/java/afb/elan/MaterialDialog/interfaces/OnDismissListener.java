package afb.elan.MaterialDialog.interfaces;

import dev.shreyaspatil.MaterialDialog.interfaces.DialogInterface;

public interface OnDismissListener {
    void onDismiss(DialogInterface dialogInterface);
}
