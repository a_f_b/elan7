package afb.elan.MaterialDialog;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.RawRes;
import androidx.appcompat.app.AlertDialog;

import dev.shreyaspatil.MaterialDialog.AbstractDialog;
import dev.shreyaspatil.MaterialDialog.model.DialogButton;
import dev.shreyaspatil.MaterialDialog.model.DialogMessage;
import dev.shreyaspatil.MaterialDialog.model.DialogTitle;

/**
 * Creates a Material Dialog with 2 buttons.
 * <p>
 * Use {@link Builder} to create a new instance.
 */
@SuppressWarnings("unused")
public final class CompatDialog extends afb.elan.MaterialDialog.AbstractCompatDialog {

    private CompatDialog(@NonNull final Activity mActivity,
                           @NonNull DialogTitle title,
                           @NonNull DialogMessage message,
                           boolean mCancelable,
                           @NonNull DialogButton mPositiveButton,
                           @NonNull DialogButton mNegativeButton,
                           @RawRes int mAnimationResId,
                           @NonNull String mAnimationFile) {
        super(mActivity, title, message, mCancelable, mPositiveButton, mNegativeButton, mAnimationResId, mAnimationFile);

        // Init Dialog
        final AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);

        LayoutInflater inflater = mActivity.getLayoutInflater();

        View dialogView = createView(inflater, null);

        builder.setView(dialogView);

        // Set Cancelable property
        builder.setCancelable(mCancelable);

        // Create and show dialog
        mDialog = builder.create();
    }

    /**
     * Builder for {@link MaterialDialog}.
     */
    public static class Builder extends AbstractCompatDialog.Builder<CompatDialog> {
        /**
         * @param activity where Material Dialog is to be built.
         */
        public Builder(@NonNull Activity activity) {
            super(activity);
        }

        /**
         * Builds the {@link MaterialDialog}.
         */
        @NonNull
        @Override
        public CompatDialog build() {
            return new CompatDialog(
                    activity,
                    title,
                    message,
                    isCancelable,
                    positiveButton,
                    negativeButton,
                    animationResId,
                    animationFile
            );
        }
    }
}

