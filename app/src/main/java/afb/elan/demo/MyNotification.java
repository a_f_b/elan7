package afb.elan.demo;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;

import android.net.Uri;
import android.view.View;
import android.widget.RemoteViews;

import afb.elan.demo.Db.SQLHelper;

public class MyNotification {
    Context mainInstance;

    public MyNotification(Context _context){
        mainInstance=_context;

    }

    private NotificationCompat.Builder createNotificationBuilder(SQLHelper.SearchNode sn)  {
        // Layouts for the custom notification
        RemoteViews notificationLayoutCollapsed = new RemoteViews(mainInstance.getPackageName(), R.layout.notification_collapsed);
        notificationLayoutCollapsed.setTextViewText(R.id.collapsed_notification_title,sn.title);
        notificationLayoutCollapsed.setTextViewText(R.id.collapsed_notification_info,sn.date);

        RemoteViews notificationLayoutExpanded = new RemoteViews(mainInstance.getPackageName(), R.layout.notification_expanded);
        notificationLayoutExpanded.setTextViewText(R.id.expanded_notification_title,sn.title);
        notificationLayoutExpanded.setTextViewText(R.id.expanded_notification_info,sn.date);
        Intent browserIntent=null;
        int iconRes=R.drawable.ic_launcher;
        if (sn.type== SQLHelper.SearchNode.TYPE_NOTE){
            notificationLayoutExpanded.setViewVisibility(R.id.image_view_expanded, View.GONE);
            notificationLayoutExpanded.setTextViewText(R.id.expanded_notification_content,sn.sms);
            browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("elanapp://afb.elan.demo/note/?id="+sn.id));
            iconRes=R.drawable.widget_note_icon;
        }
        else //if (sn.type== SQLHelper.SearchNode.TYPE_DATE)
        {
            notificationLayoutExpanded.setViewVisibility(R.id.image_view_expanded,sn.sms.isEmpty()?View.GONE:View.VISIBLE);
            browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("elanapp://afb.elan.demo/date/?id="+sn.id));
            iconRes=R.drawable.widget_date_icon;
        }
        browserIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        if (sn.image != null) {
            notificationLayoutExpanded.setImageViewBitmap(R.id.image_view_expanded,BitmapFactory
                    .decodeByteArray(sn.image, 0, sn.image.length));
        } else {
            int res=R.drawable.blank_user;
            if (sn.type== SQLHelper.SearchNode.TYPE_NOTE)
            notificationLayoutExpanded.setImageViewResource(R.id.image_view_expanded,res);
        }

        PendingIntent pendingIntent = PendingIntent.getActivity(mainInstance, 0, browserIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        // Apply the layouts to the notification builder
        NotificationCompat.Builder mNotifyBuilder = new NotificationCompat.Builder(mainInstance, "afb.elan.demo")
                .setSmallIcon(iconRes)
                .setColor(ContextCompat.getColor(mainInstance, R.color.black))
                .setStyle(new NotificationCompat.DecoratedCustomViewStyle())
                .setCustomContentView(notificationLayoutCollapsed)
                .setCustomBigContentView(notificationLayoutExpanded)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);
        mNotifyBuilder.build().flags |= Notification.FLAG_AUTO_CANCEL;
        return mNotifyBuilder;
    }

    public void showNotification(SQLHelper.SearchNode sn,int _uniqueNotificationId) {
        NotificationCompat.Builder notification = createNotificationBuilder(sn);

        // Show the notification with notificationId.
        int uniqueNotificationId = _uniqueNotificationId;
        NotificationManagerCompat notificationmanager = NotificationManagerCompat.from(mainInstance);
        // Build Notification with Notification Manager
        notificationmanager.notify(uniqueNotificationId, notification.build());

    }

/*
    private void updateRemoteViews(RemoteViews remoteView) {

        if (currentMusic != null) {
            remoteView.setTextViewText(R.id.lblWidgetCurrentMusicName, "Music name");
            remoteView.setTextViewText(R.id.lblWidgetCurrentArtistName, "Music Artist");
        }
        remoteView.setImageViewResource(R.id.btnWidgetPlayPauseMusic, isMusicPlaying == true ? R.drawable.vector_pause : R.drawable.vector_play);

        if(theMusicApplication.global_programIsReady)
        {
            Bitmap album = (Bitmap)your_image;
            if (album == null) {
                remoteView.setImageViewResource(R.id.imgWidgetAlbumArt, R.mipmap.ic_launcher);
            } else {
                remoteView.setImageViewBitmap(R.id.imgWidgetAlbumArt, album);
            }
        }
    }
    private void setUpRemoteView(RemoteViews remoteView) {

        Intent closeIntent = new Intent(mainInstance, MusicPlayerService.class);
        closeIntent.setAction(ACTION_STOP_SERVICE);
        PendingIntent pcloseIntent = PendingIntent.getService(mainInstance, 0, closeIntent, 0);

        Intent playNextIntent = new Intent(mainInstance, MusicPlayerService.class);
        playNextIntent.setAction(ACTION_PLAY_NEXT);
        PendingIntent pNextIntent = PendingIntent.getService(mainInstance, 0, playNextIntent, 0);

        Intent playPrevIntent = new Intent(mainInstance, MusicPlayerService.class);
        playPrevIntent.setAction(ACTION_PLAY_LAST);
        PendingIntent pPrevIntent = PendingIntent.getService(mainInstance, 0, playPrevIntent, 0);

        Intent playPauseIntent = new Intent(mainInstance, MusicPlayerService.class);
        playPauseIntent.setAction(ACTION_PLAY_PAUSE);
        PendingIntent pplayPauseIntent = PendingIntent.getService(mainInstance, 0, playPauseIntent, 0);

        remoteView.setImageViewResource(R.id.btnWidgetCloseService, R.drawable.vector_close);
        remoteView.setImageViewResource(R.id.btnWidgetPlayPrevious, R.drawable.vector_previous);
        remoteView.setImageViewResource(R.id.btnWidgetPlayNext, R.drawable.vector_next);

        remoteView.setOnClickPendingIntent(R.id.btnWidgetCloseService, pcloseIntent);
        remoteView.setOnClickPendingIntent(R.id.btnWidgetPlayPrevious, pPrevIntent);
        remoteView.setOnClickPendingIntent(R.id.btnWidgetPlayNext, pNextIntent);
        remoteView.setOnClickPendingIntent(R.id.btnWidgetPlayPauseMusic, pplayPauseIntent);
    }*/
}
