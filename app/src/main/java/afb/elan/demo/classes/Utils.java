package afb.elan.demo.classes;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings.Secure;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.PopupMenu;


import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import afb.elan.demo.LoadingDialog;
import afb.elan.demo.R;
import afb.elan.demo.calendar.SolarDate;


public class Utils {

	/*public static String getProvinceName(Context ctx,int prov){
		int val=0;
		String provs[]=ctx.getResources().getStringArray(R.array.province_names);
		return provs[prov];
	}

	public static int getProvinceCode(Context ctx,String prov){
		int val=0;
		String provs[]=ctx.getResources().getStringArray(R.array.province_names);
		for (int i=0;i<provs.length;i++)
			if (provs[i].equalsIgnoreCase(prov))
			{
				val=i;
				break;
			}
		return val;
	}

	private static final LatLng BOUNDS_TEHRAN = new LatLng(35.7018797, 51.3376817);

	public static LatLngBounds getLatLngBounds(LatLng pos) {
		// TODO Auto-generated method stub
		if (pos==null)
			return new LatLngBounds(new LatLng(BOUNDS_TEHRAN.latitude-0.01, BOUNDS_TEHRAN.longitude-0.01),
					new LatLng(BOUNDS_TEHRAN.latitude+0.01, BOUNDS_TEHRAN.longitude+0.01));

		return new LatLngBounds(new LatLng(pos.latitude-0.01, pos.longitude-0.01),
				new LatLng(pos.latitude+0.01, pos.longitude+0.01));
	}
*/
	public static void CopyStream(InputStream is, OutputStream os)
    {
        final int buffer_size=1024;
        try
        {
            byte[] bytes=new byte[buffer_size];
            for(;;)
            {
              int count=is.read(bytes, 0, buffer_size);
              if(count==-1)
                  break;
              os.write(bytes, 0, count);
            }
        }
        catch(Exception ex){}
    }
    
    public static Bitmap getRoundedCroppedBitmap(Bitmap bitmap, int radius,int frameStroke, int shadowStroke,int frameColor) {
    	Bitmap finalBitmap;
        // if (bitmap.getWidth() != radius-frameStroke-shadowStroke || bitmap.getHeight() != radius-frameStroke-shadowStroke)
                finalBitmap = Bitmap.createScaledBitmap(bitmap, radius, radius,true);
        // else
          //      finalBitmap = bitmap;
         Bitmap output = Bitmap.createBitmap(radius,
                      radius, Config.ARGB_8888);
         Canvas canvas = new Canvas(output);

         final Paint paint = new Paint();
         int delta=(frameStroke+shadowStroke)/2;
         final Rect rect = new Rect(delta, delta, radius-delta,
                      radius-delta);
         
         
         paint.setAntiAlias(true);
         paint.setFilterBitmap(true);
         paint.setDither(true);
         canvas.drawARGB(0, 0, 0, 0);

         paint.setColor(Color.parseColor("#BAB399"));
         /*canvas.drawCircle(output.getWidth() / 2 + 0.7f,
                 output.getHeight() / 2 + 0.7f,
                 output.getWidth() / 2-frameStroke-shadowStroke + 0.1f, paint);*/
         canvas.drawCircle((rect.left+rect.right)/2 + 0.7f,
       		  (rect.left+rect.right) / 2 + 0.7f,
       		  (rect.right-rect.left)/2-shadowStroke + 0.1f, paint);
         paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
         canvas.drawBitmap(finalBitmap, rect, rect, paint);
         
         
         paint.setXfermode(null);                                                
         paint.setStyle(Style.STROKE);                                           
         paint.setColor(frameColor);                                            
         paint.setStrokeWidth(frameStroke);
         paint.setShadowLayer(shadowStroke, 0f, 0f, Color.BLACK);
         canvas.drawCircle(output.getWidth() / 2 + 0.7f,
                 output.getHeight() / 2 + 0.7f,
                 output.getWidth() / 2 -frameStroke-shadowStroke+ 0.1f, paint);             
         
         return output;
 }


	public static String getCurrentDate(){
		SolarDate sd = SolarDate.newInstance(SolarDate.DATE_KIND_SOLAR);
		return sd.toString();
	}



	public static Dialog prepareLoadingDialog(Context ctx, boolean animation){

		   LoadingDialog ld=new LoadingDialog(ctx, animation);
		   
		   WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		   Window window = ld.getWindow();
		   lp.copyFrom(window.getAttributes());
		   lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
		   lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
		   window.setAttributes(lp);
		   return ld;				
	}

	public static boolean isValidEmail(CharSequence target) {
		return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
	}
	public static boolean isValidEmail(String target) {
		String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
		return (target.matches(emailPattern) && target.length() > 0);
	}

	/*public static Dialog prepareLoadingDialog(Context ctx, String title, boolean animation){

		LoadingDialog ld=new LoadingDialog(ctx, title, animation);

		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		Window window = ld.getWindow();
		lp.copyFrom(window.getAttributes());
		lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
		lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
		window.setAttributes(lp);
		return ld;
	}


	public static Dialog prepareLoadingDialog(Context ctx, int theme, boolean animation){

		   LoadingDialog ld=new LoadingDialog(ctx, theme, animation);
		   
		   WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		   Window window = ld.getWindow();
		   lp.copyFrom(window.getAttributes());
		   lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
		   lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
		   window.setAttributes(lp);
		   return ld;				
	}*/

		public static View prepareLoadingLayout(Context ctx){

		LayoutInflater factory = LayoutInflater.from(ctx);
		View title= factory.inflate(R.layout.loading_layout, null);
		//DoubleBounce doubleBounce = new DoubleBounce();
		//doubleBounce.setBounds(0, 0, 100, 100);
		//doubleBounce.setColor(ctx.getResources().getColor(R.color.colorAccent));
		//((ProgressBar)title.findViewById(R.id.progressBar1)).setIndeterminateDrawable(doubleBounce);

		title.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT));
		return title;

	}

	public static boolean hasActiveInternetConnection(Context context) {
		ConnectivityManager cm =
				(ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		return netInfo != null && netInfo.isConnectedOrConnecting();
	}
/*
	public static View prepareLoadingLayout(Context ctx,int backColor, View waitLayout){
		LinearLayout ll=(LinearLayout) waitLayout.findViewById(R.id.wait);
		ll.setBackgroundColor(backColor);
		ImageView iv = (ImageView) waitLayout.findViewById(R.id.imageView1);
		iv.startAnimation(AnimationUtils.loadAnimation(ctx,
				R.anim.rotate_indefinitely)); 
		return ll;
	}	
	

	public static boolean checkYearValidation(int year){
		Calendar c = Calendar.getInstance(); 
		int y = c.get(Calendar.YEAR);
		if (year<1000 || year>y) return false;
		return true;
	}
	
	public static int getScorePercent(String score){
		float scoref=Float.valueOf(score).floatValue();
		int val=(int) (scoref*20);
		return val;
	}	
	
	public static int getEducationPercent(Context ctx,String edu){
		int val=0;
		String edus[]=ctx.getResources().getStringArray(R.array.educations);
		for (int i=0;i<edus.length;i++)
		  if (edus[i].equalsIgnoreCase(edu))
		  {
			  val=i;
			  break;
		  }
		val=val*100 / (edus.length-1);
		return val;
	}
	
	public static int getEducationVal(Context ctx,String edu){
		int val=0;
		String edus[]=ctx.getResources().getStringArray(R.array.educations);
		for (int i=0;i<edus.length;i++)
		  if (edus[i].equalsIgnoreCase(edu))
		  {
			  val=i;
		  }
		return val;
	}	
	
	public static String getEducationName(Context ctx,int educationVal){
		if (educationVal<0) return ctx.getResources().getString(R.string.not_defined);
		String edus[]=ctx.getResources().getStringArray(R.array.educations);

		return edus[educationVal];
	}	
     
	

	
	private static boolean isNetworkAvailable(Context context) {
	    ConnectivityManager connectivityManager 
	         = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	    return activeNetworkInfo != null;
	}
	
	public static AlertDialog displayNoNetwork(Context context)
    {
        final AlertDialog.Builder loginDialog = new AlertDialog.Builder(new ContextThemeWrapper(context, android.R.style.Theme_DeviceDefault_Light_Dialog));
                    LayoutInflater factory = LayoutInflater.from(context);
                    final View f = factory.inflate(R.layout.no_network, null);
 
                    loginDialog.setTitle("No Network");
                    loginDialog.setView(f);
 
                    TextView tv1 = (TextView)f.findViewById(R.id.tv1);
                    tv1.setText("there's no internet access");
                    return loginDialog.create();
    }
	
	*/
	public static Bitmap downloadImage(String url) {
		Bitmap bitmap = null;
		InputStream stream = null;
		BitmapFactory.Options bmOptions = new BitmapFactory.Options();
		bmOptions.inSampleSize = 1;

		try {
			stream = getHttpConnection(url);
			bitmap = BitmapFactory.
					decodeStream(stream, null, bmOptions);
			if (stream!=null) stream.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		return bitmap;
	}

	// Makes HttpURLConnection and returns InputStream

	public static InputStream getHttpConnection(String urlString){
		InputStream stream = null;
		try {
			
			URL url = new URL(urlString);
			URLConnection connection = url.openConnection();
			HttpURLConnection httpConnection = (HttpURLConnection) connection;
			httpConnection.setRequestMethod("GET");
			httpConnection.connect();

			if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
				stream = httpConnection.getInputStream();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return stream;
	}
	
	private static boolean checkIfLoggined(Context context){
		SharedPreferences someData = context.getSharedPreferences("loginData", 0);
		String email = someData.getString("email",null);
		String key = someData.getString("key",null);
		if (key==null) return false;

		return true;
		
		

	}
	
	
	public static boolean isValidEmailAddress(String address){
		  String parts[]=address.split("@");
		  if (parts.length!=2) return false;
		  int c=0;
		  for (int i=0;i<parts[1].length();i++)
			  if (parts[1].charAt(i)=='.'){
				  if ((i==0)|| (i==parts[1].length()-1)) 
					  return false;
				  else
					  c++;
			  }
		  if (c>1) return false;
		  if (address.contains(" ")) return false;
		  if (address.contains("..")) return false;
		  return true;
		}

	public static boolean ifUpdateNeeded(Context context,String name){
		SharedPreferences someData = context.getSharedPreferences("loginData", 0);
		return (someData.getInt(name,0)>0);

	}
	public static void setUpdateFieldsNeeded(Context context, boolean need,String name){
		SharedPreferences someData = context.getSharedPreferences("loginData", 0);
		SharedPreferences.Editor editor = someData.edit();
		editor.putInt(name,(need)?1:0);
		editor.commit();

	}



	public static int getOwnId(Context context){
		SharedPreferences someData = context.getSharedPreferences("loginData", 0);
		return someData.getInt("session_expert_id",0);

	}

	public static boolean isAdminLogined(Context context){
		SharedPreferences someData = context.getSharedPreferences("loginData", 0);
		int a= someData.getInt("session_expert_id",0);
		return ((a>0) && (a<21));

	}


	public static String getOwnMobile(Context context){
		SharedPreferences someData = context.getSharedPreferences("loginData", 0);
		return someData.getString("session_mobile","");

	}

	public static String getOwnImage(Context context){
		SharedPreferences someData = context.getSharedPreferences("loginData", 0);
		return someData.getString("expert_image","blank_user.png");

	}

	public static String getOwnName(Context context){
		SharedPreferences someData = context.getSharedPreferences("loginData", 0);
		return someData.getString("expert_name","کاربر مهمان");

	}

	public static String getOwnLastErrorCode(Context context){
		SharedPreferences someData = context.getSharedPreferences("loginData", 0);
		String pass = someData.getString("last_error_codes","");
	    if (pass != null && pass.length() > 3) {
				pass = pass.substring(0, pass.length() - 3);
			}
		return pass;

	}

	public static String getOwnLoginKey(Context context){
		SharedPreferences someData = context.getSharedPreferences("loginData", 0);
		return someData.getString("session_key","");
	}


	public static int getProvince(Context context){
		SharedPreferences someData = context.getSharedPreferences("generalData", 0);
		return someData.getInt("province_id",0);

	}
	public static String getProvinceName(Context context){
		SharedPreferences someData = context.getSharedPreferences("generalData", 0);
		return someData.getString("province_name","همه استانها");

	}
	public static int getCity(Context context){
		SharedPreferences someData = context.getSharedPreferences("generalData", 0);
		return someData.getInt("city_id",1000);

	}
	public static String getCityName(Context context){
		SharedPreferences someData = context.getSharedPreferences("generalData", 0);
		return someData.getString("city_name","همه شهرها");

	}

	public static String getOwnDeviceId(Context context){
		return Secure.getString(context.getContentResolver(),
                Secure.ANDROID_ID);

	}

	public static String readAsset(Context ctx,String asset){
		String text="";
		try {
			InputStream is = ctx.getAssets().open(asset);

			// We guarantee that the available method returns the total
			// size of the asset...  of course, this does mean that a single
			// asset can't be more than 2 gigs.
			int size = is.available();

			// Read the entire asset into a local byte buffer.
			byte[] buffer = new byte[size];
			is.read(buffer);
			is.close();

			// Convert the buffer into a string.
			text = new String(buffer);

			// Finally stick the string into the text view.
			// Replace with whatever you need to have the text into.

		} catch (IOException e) {
			// Should never happen!
			throw new RuntimeException(e);
		}
		return text;
	}
	
	/*
    private static final LatLng BOUNDS_TEHRAN = new LatLng(35.7018797, 51.3376817);
	
	public static LatLngBounds getLatLngBounds(LatLng pos) {
		// TODO Auto-generated method stub
		if (pos==null) 
			return new LatLngBounds(new LatLng(BOUNDS_TEHRAN.latitude-0.01, BOUNDS_TEHRAN.longitude-0.01), 
					new LatLng(BOUNDS_TEHRAN.latitude+0.01, BOUNDS_TEHRAN.longitude+0.01));
		
		return new LatLngBounds(new LatLng(pos.latitude-0.01, pos.longitude-0.01), 
								new LatLng(pos.latitude+0.01, pos.longitude+0.01));
	}
	
	
	public static void MakeToast(Context ctx, int img_res_id,String msg,int length, int gravity){
		LayoutInflater li=(LayoutInflater)
                ctx.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		View customview=li.inflate(R.layout.customtoast, null);
		ImageView iv2=(ImageView)customview.findViewById(R.id.tvImageToast);
		TextView tvbody=(TextView)customview.findViewById(R.id.tvTextToast);
		//tvbody.setTypeface();
		if (img_res_id>0){			
			iv2.setImageResource(img_res_id);
			iv2.setVisibility(View.VISIBLE);
		}
		tvbody.setText(msg);
		Toast toast3 = new Toast(ctx);
		toast3.setGravity(gravity,0,0);
		toast3.setDuration(length);
		toast3.setView(customview);
		toast3.show();
	}
	
	public static Bitmap scaleDown(Bitmap realImage, float maxImageSize,
	        boolean filter) {
	    float ratio = Math.min(
	            (float) maxImageSize / realImage.getWidth(),
	            (float) maxImageSize / realImage.getHeight());
	    int width = Math.round((float) ratio * realImage.getWidth());
	    int height = Math.round((float) ratio * realImage.getHeight());

	    Bitmap newBitmap = Bitmap.createScaledBitmap(realImage, width,
	            height, filter);
	    return newBitmap;
	}
	
	public static String myDecimalFormat(String str){
    	StringTokenizer st=new StringTokenizer(str, ".");
    	String s_decimal=str;
    	String s_float="";
    	
    	if (st.countTokens()>1)
    	{
    		s_decimal=st.nextToken();
    		s_float=st.nextToken();
    	}
    	String s_output="";
    	int c=0;
    	int pointFlag=s_decimal.length()-1;
    	if (s_decimal.charAt(s_decimal.length()-1)=='.'){
    		pointFlag--;
    		s_output=".";
    	}
    	for (int i=pointFlag;i>=0;i--){
    		if (c==3) {
    			s_output=","+s_output;
    			c=0;
    		}
    		s_output=s_decimal.charAt(i)+s_output;
    		c++;
    	}
    	if (s_float.length()>0) s_output=s_output+"."+s_float;
    	return s_output;
    }
    */
    public static String myDecimalFormat(Double d){
    	String s=Double.valueOf(d).toString();
    	//String parts[]=s.split(".");
    	String s2="";
    	int c=0;
    	for (int i=s.length()-1;i>=0;i--){
    		if (c==3) {
    			s2=","+s2;
    			c=0;
    		}
    		s2=s.charAt(i)+s2;
    		c++;
    	}
    	//if (parts.length>1) s2=s2+"."+parts[1];
    	return s2;
    }
	
	public static String encryptKey(String _key){
		return _key;
	}
	
	public static String decryptKey(String _key){
		return _key;
	}
	
	public static boolean isOdd( int val ) { return (val & 0x01) != 0; }

	
	   public static void setForceShowIcon(PopupMenu popupMenu) {
		    try {
		        Field[] fields = popupMenu.getClass().getDeclaredFields();
		        for (Field field : fields) {
		            if ("mPopup".equals(field.getName())) {
		                field.setAccessible(true);
		                Object menuPopupHelper = field.get(popupMenu);
		                Class<?> classPopupHelper = Class.forName(menuPopupHelper
		                        .getClass().getName());
		                Method setForceIcons = classPopupHelper.getMethod(
		                        "setForceShowIcon", boolean.class);
		                setForceIcons.invoke(menuPopupHelper, true);
		                break;
		            }
		        }
		    } catch (Throwable e) {
		        e.printStackTrace();
		    }
		}

/*	public static GuideStatus getGuideStatus(Context context) {
		SharedPreferences someData = context.getSharedPreferences("guide", 0);
		GuideStatus gs=new GuideStatus();
		gs.navigationDrawer=someData.getBoolean("navigationDrawer",false);
		gs.profile=someData.getBoolean("profile",false);
		gs.tabbedActivity=someData.getBoolean("tabbedActivity",false);
		gs.imagesHint=someData.getBoolean("imagesHint",false);

		return gs;

	}

	public static String getForgetConfirmationNumber(Context context){

    	String temp=getOwnLastErrorCode(context);
        //Log.e("getOwnLastErrorCode",temp);
		try {

			//pass = je.encrypt(etPassword.getText().toString(), "AES");
			AesCipher encrypted=new AesCipher();
			temp=encrypted.decrypt(temp);
            //Log.e("encrypted",temp);

		} catch (Exception e) {
			e.printStackTrace();
		}
    	double i=Double.parseDouble(temp);
    	i= (Math.floor(i / 3)+1122454)*2;
        //Log.e("i",i+"");

        return i+"";


	};*/

	public static void  setLocked(ImageView v)
	{
		ColorMatrix matrix = new ColorMatrix();
		matrix.setSaturation(0);  //0 means grayscale
		ColorMatrixColorFilter cf = new ColorMatrixColorFilter(matrix);
		v.setColorFilter(cf);
		// v.setImageAlpha(128);   // 128 = 0.5
	}
	public static void  setUnlocked(ImageView v)
	{
		ColorMatrix matrix = new ColorMatrix();
		matrix.setSaturation(0);  //0 means grayscale
		ColorMatrixColorFilter cf = new ColorMatrixColorFilter(matrix);
		v.clearColorFilter();
		// v.setImageAlpha(128);   // 128 = 0.5
	}

	/*public static String objectToString(Serializable obj) {
		String serializedObject = null;
		try {
			ByteArrayOutputStream bo = new ByteArrayOutputStream();
			ObjectOutputStream so = new ObjectOutputStream(bo);
			so.writeObject(obj);
			so.flush();
			serializedObject = bo.toString();

		} catch (Exception e) {
			System.out.println(e);
		}
		return serializedObject;
	}

	public static Object stringToObject(String str) {
		try {
			byte b[] = str.getBytes();
			ByteArrayInputStream bi = new ByteArrayInputStream(b);
			ObjectInputStream si = new ObjectInputStream(bi);
			return si.readObject();
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}
	}*/

}