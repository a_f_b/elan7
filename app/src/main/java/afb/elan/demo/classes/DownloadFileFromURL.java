package afb.elan.demo.classes;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import afb.elan.demo.Db.SQLHelper;
import afb.elan.demo.MainActivity;
import afb.elan.demo.R;

public class DownloadFileFromURL {

    Context context;
    String url="";
    boolean abort=false;
    AsyncDownloader myTask=null;

    private onTaskCompleteListener	mListener=null;
    public interface onTaskCompleteListener {
        public void onTaskComplete(String result);
    }
    public void setonTaskCompleteListener(onTaskCompleteListener listener){
        mListener = listener;
    }


    public DownloadFileFromURL(Context _context,String _url,boolean _abort){
        context=_context;
        url=URLs.backup_files_path+_url;
        abort=_abort;
    }

    public void execute(){
        myTask= new AsyncDownloader(url,abort);
        myTask.execute("");

    }


    class AsyncDownloader  extends AsyncTask<String, String, String> {
        public String _url="";
        public String localPatch="";
        public boolean _abort=false;

        public AsyncDownloader(String url,boolean abort){
            _url= url;
            _abort=abort;
            Log.e("file: ", _url);
        }

        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         * */
        private ProgressDialog pDialog;
        boolean canceled=false;
        OutputStream output=null;
        InputStream input=null;



        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(context);
            pDialog.setMessage(context.getString(R.string.downloading));
            //pDialog.setIndeterminate(true);
            pDialog.setMax(100);
            pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            pDialog.setCancelable(true);
            pDialog.show();
            pDialog.setCancelable(true);
            pDialog.setOnCancelListener(new DialogInterface.OnCancelListener(){
                public void onCancel(DialogInterface dialog) {
                    if (output!=null){
                        canceled=true;
                        myTask.cancel(true);

                        // closing streams

                    }
                    //finish();
                }
            });
        }

        /**
         * Downloading file in background thread
         * */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                PackageManager m = context.getPackageManager();
                String s = context.getPackageName();
                PackageInfo p;

                p = m.getPackageInfo(s, 0);
                s = p.applicationInfo.dataDir;
                localPatch=s + "/databases";
                // Toast.makeText(Offline_map_project.this, s, 1000).show();
                File ftest = new File(localPatch);

                if (!ftest.exists()) {
                    ftest.mkdir();
                }

                File file = new File(localPatch , SQLHelper.DB_NAME+".tmp");

                //Log.e("file ", file.getName());
                //ConnectionDetector cd=new ConnectionDetector(context);
                URL url = new URL(_url);
                URLConnection conection = url.openConnection();
                conection.connect();
                // getting file length
                int lenghtOfFile = conection.getContentLength();
                // input stream to read file - with 8k buffer
                input = new BufferedInputStream(url.openStream(), 8192);

                // Output stream to write file


                output = context.openFileOutput(file.getName(), Context.MODE_PRIVATE);

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    //if (canceled) break;
                    publishProgress(""+(int)((total*100)/lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                    if (canceled) break;
                    //Log.e("task", "running");
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
                myTask.cancel(true);
            }

            return null;
        }

        /**
         * Updating progress bar
         * */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            pDialog.setProgress(Integer.parseInt(progress[0]));
        }

        /**
         * After completing background task
         * Dismiss the progress dialog
         * **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            pDialog.dismiss();
            File file = new File(context.getFilesDir(), SQLHelper.DB_NAME+".tmp");
            if (!_abort){
                File renameTo = new File(localPatch, SQLHelper.DB_NAME);
                if (renameTo.exists()) renameTo.delete();
                file.renameTo(renameTo);
                if (mListener!=null) mListener.onTaskComplete(renameTo.getName());



            }
            else
                try
                {
                    file.delete();
                }catch(RuntimeException e){
                }catch(Exception e){
                }

        }



    }

}









