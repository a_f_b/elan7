package afb.elan.demo.classes;

public class Variables {
    private int shopEnabaled = 0;   //1 enable, 0 disable

    public final static int connectionTimeoutMilliseccond = 10000;
    public final static int socketTimeoutMilliseccond = 10000;


    public final static boolean needDBUpdate = true;

    public Variables(int shopEnable){
        shopEnabaled=shopEnable;
    }

    public Variables(boolean shopEnable){
        int temp=(shopEnable)?1:0;
        shopEnabaled=temp;
    }


    public Variables(){

    }



    public boolean isShopEnabled(){
        return  (shopEnabaled==1);
    }




}
