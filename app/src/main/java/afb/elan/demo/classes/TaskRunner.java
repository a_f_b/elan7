package afb.elan.demo.classes;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;

import org.apache.http.NameValuePair;

import java.util.List;

public class TaskRunner extends AsyncTask<Void, Void, Void>{
	
	String url=null;
	String webResult;
	List<NameValuePair> params=null;
	int method=ServiceHandler.POST;
	ProgressDialog pd;
	Context context=null;
	boolean canceled=false;
	String message=null;
	private boolean cancelable=true;
	
	private onTaskCompleteListener	mListener=null;



	public interface onTaskCompleteListener {
		public void onTaskComplete(String result);
	}

	private doTask	mTask=null;

	public interface doTask {
		public void doTask();
	}	
	
	/////////////////////////////////////////////////////////////////


	public TaskRunner(doTask task){
		this.url=null;
		mTask = task;
	}	
	
	public TaskRunner(String url, onTaskCompleteListener listener){
		this.url=url;
		mListener = listener;
	}
	public TaskRunner(String url, int method, List<NameValuePair> params, onTaskCompleteListener listener){
		this.url=url;
		this.params=params;
		this.method=method;
		mListener = listener;
	}	
	

	
	public void setonTaskCompleteListener(onTaskCompleteListener listener){
		mListener = listener;
	}
	
	public void setShowProgrss(Context context,String _message, boolean _cancelable){
		this.context=context;
		this.cancelable=_cancelable;
		if (_message!=null)
			message=_message;
		else
			message="Loading...";
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		if (context!=null){
			pd=new ProgressDialog(context);
			pd.setMessage(message);
			pd.show();
			pd.setCancelable(cancelable);
			if (cancelable){
				pd.setOnCancelListener(new DialogInterface.OnCancelListener() {

					@Override
					public void onCancel(DialogInterface arg0) {
						// TODO Auto-generated method stub
						cancel(true);
						canceled=true;
						//httppost.abort();
					}
				});
			}

		}

		

	}

	@Override
	protected Void doInBackground(Void... arg0) {
		// Creating service handler class instance
			// adding contact to contact list
		if (url!=null){
			ServiceHandler sh = new ServiceHandler();			
			webResult = sh.makeServiceCall(url, method, params);			
		}
		else
			mTask.doTask();


		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		if (canceled) return;
		try{
			
			if (mListener!=null) mListener.onTaskComplete(webResult);
			if (context!=null) pd.dismiss();
		}catch(Exception e){
			
		}

	}


}
