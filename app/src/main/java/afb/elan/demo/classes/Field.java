package afb.elan.demo.classes;

import androidx.annotation.NonNull;

public class Field {
    public String name;
    public int image_res_id=-1;

    public Field(String _name, int _res){
        name=_name;
        image_res_id=_res;
    }

    @NonNull
    @Override
    public String toString() {
        return name;
    }



}
