package afb.elan.demo;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import afb.elan.demo.Db.SQLHelper;

public class SearchActivity extends AppCompatActivity implements View.OnClickListener {
    RecyclerView recyclerView;
    View loading;
    Search_Adapter notesAdapter;
    GridLayoutManager mGridLayoutManager;
    LinearLayoutManager mLayoutManager;
    EditText etSearch;
    afb.elan.demo.ui.SquareImageView search;
    public List<SQLHelper.SearchNode> nodesList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_search);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar1);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_TITLE | ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_HOME_AS_UP);
        }

        search= (afb.elan.demo.ui.SquareImageView)findViewById(R.id.iv_search_icon);
        search.setOnClickListener(this);
        etSearch=(EditText)findViewById(R.id.et_search);
        nodesList=new ArrayList<SQLHelper.SearchNode>();
        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH ) {
                    // handle next button
                    hideKeyboard(etSearch);
                    search.performClick();
                    return true;
                }
                return false;
            }
        });


            recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

            notesAdapter=new Search_Adapter(this,nodesList);
            notesAdapter.setAnimationEnabled(false);
            //mAdapter.setHasStableIds(true);


            recyclerView.setHasFixedSize(true);
            //mLayoutManager = new LinearLayoutManager(SearchActivity.this, LinearLayoutManager.HORIZONTAL, false);
            // = new GridLayoutManager(SearchActivity.this,1,LinearLayoutManager.HORIZONTAL,false);
            mLayoutManager = new LinearLayoutManager(SearchActivity.this, LinearLayoutManager.VERTICAL, false);
           recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(notesAdapter);

        reLoad(VIEW_TYPE_LINEAR);
            recyclerView.addOnItemTouchListener(new RecyclerTouchListener(SearchActivity.this, recyclerView, new RecyclerTouchListener.ClickListener() {
                @Override
                public void onClick(View view, int position) {


                    if (notesAdapter.nodesList.get(position).type== SQLHelper.SearchNode.TYPE_DATE){
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("elanapp://afb.elan.demo/date/?id="+notesAdapter.nodesList.get(position).id));
                        startActivity(browserIntent);
                    }
                    else
                    if (notesAdapter.nodesList.get(position).type== SQLHelper.SearchNode.TYPE_NOTE){
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("elanapp://afb.elan.demo/note/?id="+notesAdapter.nodesList.get(position).id));
                        startActivity(browserIntent);
                    }




                }

                @Override
                public void onLongClick(View view, int position) {

				/*FlatDialog flatDialog = new FlatDialog(SearchActivity.this);
				flatDialog.setTitle("")
						.setSubtitle("شما در حال حذف این یادداشت می باشید. آیا از حذف اطمینان دارید؟")
						.setBackgroundColor(getResources().getColor(R.color.green_dark))
						//.setFirstTextFieldHint("email")
						//.setSecondTextFieldHint("password")
						.setFirstButtonText("حذف")
						.setSecondButtonText("صرف نظر")
						.withFirstButtonListner(new View.OnClickListener() {
							@Override
							public void onClick(View view) {
								new AestheticDialog.Builder(SearchActivity.this, DialogStyle.RAINBOW, DialogType.ERROR)
										.setTitle("عنوان")
										.setMessage("سلام")
										.setOnClickListener(new OnDialogClickListener() {
											@Override
											public void onClick(AestheticDialog.Builder builder) {
												builder.dismiss();
											}
										})
								.setCancelable(true)
								.setGravity(Gravity.TOP)
								//.setAnimation(DialogAnimation.FADE)
								//.setDuration(500)
								.show();
							}
						})
						.withSecondButtonListner(new View.OnClickListener() {
							@Override
							public void onClick(View view) {
								flatDialog.dismiss();
							}
						})
						.setCancelable(true);
				flatDialog.show();*/
				/*LottieDialog
						alertDialog= new LottieDialog.Builder(SearchActivity.this, DialogTypes.TYPE_QUESTION)
						.setTitle("تیتراژ")
						.setDescription("متن پیام")
						.setPositiveText("بلی")
						.setPositiveButtonColor(getResources().getColor(R.color.green))
						.setNegativeText("خیر")
						.setNegativeButtonColor(Color.parseColor("#1cd3ef"))
						.build();
				alertDialog.setCancelable(true);
				alertDialog.show();*/


                }
            }));

            recyclerView.setAdapter(notesAdapter);

            // notesAdapter = new ArrayAdapter<String>(NotesList.this,
            // android.R.layout.simple_list_item_1, menulabels);

		/*if (lang.equals("2")) {
			bAddNote.setText("یادداشت جدید");			
		}*/


        }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;


        }
        return super.onOptionsItemSelected(item);
    }


        private Menu menu=null;

        public void hideOption(int id)
        {
            MenuItem item = menu.findItem(id);
            item.setVisible(false);
        }

        public void showOption(int id)
        {
            MenuItem item = menu.findItem(id);
            item.setVisible(true);
        }

    protected void hideKeyboard(View v) {
        InputMethodManager imm = (InputMethodManager) v.getContext().getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

        /*
            @Override
            public boolean onCreateOptionsMenu(Menu menu) {
                this.menu=menu;
        
                MenuInflater inflater = getMenuInflater();
                inflater.inflate(R.menu.note_list, menu);
        
                return true;
        
            }
        */
        public final static int VIEW_TYPE_LINEAR=1;
        public final static int VIEW_TYPE_GRID=2;

        public void reLoad(int theme){
            notesAdapter.setAnimationEnabled(false);
            //recyclerView.showShimmerAdapter();

            switch (theme) {
                case VIEW_TYPE_GRID:
                    mGridLayoutManager = new GridLayoutManager(SearchActivity.this,2, LinearLayoutManager.VERTICAL,false);
                    //supportInvalidateOptionsMenu();
                    recyclerView.setLayoutManager(mGridLayoutManager);
                    //recyclerView.setAdapter(mAdapter);
                    notesAdapter.notifyDataSetChanged();

                    break;
                case VIEW_TYPE_LINEAR:
                    // recyclerView.setAdapter(mAdapter);
                    notesAdapter.notifyDataSetChanged();
                    break;

            }
            recyclerView.postDelayed(new Runnable() {
                @Override
                public void run() {
                    //recyclerView.hideShimmerAdapter();
                    //mAdapter.setAnimationEnabled(true);

                }
            },1000);
        }



        public void showLoading(){
		/*Animation anim=AnimationUtils.loadAnimation(SearchActivity.this,
				R.anim.fade_in_simple);
		anim.setAnimationListener(new Animation.AnimationListener() {

			@Override
			public void onAnimationStart(Animation arg0) {
				// TODO Auto-generated method stub
				((ImageButton) findViewById(R.id.bAddNote)).setEnabled(false);

			}

			@Override
			public void onAnimationRepeat(Animation arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationEnd(Animation arg0) {
				// TODO Auto-generated method stub
				loading.setVisibility(ViewGroup.VISIBLE);
			}
		});
		loading.startAnimation(anim);*/
        }


        public void hideLoading(){
		/*Animation anim=AnimationUtils.loadAnimation(SearchActivity.this,
				R.anim.fade_out_simple);
		anim.setAnimationListener(new Animation.AnimationListener() {

			@Override
			public void onAnimationStart(Animation arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationRepeat(Animation arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationEnd(Animation arg0) {
				// TODO Auto-generated method stub
				loading.setVisibility(ViewGroup.GONE);
				((ImageButton) findViewById(R.id.bAddNote)).setEnabled(true);
			}
		});
		loading.startAnimation(anim);*/
        }





        @Override
        public void onCreateContextMenu(ContextMenu menu, View v,
                                        ContextMenu.ContextMenuInfo menuInfo) {
            // TODO Auto-generated method stub
            super.onCreateContextMenu(menu, v, menuInfo);

            MenuInflater mi = SearchActivity.this.getMenuInflater();
            switch (v.getId()) {
		/*case R.id.lvNotes:
			if (lang.equals("2"))
				mi.inflate(R.menu.menu_context_notelistper, menu);
			else
				mi.inflate(R.menu.menu_context_notelist, menu);
			break;*/
            }

        }

        @Override
        public boolean onContextItemSelected(MenuItem item) {
            // TODO Auto-generated method stub

            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item
                    .getMenuInfo();

            switch (item.getItemId()) {

            }
            return super.onContextItemSelected(item);

        }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.iv_search_icon:
                String filter = etSearch.getText().toString();
                notesAdapter.searchByString(filter);
                break;
        }
    }

/*
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK) {

			if (requestCode == REQUEST_ADD_NOTE) {
				NoteObject no= (NoteObject) data.getSerializableExtra("note");
				no.date_created="اکنون";
				notes.add(0,no);
				notesAdapter.notifyDataSetChanged();
			}
			else
				
			if (requestCode == REQUEST_UPDATE_NOTE) {
				NoteObject no= (NoteObject) data.getSerializableExtra("note");
				notes.get(editing_pos).title=no.title;
				notes.get(editing_pos).body=no.body;
				notesAdapter.notifyDataSetChanged();
			}
		}

	}
*/







    }

