package afb.elan.demo.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.widget.RemoteViews;

import afb.elan.demo.Db.SQLHelper;
import afb.elan.demo.R;
import afb.elan.demo.calendar.SolarDate;

public class DateWidgetShow extends AppWidgetProvider{
	public static final String TOAST_ACTION = "afb.elan.demo.TOAST_ACTION";
	public static final String EXTRA_ITEM = "afb.elan.demo.EXTRA_ITEM";
	@Override
	public void onDeleted(Context context, int[] appWidgetIds) {
		// TODO Auto-generated method stub
		super.onDeleted(context, appWidgetIds);
	}



	@Override
	public void onReceive(Context context, Intent intent) {
		Log.e("onReceive()",intent.getAction());
		if (intent.getAction().equals(TOAST_ACTION)) {
			int viewType = intent.getIntExtra("type", -1);
			int id = intent.getIntExtra("id", -1);
			if (viewType== SQLHelper.SearchNode.TYPE_DATE){
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("elanapp://afb.elan.demo/date/?id="+id));
				browserIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(browserIntent);
			}
			else
			if (viewType== SQLHelper.SearchNode.TYPE_NOTE){
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("elanapp://afb.elan.demo/note/?id="+id));
				browserIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(browserIntent);
			}
		}
		super.onReceive(context, intent);
	}
	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager,
			int[] appWidgetIds) {
		// TODO Auto-generated method stub
		super.onUpdate(context, appWidgetManager, appWidgetIds);
		
		for (int i = 0; i < appWidgetIds.length; i++) {
			// int awID = appWidgetIds[appWidgetIds.length - 1];
			int awID = appWidgetIds[i];
			Intent intent = new Intent(context, WidgetAdapterService.class);
			intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetIds[i]);
			intent.setData(Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME)));
			RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.studio_widget_date);
			remoteViews.setRemoteAdapter(R.id.widgetRemindersListView, intent);
			remoteViews.setEmptyView(R.id.widgetRemindersListView, R.id.widgetEmptyViewText);
			Intent inup = new Intent(context, DateUpdateWidget.class);
			inup.putExtra("awId", awID);
			PendingIntent pi2 = PendingIntent.getActivity(context, 5555+awID, inup,
					PendingIntent.FLAG_UPDATE_CURRENT);
			remoteViews.setOnClickPendingIntent(R.id.im_widget_refresh, pi2);
			SolarDate sd = SolarDate.newInstance(SolarDate.DATE_KIND_SOLAR);
			remoteViews.setTextViewText(R.id.tv_widget_title, context.getResources().getString(R.string.notificationTodayBody)+" "+sd.toString());

			Intent toastIntent = new Intent(context, DateWidgetShow.class);
			toastIntent.setAction(DateWidgetShow.TOAST_ACTION);
			toastIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetIds[i]);
			intent.setData(Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME)));
			PendingIntent toastPendingIntent = PendingIntent.getBroadcast(context, 0, toastIntent,
					PendingIntent.FLAG_UPDATE_CURRENT);
			remoteViews.setPendingIntentTemplate(R.id.widgetRemindersListView, toastPendingIntent);



			appWidgetManager.updateAppWidget(awID, remoteViews);

			/*
			RemoteViews v = new RemoteViews(context.getPackageName(), R.layout.studio_widget_layout);
			Intent in = new Intent(context, PopupBirthday.class);
			SolarDate sd = SolarDate.newInstance(SolarDate.DATE_KIND_SOLAR);
			String currentSolarDay = sd.getMMDD();


			createViews(context,currentSolarDay,v);

			in.putExtra("currentSolarDay", currentSolarDay);
			in.putExtra("makeSound", false);
			in.putExtra("title", sd.toString());			
			PendingIntent pi = PendingIntent.getActivity(context, 4444, in,
					PendingIntent.FLAG_UPDATE_CURRENT);
			v.setOnClickPendingIntent(R.id.tv_widget_body, pi);
			v.setTextViewText(R.id.tv_widget_title, sd.toString());*/
			/*-------------------------------------------------------*/
			
			//v.setTextViewText(R.id.tv_widget_body, todayText);
			/*-------------------------------------------------------*/
			
			/*Intent inTom = new Intent(context, PopupBirthday.class);
			sd.appendDay((short) 1);
			inTom.putExtra("currentSolarDay", sd.getMMDD());
			inTom.putExtra("makeSound", false);
			inTom.putExtra("fromWidget_tomorrow", true);
			inTom.putExtra("title", sd.toString());		
			PendingIntent pi3 = PendingIntent.getActivity(context, 6666, inTom,
					PendingIntent.FLAG_UPDATE_CURRENT);
			v.setOnClickPendingIntent(R.id.im_widget_tommorow, pi3);*/
			/*-------------------------------------------------------*/
			
			/*-------------------------------------------------------*/
			
			
			
			//appWidgetManager.updateAppWidget(awID, v);
		}
	}


	

}
