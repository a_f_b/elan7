package afb.elan.demo.widget;

import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import java.util.ArrayList;

import afb.elan.demo.Db.BirthdayRecord;
import afb.elan.demo.Db.SQLHelper;
import afb.elan.demo.R;
import afb.elan.demo.calendar.SolarDate;

public class WidgetAdapterService extends RemoteViewsService {
    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        return new DfdRemoteViewsFactory(getApplicationContext());
    }

    class DfdRemoteViewsFactory implements RemoteViewsService.RemoteViewsFactory {
        private final Context context;
        //private ReminderRepository reminderRepository;
        private ArrayList<BirthdayRecord> reminders;

        public DfdRemoteViewsFactory(Context context) {
            this.context = context;
            reminders=new ArrayList<BirthdayRecord>();

                //this.reminderRepository = ReminderRepository.getReminderRepository(context);
        }

        @Override
        public void onCreate() {
            reminders.clear();
            SolarDate sd = SolarDate.newInstance(SolarDate.DATE_KIND_SOLAR);
            String currentSolarDay = sd.getMMDD();
            SQLHelper cdbo = new SQLHelper(context);
            cdbo.openDb();
            cdbo.fetchTodayBirthdays(currentSolarDay);
            cdbo.getCursor().moveToFirst();
            while (!cdbo.getCursor().isAfterLast()) {
                BirthdayRecord br = cdbo.readNext();
                reminders.add(br);

            }
            cdbo.closeDb();


        }

        @Override
        public void onDataSetChanged() {
            reminders.clear();
            SolarDate sd = SolarDate.newInstance(SolarDate.DATE_KIND_SOLAR);
            String currentSolarDay = sd.getMMDD();
            SQLHelper cdbo = new SQLHelper(context);
            cdbo.openDb();
            cdbo.fetchTodayBirthdays(currentSolarDay);
            cdbo.getCursor().moveToFirst();
            while (!cdbo.getCursor().isAfterLast()) {
                BirthdayRecord br = cdbo.readNext();
                reminders.add(br);

            }
            cdbo.closeDb();
        }

        @Override
        public void onDestroy() {
            reminders.clear();
        }

        @Override
        public int getCount() {
            return reminders.size();
        }

        @Override
        public RemoteViews getViewAt(int position) {
            BirthdayRecord reminder = reminders.get(position);
            RemoteViews itemView = new RemoteViews(context.getPackageName(), R.layout.studio_thin_birthday_row);
            itemView.setTextViewText(R.id.tvName,reminder.getFullname());
            itemView.setTextViewText(R.id.tvDate,reminder.getBirthday_Date());
            if (reminder.getImage() != null) {
                itemView.setImageViewBitmap(R.id.imageView1, BitmapFactory
                        .decodeByteArray(reminder.getImage(), 0, reminder.getImage().length));
                //holder.image.setBackgroundColor(Color.BLACK);
            } else {
                itemView.setImageViewResource(R.id.imageView1,R.drawable.blank_user);
                //holder.image.setBackgroundColor(Color.TRANSPARENT);
            }
            //Bundle extras = new Bundle();
            Bundle extras = new Bundle();
            extras.putInt("id", reminder.getId());
            extras.putInt("type", SQLHelper.SearchNode.TYPE_DATE);
            Intent fillInIntent = new Intent();
            fillInIntent.putExtras(extras);
            itemView.setOnClickFillInIntent(R.id.layout, fillInIntent);            // Make it possible to distinguish the individual on-click
            // action of a given item

            return itemView;
        }

        @Override
        public RemoteViews getLoadingView() {
            return null;
        }

        @Override
        public int getViewTypeCount() {
            return 1;
        }

        @Override
        public long getItemId(int position) {
            return reminders.get(position).getId();
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }
    }
}
