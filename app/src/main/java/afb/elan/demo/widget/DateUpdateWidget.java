package afb.elan.demo.widget;

import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.os.Bundle;
import android.widget.Toast;

import afb.elan.demo.R;

public class DateUpdateWidget extends Activity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		int awId=getIntent().getExtras().getInt("awId");

		/*Intent intent = new Intent(this, DfdWidgetService.class);
		RemoteViews remoteViews = new RemoteViews(getPackageName(), R.layout.studio_widget_layout);
		remoteViews.setRemoteAdapter(R.id.widgetRemindersListView, intent);
		remoteViews.setEmptyView(R.id.widgetRemindersListView, R.id.widgetEmptyViewText);
		Intent inup = new Intent(this, UpdateWidget.class);
		inup.putExtra("awId", awId);
		PendingIntent pi2 = PendingIntent.getActivity(this, 5555, inup,
				PendingIntent.FLAG_UPDATE_CURRENT);
		remoteViews.setOnClickPendingIntent(R.id.im_widget_refresh, pi2);
		SolarDate sd = SolarDate.newInstance(SolarDate.DATE_KIND_SOLAR);
		remoteViews.setTextViewText(R.id.tv_widget_title, getResources().getString(R.string.notificationTodayBody)+" "+sd.toString());

		// Instruct the widget manager to update the widget*/

		
		AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(this);
		//appWidgetManager.updateAppWidget(awId, remoteViews);
		appWidgetManager.notifyAppWidgetViewDataChanged(awId, R.id.widgetRemindersListView);

		Toast.makeText(this, getResources().getString(R.string.refreshComplete), Toast.LENGTH_SHORT).show();
		
		finish();
	}
	
	

}
