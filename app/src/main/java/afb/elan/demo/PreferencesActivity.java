package afb.elan.demo;

import android.content.Intent;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.widget.Toast;


public class PreferencesActivity extends PreferenceActivity {
	
	String currentSortOrder=null,currentTheme=null;
	boolean sortchanged=false,themeChanged=false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);

			addPreferencesFromResource(R.xml.preferences);
			
			final CheckBoxPreference cbshowtoday=(CheckBoxPreference)findPreference("prefShowTodayListEnabled");
			final ListPreference lpshowtodaytype=(ListPreference)findPreference("prefShowTodayListType");
			final CheckBoxPreference cbsendsms=(CheckBoxPreference)findPreference("prefSmsSendEnabled");
			final CheckBoxPreference cbsmsdelivery=(CheckBoxPreference)findPreference("prefSmsDeliveryEnabled");
			
			cbshowtoday.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
				
				@Override
				public boolean onPreferenceClick(Preference arg0) {
					// TODO Auto-generated method stub
					
					
					lpshowtodaytype.setEnabled(cbshowtoday.isChecked());
					return false;
				}
			});
			
			
			cbsendsms.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
				
				@Override
				public boolean onPreferenceClick(Preference arg0) {
					// TODO Auto-generated method stub
					
					
					cbsmsdelivery.setEnabled(cbsendsms.isChecked());
					return false;
				}
			});
			
			ListPreference lp=(ListPreference)findPreference("prefSortOrder");
			lp.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
				
				@Override
				public boolean onPreferenceChange(Preference arg0, Object arg1) {
					// TODO Auto-generated method stub
					
					currentSortOrder=(String)arg1;
					sortchanged=true;
					return true;
					
				}
			});
			
			ListPreference lptheme=(ListPreference)findPreference("prefListTheme");
			lptheme.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
				
				@Override
				public boolean onPreferenceChange(Preference arg0, Object arg1) {
					// TODO Auto-generated method stub					
					currentTheme=(String)arg1;
					themeChanged=true;
					Toast.makeText(PreferencesActivity.this, getResources().getString(R.string.pleaseRestart) , Toast.LENGTH_SHORT).show();
					return true;
					
				}
			});
			
			lpshowtodaytype.setEnabled(cbshowtoday.isChecked());
			cbsmsdelivery.setEnabled(cbsendsms.isChecked());
	}

	@Override
	public void finish() {
		// we need to override this to performe the animtationOut on each
		// finish.
				Bundle b=new Bundle();
				b.putBoolean("sortChanged", sortchanged);
				b.putBoolean("themeChanged", themeChanged);
				if (sortchanged) b.putString("sortOrder", currentSortOrder);
				if (themeChanged) b.putString("theme", currentTheme);
				Intent i=new Intent();
				i.putExtras(b);
				setResult(RESULT_OK,i);
				
		        super.finish();
				// disable default animation
				overridePendingTransition(0, 0);
				


	} 	
	
}