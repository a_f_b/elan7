package afb.elan.demo;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.labters.lottiealertdialoglibrary.DialogTypes;
import com.labters.lottiealertdialoglibrary.LottieAlertDialog;

import afb.elan.demo.R;

public class TestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test);
        ((Button)findViewById(R.id.b1)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*BottomSheetMaterialDialog mBottomSheetDialog = new BottomSheetMaterialDialog.Builder(TestActivity.this)
                        .setTitle("Delete?")
                        .setMessage("Are you sure want to delete this file?")
                        .setCancelable(true)
                        .setAnimation(R.raw.lottie_bicycle)
                        .setPositiveButton("Delete", R.drawable.ic_action_delete, new BottomSheetMaterialDialog.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int which) {
                                Toast.makeText(getApplicationContext(), "Deleted!", Toast.LENGTH_SHORT).show();
                                dialogInterface.dismiss();
                            }
                        })
                        .setNegativeButton("Cancel", R.drawable.ic_action_exit, new BottomSheetMaterialDialog.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int which) {
                                Toast.makeText(getApplicationContext(), "Cancelled!", Toast.LENGTH_SHORT).show();
                                dialogInterface.dismiss();
                            }
                        })
                        .build();

                // Show Dialog
                mBottomSheetDialog.show();*/
                LottieAlertDialog
                        alertDialog= new LottieAlertDialog.Builder(TestActivity.this, DialogTypes.TYPE_QUESTION)
                        .setTitle("تیتراژ")
                        .setDescription("متن پیام")
                        .setPositiveText("بلی")
                        .setPositiveButtonColor(getResources().getColor(R.color.green))
                        .setNegativeText("خیر")
                        .setNegativeButtonColor(Color.parseColor("#1cd3ef"))
                        .build();
                alertDialog.setCancelable(true);
                alertDialog.show();
            }
        });
    }
}
