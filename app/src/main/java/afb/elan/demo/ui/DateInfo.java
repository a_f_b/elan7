package afb.elan.demo.ui;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.List;

import afb.elan.demo.AddContact;
import afb.elan.demo.Db.BirthdayRecord;
import afb.elan.demo.Db.SQLHelper;
import afb.elan.demo.ImageViewer;
import afb.elan.demo.MainActivity;
import afb.elan.demo.R;
import dev.shreyaspatil.MaterialDialog.BottomSheetMaterialDialog;

public class DateInfo extends AppCompatActivity implements View.OnClickListener {

    BirthdayRecord br;
    TextView tvName;
    TextView tvFamily;
    TextView tvdate;
    TextView tvmobile;
    TextView tvphone;
    TextView tvemail;
    TextView tvdatetype;
    ImageView im;
    //SharedPreferences sp;
    protected static final int GOTO_SMS = 0x1337;
    protected static final int GOTO_PHONE_CALL = 0x1338;
    protected static final int GOTO_MOBILE_CALL = 0x1339;
    protected static final int GOTO_EMAIL = 0x133A;

    Handler h=new Handler(){
        @Override
        public void handleMessage(Message msg) {

            super.handleMessage(msg);

            switch(msg.what){
                case GOTO_SMS:
                    sendSMS();
                    break;
                case GOTO_EMAIL:
                    sendEmail();
                    break;
                case GOTO_PHONE_CALL:
                    callPhone();
                    break;
                case GOTO_MOBILE_CALL:
                    callMobile();
                    break;
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.infodialog);


        String action = getIntent().getAction();
        if (action==null) action="null";
        if (action.equals(Intent.ACTION_VIEW)) {
            // throw new RuntimeException("Should not happen");
            Uri data = getIntent().getData();
            String scheme = data.getScheme(); // "elanapp"
            String host = data.getHost(); // "afb.elan.demo"
            List<String> paths = data.getPathSegments(); // 0:date
            int id=Integer.valueOf(data.getQueryParameter("id"));
            SQLHelper cdbo = new SQLHelper(this);
            cdbo.openDb();
            br=cdbo.fetchRecordById(id);
            cdbo.closeDb();
        }
        else {
                br = (BirthdayRecord) getIntent().getSerializableExtra("date");

        }
        setupViews();




    }

    private void setupViews() {
        // TODO Auto-generated method stub
        tvName =(TextView)findViewById(R.id.tvName);
        tvFamily =(TextView)findViewById(R.id.tvFamily);
        tvdate=(TextView)findViewById(R.id.tvInfoDialogDateValue);
        tvmobile=(TextView)findViewById(R.id.tvInfoDialogMobileValue);
        tvphone=(TextView)findViewById(R.id.tvInfoDialogPhoneValue);
        tvemail=(TextView)findViewById(R.id.tvInfoDialogEmailValue);
        tvdatetype=(TextView)findViewById(R.id.tvInfoDialogDateTypeValue);
        im=(ImageView)findViewById(R.id.im_infodialog);

        tvName.setText(br.getName());
        tvFamily.setText(br.getFamily());
        tvdate.setText(br.getBirthday_Date());
        tvmobile.setText(br.getMobile());
        tvphone.setText(br.getPhone());
        tvemail.setText(br.getEmail());
        tvdatetype.setText(br.getCustomDateExp());
        if (br.hasImage())
        {
            im.setBackgroundColor(Color.BLACK);
            im.setImageBitmap(BitmapFactory.decodeByteArray( br.getImage(),
                    0,br.getImage().length));
            im.setOnClickListener(this);
        }
        Button finish=(Button)findViewById(R.id.bEditDate);
        finish.setOnClickListener(this);
        Button sendSms=(Button)findViewById(R.id.bDeleteDate);
        sendSms.setOnClickListener(this);

        tvemail.setOnClickListener(this);
        tvmobile.setOnClickListener(this);
        tvphone.setOnClickListener(this);
        tvdate.setOnClickListener(this);
        tvdatetype.setOnClickListener(this);




    }

    private void sendSMS(){
        Intent newintent=null;
        //String defaultText=sp.getString("prefsSmsDefaultText", "");
        newintent=new Intent(Intent.ACTION_VIEW);
        newintent.putExtra("address", tvmobile.getText().toString());
        newintent.putExtra("sms_body", br.getSmsBody());
        newintent.setData(Uri.parse("smsto:" + tvmobile.getText().toString()));
        startActivity(newintent);
        finish();
    }

    private void sendEmail(){
        if (tvemail.getText().toString().length()>0)
            try{
                Intent emailintent = new Intent(Intent.ACTION_SEND);
                String emailaddresses[] = { tvemail.getText().toString() };
                emailintent.putExtra(Intent.EXTRA_EMAIL,
                        emailaddresses);
                emailintent.putExtra(Intent.EXTRA_SUBJECT,
                        "");
                emailintent.setType("plain/text");
                startActivity(emailintent);
                finish();

            }catch (Exception e){
                e.printStackTrace();
            }
    }

    private void callMobile(){
        Intent newintent2=new Intent(Intent.ACTION_DIAL,Uri.parse("tel:"+tvmobile.getText().toString()));
        startActivity(newintent2);
        finish();
    }

    private void callPhone(){
        Intent newintent=new Intent(Intent.ACTION_DIAL,Uri.parse("tel:"+tvphone.getText().toString()));
        startActivity(newintent);
        finish();
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        //Toast.makeText(, "email", 1000).show();
        Message msg=null;
        switch (v.getId()){
            case R.id.tvInfoDialogMobileValue:
                if (tvmobile.getText().toString().length()>0){
                    msg=new Message();
                    msg.what=GOTO_MOBILE_CALL;
                    this.h.sendMessage(msg);

                }
                break;
            case R.id.tvInfoDialogPhoneValue:
                if (tvphone.getText().toString().length()>0){
                    msg=new Message();
                    msg.what=GOTO_PHONE_CALL;
                    this.h.sendMessage(msg);

                }
                break;
            case R.id.tvInfoDialogEmailValue:

                msg=new Message();
                msg.what=GOTO_EMAIL;
                this.h.sendMessage(msg);

                break;

            case R.id.bEditDate:
                Bundle extra = new Bundle();
                extra.putBoolean("editMode", true);
                extra.putLong("editingId", br.getId());
                Intent intent = new Intent(this, AddContact.class);
                intent.putExtras(extra);
                startActivity(intent);
                finish();
                break;

            case R.id.bDeleteDate:
                afb.elan.MaterialDialog.CompatDialog mBottomSheetDialog = new afb.elan.MaterialDialog.CompatDialog.Builder(DateInfo.this)
                        .setTitle("حذف")
                        .setMessage("آیا از حذف این مناسبت اطمینان دارید؟")
                        .setCancelable(true)
                        .setAnimation(R.raw.question)
                        .setPositiveButton("حذف", R.drawable.ic_action_delete, new BottomSheetMaterialDialog.OnClickListener() {
                            @Override
                            public void onClick(dev.shreyaspatil.MaterialDialog.interfaces.DialogInterface dialogInterface, int which) {
                                SQLHelper cdbo = new SQLHelper(DateInfo.this, SQLHelper.KEY_ID);
                                cdbo.openDb();
                                cdbo.DeleteRecord(br.getId());
                                cdbo.closeDb();
                                Intent intent=new Intent(MainActivity.ELAN_DELETE_DATE);
                                sendBroadcast(intent);
                                dialogInterface.dismiss();
                                finish();
                            }
                        })
                        .setNegativeButton("صرف نظر", R.drawable.ic_action_no, new BottomSheetMaterialDialog.OnClickListener() {
                            @Override
                            public void onClick(dev.shreyaspatil.MaterialDialog.interfaces.DialogInterface dialogInterface, int which) {
                                dialogInterface.dismiss();
                            }
                        })
                        .build();

                // Show Dialog
                mBottomSheetDialog.show();


                break;

            case R.id.im_infodialog:
                Intent i=new Intent(this, ImageViewer.class);
                i.putExtra("image", br.getImage());
                startActivity(i);

        }

    }

}
