package afb.elan.demo.ui;

import android.content.Context;
import android.graphics.BitmapFactory;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import afb.elan.demo.Db.BirthdayRecord;
import afb.elan.demo.Db.SQLHelper;
import afb.elan.demo.R;
import afb.elan.demo.calendar.SolarDate;
import de.hdodenhof.circleimageview.CircleImageView;

public class DatesAdapter extends RecyclerView.Adapter<DatesAdapter.MyViewHolder> {

    private Context context;

    private ArrayList<BirthdayRecord> datesList;
    private boolean animationEnabled = false;
    int lastPosition=-1;
    int viewType=1;
    int currentDayValue=0;
    int kabise=0;



    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, date, remaindays,sms;
        public CircleImageView image;
        public View parent;
        //public SquareImageView image;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            date = (TextView) view.findViewById(R.id.date);
            sms = (TextView) view.findViewById(R.id.sms);
            remaindays = (TextView) view.findViewById(R.id.remaindays);
            image=(CircleImageView)view.findViewById(R.id.imageView1);
            parent=(View) view.findViewById(R.id.selectedLayout);
        }
    }


    public DatesAdapter(Context context, int view_type) {
        this.context = context;
        viewType=view_type;
        SolarDate sd = SolarDate.newInstance(SolarDate.DATE_KIND_SOLAR);
        currentDayValue = sd.getDayValue();
        SQLHelper cdbo = new SQLHelper(context, SQLHelper.SORT_ORDER_REMAIN);

        datesList =new ArrayList<>();
        cdbo.openDb();
        datesList =cdbo.fetchAllRecords(currentDayValue);
        /*for (int i=0;i<50;i++){
            BirthdayRecord br=new BirthdayRecord(i,i+"-"+i);
            datesList.add(br);
        }*/
        cdbo.closeDb();

        kabise=(short) ((sd.getYear()%4==3)?1:0);


    }

    public ArrayList<BirthdayRecord> getList(){
        return datesList;
    }

    public void setViewType(int viewType){
        this.viewType=viewType;
    }

    @Override
    public int getItemViewType(int position) {
        // Just as an example, return 0 or 2 depending on position
        // Note that unlike in ListView adapters, types don't have to be contiguous
        return viewType;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate((viewType==DatesFragment.VIEW_TYPE_LINEAR)?R.layout.studio_birthday_item_row :R.layout.studio_birthday_item_grid, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onViewDetachedFromWindow(MyViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.itemView.clearAnimation();
    }



    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        BirthdayRecord movie = datesList.get(position);
        holder.name.setText((viewType==DatesFragment.VIEW_TYPE_LINEAR)?movie.toString():movie.getCustomDateExp()+" "+movie.toString());
        int tb = movie.getSolarValue();
        tb = (tb >= currentDayValue) ? tb - currentDayValue : tb
                - currentDayValue + 365+kabise;
        String remain=(tb==0)?new StringBuilder().append(movie.getCustomDateExp()).append("  -  ").append(context.getResources().getString(R.string.today)).toString():new StringBuilder().append(tb).append(context.getResources().getString(R.string.toBirthday)).append(" ").append(movie.getCustomDateExp()).toString();
        holder.remaindays.setText(remain);
        holder.date.setText(movie.getBirthday_Date());
        holder.sms.setText(movie.getSmsStatus(context));
        if (datesList.get(position).getChecked())
            holder.parent.setVisibility(View.VISIBLE);
         else
            holder.parent.setVisibility(View.GONE);


        if (movie.getImage() != null) {
            holder.image.setImageBitmap(BitmapFactory
                    .decodeByteArray(movie.getImage(), 0, movie.getImage().length));
        } else {
            holder.image.setImageResource(R.drawable.blank_user);
        }

        /*if (animationEnabled){
            Animation animation = AnimationUtils.loadAnimation(context,
                    (position > lastPosition) ? R.anim.up_from_bottom
                            : R.anim.down_from_top);
            holder.itemView.startAnimation(animation);
        }

        lastPosition = position;*/

    }

    public void setAnimationEnabled(boolean _anim){
        animationEnabled=_anim;
        //Log.e("enabledanim","anim"+animationEnabled);

    }


    @Override
    public int getItemCount() {
        return datesList.size();
    }

    public  void deleteSelectedList(){
        SQLHelper cdbo = new SQLHelper(context, SQLHelper.KEY_ID);
        cdbo.openDb();
        for (int i=datesList.size()-1;i>=0;i--){
            if (datesList.get(i).getChecked()){
                cdbo.DeleteRecord(datesList.get(i).getId());
                datesList.remove(i);
            }
        }
        cdbo.closeDb();
        notifyDataSetChanged();
    }

    public void reloadFromDataBase(){
        SolarDate sd = SolarDate.newInstance(SolarDate.DATE_KIND_SOLAR);
        currentDayValue = sd.getDayValue();
        SQLHelper cdbo = new SQLHelper(context, SQLHelper.SORT_ORDER_REMAIN);

        datesList.clear();
        cdbo.openDb();
        datesList =cdbo.fetchAllRecords(currentDayValue);
        /*for (int i=0;i<50;i++){
            BirthdayRecord br=new BirthdayRecord(i,i+"-"+i);
            datesList.add(br);
        }*/
        cdbo.closeDb();
        notifyDataSetChanged();

    }


}
