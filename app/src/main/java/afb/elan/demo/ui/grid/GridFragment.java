package afb.elan.demo.ui.grid;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.apache.http.NameValuePair;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import afb.elan.demo.R;
import afb.elan.demo.classes.Utils;


public class GridFragment extends Fragment implements OnClickListener, OnItemClickListener{

	RelativeLayout rootView;
	String url=null,loadedJson=null;
	int columnCount=3;
	View loading;
	private ArrayList<GridNode> nodes=null;
	private ArrayList<GridNode> filtered=null;
	//ImageLoader imageLoader;
	GridView grid;
	ImageAdapter adapter=null;
	int item_width=75;
	boolean hasCaption=true;
	String key=null;
	List<NameValuePair> nameValuePairs=null;
	boolean guest=true;

	int currentToolsDirectoryID=0;

	private OnGridClickedListener	mListener=null;
	public interface OnGridClickedListener {
		public void onGridClick(String key, String link, boolean needLogin);
	}
	public void setOnGridClickedListener(OnGridClickedListener listener){
		mListener = listener;
	}




	public static int convertSpToPixels(float sp, Context context) {
		int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, context.getResources().getDisplayMetrics());
		return px;
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle b=getArguments();
		url=b.getString("url");
		guest=b.getBoolean("guest", true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {

		columnCount=3;
		this.hasCaption=true;



		LayoutInflater mInflater = (LayoutInflater)
				getActivity().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

		rootView=(RelativeLayout) mInflater.inflate(R.layout.gridview, null);

		//if (savedInstanceState == null)
		init();





		grid=(GridView) rootView.findViewById(R.id.gridView1);
		grid.setNumColumns(columnCount);

		adapter=new ImageAdapter(getActivity());
		ViewTreeObserver vto = grid.getViewTreeObserver();
		vto.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {

			@Override
			public void onGlobalLayout() {
				// TODO Auto-generated method stub
				int gridwidth = grid.getMeasuredWidth();
				int space=convertSpToPixels(11,getActivity());
				item_width=(gridwidth-(columnCount+1)*space)/columnCount;
				//DisplayMetrics dm=getResources().getDisplayMetrics();
				//item_width=(int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, item_width, dm);

				grid.setAdapter(adapter);

				ViewTreeObserver obs = grid.getViewTreeObserver();
				obs.removeGlobalOnLayoutListener(this);

			}
		});

		/*AnimationSet set = new AnimationSet(true);
		Animation animation2 = AnimationUtils.loadAnimation(getActivity(),
				R.anim.grid_anim);
		set.addAnimation(animation2);
		Animation animation3 = AnimationUtils.loadAnimation(getActivity(),
				R.anim.grid_anim2);
		set.addAnimation(animation3);
		LayoutAnimationController controller = new LayoutAnimationController(
				set, 0.2f);
		controller.setOrder(LayoutAnimationController.ORDER_RANDOM);
		grid.setLayoutAnimation(controller);*/
		grid.setOnItemClickListener(this);
		AnimationSet set = new AnimationSet(true);
		Animation animation2 = AnimationUtils.loadAnimation(getActivity(),
				R.anim.grid_anim);
		set.addAnimation(animation2);
		Animation animation3 = AnimationUtils.loadAnimation(getActivity(),
				R.anim.grid_anim2);
		set.addAnimation(animation3);
		LayoutAnimationController controller = new LayoutAnimationController(
				set, 0.2f);
		controller.setOrder(LayoutAnimationController.ORDER_RANDOM);
		//controller.setDelay(0.2f);
		grid.setLayoutAnimation(controller);



		return rootView;

	}



	private void init(){
		//imageLoader=new ImageLoader(getActivity());
		if (loadedJson==null){
			loading= Utils.prepareLoadingLayout(getActivity());
			rootView.addView(loading);

			new GetQuickLinks(url).execute();
		}

	}

	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		super.onSaveInstanceState(savedInstanceState);
		savedInstanceState.putBoolean("displayed_contacts", true);
	}




	public void hideLoading(){
		if (getActivity() == null) return;
		Animation anim=AnimationUtils.loadAnimation(getActivity(),
				R.anim.fade_out_simple);
		anim.setAnimationListener(new Animation.AnimationListener() {

			@Override
			public void onAnimationStart(Animation arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationRepeat(Animation arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationEnd(Animation arg0) {
				// TODO Auto-generated method stub
				loading.setVisibility(ViewGroup.GONE);
			}
		});
		loading.startAnimation(anim);
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub

	}

	private class GetQuickLinks extends AsyncTask<Void, Void, Void> {

		public GetQuickLinks(String key) {
			// TODO Auto-generated constructor stub
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Showing progress dialog
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// Creating service handler class instance
			// adding contact to contact list
			//quots=JsonFunctions.getQuotations(getActivity(), url, uKey);
			nodes = new ArrayList<GridNode>();
			filtered = new ArrayList<GridNode>();

			//ServiceHandler sh = new ServiceHandler();

			//loadedJson = sh.makeServiceCall(url, ServiceHandler.GET, null);
			//Log.e("tools",loadedJson);


			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			// Dismiss the progress dialog

			//age.setLabel(userObject.age+"\n"+getResources().getString(R.string.years));
			//age.setPercentage(userObject.age);
			/*if (!isAdded()) return;
			if (loadedJson != null)
				try {

					SharedPreferences sharedpreferences = getActivity().getSharedPreferences("offline", 0);
					SharedPreferences.Editor editor = sharedpreferences.edit();
					Set<String> offlinetools=sharedpreferences.getStringSet("tools", new HashSet<String>());

					JSONArray jsonArray = new JSONArray(loadedJson);
					GridNode gn;

					for (int i = 0; i < jsonArray.length(); i++) {
						//Log.e("tools","tools"+i);
						try{
							AesCipher aes=new AesCipher("offlinetools");

						gn=new GridNode();
						JSONObject jo= jsonArray.getJSONObject(i);
						//Log.e("toooools: ", jo.toString());
						if (jo.has("id")) gn.tag=jo.getInt("id");
						if (jo.has("caption")) gn.caption=jo.getString("caption");
						if (jo.has("package")) gn.key=jo.getString("package");
						if (jo.has("directlink")) gn.link=jo.getString("directlink");
						if (jo.has("needlogin")) gn.needlogin=(jo.getInt("needlogin")==1);
						if (jo.has("folder")) gn.folder=(jo.getInt("folder")==1);
						if (jo.has("parent")) gn.parent=jo.getInt("parent");
						gn.imageUrl=jo.getString("image");
						nodes.add(gn);
						if (jo.has("offline"))
							if ((jo.getInt("offline")==1) && (jo.getInt("needlogin")==0)){
								Gson gson = new Gson();
								String json = gson.toJson(gn);
								json=aes.encrypt(json);
								offlinetools.add(json);
							}

						if (gn.parent==currentToolsDirectoryID) filtered.add(gn);

						} catch (Exception e) {
							e.printStackTrace();
						}

					}
					editor.clear();
					editor.putStringSet("offlinetools", offlinetools);
					editor.commit();				} catch (JSONException e) {
					// Log.e("Response: ", "Error parsing");
					Log.e("Response: ", " length = null");

					e.printStackTrace();
				}*/
			adapter.notifyDataSetChanged();
			hideLoading();





		}

	}



	public class ImageAdapter extends BaseAdapter {

		private Context context;

		private class Holder {
			public TextView tv;
			public ImageView iv;
		}


		// stores all data for album..

		// my folder I'm currently loading from..

		// views stores all the loaded ImageViews linked to their
		// position in the GridView; hence  in
		// the HashMap..
		//private HashMap views;

		public ImageAdapter (Context c) {
			context = c;
			//views = new HashMap();
		}

		@Override
		public int getCount() {
			if (filtered!=null)
				return filtered.size();
			else return 0;
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public void notifyDataSetChanged() {
			super.notifyDataSetChanged();
			//views.clear();
		}


		@Override
		public View getView(int position, View v, ViewGroup parent) {

			Holder h = new Holder();
			if (v == null) {
				LayoutInflater mInflater = (LayoutInflater) context
						.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
				v = mInflater
						.inflate(R.layout.grid_node, null);

				// create a new ImageView..

				v.setLayoutParams(new GridView.LayoutParams(item_width, item_width));
				h.iv=(ImageView) v.findViewById(R.id.imageView1);
				h.tv=(TextView)v.findViewById(R.id.textView1);
				v.setTag(h);

			}
			else
				h = (Holder) v.getTag();


			// I'm setting a default image here so that you don't
			// see black nothingness.. (just using an icon that
			// comes with the Android SDK)
			// get the filename that this ImageView will hold..

			// pass this Bundle along to the LoadImage class,
			// which is a subclass of Android's utility class
			// AsyncTask. Whatever happens in this class is
			// on its own thread.. the Bundle passes
			// the file to load and the position the photo
			// should be placed in the GridView..

			if (hasCaption)
				h.tv.setText(filtered.get(position).caption);
			else
				h.tv.setVisibility(View.GONE);

			if ((filtered.get(position).needlogin) && guest){
				h.tv.setAlpha(0.5f);
				h.iv.setAlpha(0.5f);


			} else
			{
				h.tv.setAlpha(1f);
				h.iv.setAlpha(1f);

			}
			/*if (!isIncluded(filtered.get(position).key)) {
				if (!MainActivity.isPackageInstalled(getActivity(), filtered.get(position).key)) {
					Utils.setLocked(h.iv);
					h.tv.setAlpha(0.4f);
					h.iv.setAlpha(0.4f);
				}

			}
			else
			{
				Utils.setUnlocked(h.iv);
			}*/




			//Picasso.with(context).load(URLs.imagesUrl+filtered.get(position).imageUrl).resize(128, 128).into(h.iv);



			// return the view to the GridView..
			// at this point, the ImageView is only displaying the
			// default icon..
			return v;

		}

		// this is the class that handles the loading of images from the cloud
		// inside another thread, separate from the main UI thread..

	}


	private void changeDir(int node_id){
		filtered.clear();
		adapter.notifyDataSetChanged();

		filtered=new ArrayList<GridNode>();

		if (node_id>0){
			GridNode gn=new GridNode();
			gn.parent=currentToolsDirectoryID;
			gn.tag=currentToolsDirectoryID;
			gn.caption="شاخه قبلی";
			gn.key="up";
			gn.link="up";
			gn.needlogin=false;
			gn.folder=true;
			gn.imageUrl="tools/folderup00.png";
			filtered.add(gn);
		}
		currentToolsDirectoryID=node_id;
		for (int i=0;i<nodes.size();i++){
			if (nodes.get(i).parent==node_id) filtered.add(nodes.get(i));
		};
		adapter.notifyDataSetChanged();
		//grid.setAdapter(adapter);
		grid.startLayoutAnimation();

	}



	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
		// TODO Auto-generated method stub
		Log.e("currentToolsDirectoryID", currentToolsDirectoryID+"");
        Log.e("tag", filtered.get(pos).tag+"");
		if (filtered.get(pos).folder){
			changeDir(filtered.get(pos).tag);
			return;
		}
		if (mListener!=null)
			mListener.onGridClick(filtered.get(pos).key,filtered.get(pos).link,filtered.get(pos).needlogin);
		else
			Log.e("listener: ", "  = null");

	}

	public boolean backPressed(){
		if (currentToolsDirectoryID>0){
			if (filtered.get(0).folder){
				changeDir(filtered.get(0).tag);
				return true;
			}
		}
		return false;
	}


	private boolean isIncluded(String text){
		String[] packages = { "up", "afb.expco.com.takhir.tadie", "afb.expco.com", "afb.expco.bongah", "afb.expco.ejare",
				"afb.expco.vakil", "afb.expco.tambr", "afb.expco.mohandes", "afb.expco.money", "afb.expco.gold", "afb.expco.davar",
				"afb.expco.nashriat", "afb.expco.convertor","afb.expco.notes","afb.expco.barcode","afb.expco.compass","afb.expco.ruler","afb.expco.soundmeter",
				"afb.expco.melli","afb.expco.plaque"};

		if (Arrays.asList(packages).contains(text))
			return true;
		else
			return false;
	}



}
