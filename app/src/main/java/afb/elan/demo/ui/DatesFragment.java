package afb.elan.demo.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import java.util.HashMap;
import java.util.LinkedHashMap;

import afb.elan.demo.AddContact;
import afb.elan.demo.Db.BirthdayRecord;
import afb.elan.demo.Db.SQLHelper;
import afb.elan.demo.MyNotification;
import afb.elan.demo.R;
import afb.elan.demo.RecyclerTouchListener;

public class DatesFragment extends Fragment {

    DatesAdapter mAdapter;
    GridLayoutManager mGridLayoutManager;
    LinearLayoutManager mLayoutManager;
    RecyclerView recyclerView;
    public int selectedItemCount =0;
    public final static int VIEW_TYPE_LINEAR=1;
    public final static int VIEW_TYPE_GRID=2;

    private OnNotifySelectedItemChanged	notifySelectedItemChangedListener=null;
    public interface OnNotifySelectedItemChanged {
        public void notifySelectedItemChanged(int selectedCount);
    }
    public void setOnNotifySelectedItemChanged(OnNotifySelectedItemChanged listener){
        notifySelectedItemChangedListener = listener;
    }


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root= inflater.inflate(R.layout.fragment_dates, container, false);


        //mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        //mGridLayoutManager = new GridLayoutManager(getActivity(),2);
        // = new GridLayoutManager(getActivity(),1,LinearLayoutManager.HORIZONTAL,false);

        /*Calendar c = Calendar.getInstance();
        SolarDate sd = new SolarDate();
        sd.setdate(c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1,
                c.get(Calendar.DAY_OF_MONTH));
        sd.toSolar();
        int currentDayValue = sd.getSolarDayValue();*/
        //Log.e("br",""+currentDayValue);

        SharedPreferences someData = getActivity().getSharedPreferences("data", 0);
        int showMode=someData.getInt("showMode",R.id.nav_row);
        Log.e("showMode",R.id.nav_row+"="+showMode+","+R.id.nav_grid+"="+showMode);
        showMode= (showMode==R.id.nav_row)? VIEW_TYPE_LINEAR:VIEW_TYPE_GRID;

        mAdapter= new DatesAdapter(getActivity(),showMode);
        mAdapter.setAnimationEnabled(false);

        recyclerView = (RecyclerView) root.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        //implementScrollListener();
        recyclerView.setAdapter(mAdapter);
        reLoad(showMode);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(),recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                if (selectedItemCount >0) {
                    if (!mAdapter.getList().get(position).getChecked()){
                        selectedItems.put(mAdapter.getList().get(position).getId(),mAdapter.getList().get(position));
                        mAdapter.getList().get(position).setChecked(true);
                        selectedItemCount++;
                    }
                    else
                    {
                        selectedItems.remove(mAdapter.getList().get(position).getId());
                        mAdapter.getList().get(position).setChecked(false);
                        selectedItemCount--;
                    }
                    mAdapter.notifyItemChanged(position);
                    notifySelectModeChanged();
                } else {
                    Intent mainintent = new Intent(getActivity(), DateInfo.class);
                    mainintent.putExtra("date", mAdapter.getList().get(position));
                    mainintent.putExtra("id", -1);
                    startActivity(mainintent);


                }

            }

            @Override
            public void onLongClick(View view, int position) {
                /*MyNotification mn=new MyNotification(getActivity());
                SQLHelper.SearchNode sn=new SQLHelper.SearchNode();
                sn.id=1;
                sn.content="343243";
                sn.title="dfg";
                mn.showNotification(sn,123456);*/


                if (!mAdapter.getList().get(position).getChecked()){
                    selectedItems.put(mAdapter.getList().get(position).getId(),mAdapter.getList().get(position));
                    mAdapter.getList().get(position).setChecked(true);
                    selectedItemCount++;
                }
                else
                {
                    selectedItems.remove(mAdapter.getList().get(position).getId());
                    mAdapter.getList().get(position).setChecked(false);
                    selectedItemCount--;
                }
                mAdapter.notifyItemChanged(position);
                notifySelectModeChanged();

            }
        }));



        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }


    public void reLoad(){
        mAdapter.reloadFromDataBase();
    }


    public void reLoad(int theme){
        mAdapter.setAnimationEnabled(false);

        switch (theme) {
            case VIEW_TYPE_GRID:
                mGridLayoutManager = new GridLayoutManager(getActivity(),2, LinearLayoutManager.VERTICAL,false);
                //supportInvalidateOptionsMenu();
                recyclerView.setLayoutManager(mGridLayoutManager);
                mAdapter.setViewType(DatesFragment.VIEW_TYPE_GRID);
                mAdapter.notifyDataSetChanged();

                break;
            case VIEW_TYPE_LINEAR:
                mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                recyclerView.setLayoutManager(mLayoutManager);
                mAdapter.setViewType(DatesFragment.VIEW_TYPE_LINEAR);
                mAdapter.notifyDataSetChanged();
                break;

        }
   }

    private void notifySelectModeChanged(){
        notifySelectedItemChangedListener.notifySelectedItemChanged(selectedItemCount);
    }

    HashMap<Integer,BirthdayRecord> selectedItems =new LinkedHashMap<Integer,BirthdayRecord>();

    public void performEdit(){
        int itemId= (int) selectedItems.keySet().toArray()[0];
        Bundle extra = new Bundle();
        extra.putBoolean("editMode", true);
        extra.putLong("editingId", itemId);
        Intent intent = new Intent(getActivity(), AddContact.class);
        intent.putExtras(extra);


        startActivity(intent);
        exitEditMode();
    }

    public void performDelete(){
        mAdapter.deleteSelectedList();
        selectedItems.clear();
        selectedItemCount=0;
        notifySelectModeChanged();

    }


    public void exitEditMode(){
        selectedItems.clear();
        selectedItemCount=0;
        for (int i=0;i<mAdapter.getList().size();i++) mAdapter.getList().get(i).setChecked(false);
        mAdapter.notifyDataSetChanged();
        notifySelectModeChanged();

    }






}