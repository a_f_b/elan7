package afb.elan.demo.ui.grid;

import org.apache.http.NameValuePair;

import java.io.Serializable;
import java.util.List;

public class GridNode implements Serializable {
	/**
	 * 
	 */
	public boolean hasCaption = true;
	public String key=null;
	public String caption;
	public String imageUrl;
	public String link;
	public String hint;
	public int tag;
	public int parent=0;
	public boolean needlogin=false;
	public boolean folder=false;
	public List<NameValuePair> nameValuePairs=null;

	public GridNode(String _key, String _caption, String _imgUrl, String _link) {
		caption=_caption;
		imageUrl=_imgUrl;
		link=_link;
		key=_key;
		hasCaption=(caption!=null);
		tag=0;
	}
	
	public GridNode() {

	}

}
