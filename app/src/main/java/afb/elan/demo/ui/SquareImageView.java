package afb.elan.demo.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import androidx.appcompat.widget.AppCompatImageView;

import afb.elan.demo.R;


public class SquareImageView extends AppCompatImageView {

	private TypedArray ta = null;
	int mode=0; //0=byWidth; 1=byHeight;

	
	public SquareImageView(Context context) {
		super(context);

		init();
	}

	public SquareImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
		ta = context.obtainStyledAttributes(attrs,

		R.styleable.SquareImageView);
		ta.recycle();
		init();
	}

	public SquareImageView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		ta = context.obtainStyledAttributes(attrs,

		R.styleable.SquareImageView);
		ta.recycle();
		init();
	}

	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		// TODO Auto-generated method stub
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);

		int width = View.MeasureSpec.getSize(widthMeasureSpec);
	    int height = View.MeasureSpec.getSize(heightMeasureSpec);
	    int widthDesc = View.MeasureSpec.getMode(widthMeasureSpec);
	    int heightDesc = View.MeasureSpec.getMode(heightMeasureSpec);
	    int wsize = 0;
	    int hsize = 0;
	    /*if (widthDesc == MeasureSpec.UNSPECIFIED
	            && heightDesc == MeasureSpec.UNSPECIFIED) {
	        //wsize = hsize = getContext().getResources().getDimensionPixelSize(R.dimen.default_percent_size); // Use your own default size, for example 125dp
	    	if (mode==0){
		    	wsize = width;
		    	hsize= wsize + (wsize*percentVal/100);
	    	} else {
		    	hsize = height;
		    	wsize= hsize + (hsize*percentVal/100);}
	    } else if ((widthDesc == MeasureSpec.UNSPECIFIED || heightDesc == MeasureSpec.UNSPECIFIED)
	            && !(widthDesc == MeasureSpec.UNSPECIFIED && heightDesc == MeasureSpec.UNSPECIFIED)) {
	        //Only one of the dimensions has been specified so we choose the dimension that has a value (in the case of unspecified, the value assigned is 0)
	        //size = width > height ? width : height;
	    	if (mode==0){
		    	wsize = width;
		    	hsize= wsize + (wsize*percentVal/100);
	    	} else {
		    	hsize = height;
		    	wsize= hsize + (hsize*percentVal/100);
	    	}
	    } else {
	        //In all other cases both dimensions have been specified so we choose the smaller of the two
	    	if (mode==0){
		    	wsize = width;
		    	hsize= wsize + (wsize*percentVal/100);
	    	} else {
		    	hsize = height;
		    	wsize= hsize + (hsize*percentVal/100);
	    	}
	    }*/
	    /*if (mode==0){
	    	wsize = width;
	    	hsize=  wsize + (wsize*percentVal/100);
    	} else {
	    	hsize = height;
	    	wsize= hsize + (hsize*percentVal/100);
    	}*/
	    if (height==0)
	    	setMeasuredDimension(width, width);
	    else
	    	setMeasuredDimension(height, height);


		//vWidth = getMeasuredWidth();

		//vHeight = getMeasuredHeight();
		;

	}
	
	private void init() {

		if (ta != null) {
			//if (ta.hasValue(R.styleable.CustomSizeLinearLayout_mode))
				//mode=ta.getInt(R.styleable.SquareImageView_squaremode, 0);	
			//if (ta.hasValue(R.styleable.CustomSizeLinearLayout_percentValue))
			
		}
	}
}
