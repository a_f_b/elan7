package afb.elan.demo;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;

public class Luncher extends Activity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		SharedPreferences shp = PreferenceManager
				.getDefaultSharedPreferences(this);
		if (shp.getBoolean("prefSplashEnabled", true)){			
			Intent startactivity=new Intent(Luncher.this,Splash.class);
			startActivity(startactivity);
			finish();			
		}
		else
		{
			Intent startactivity=new Intent(Luncher.this,MainActivity.class);
			startActivity(startactivity);
			finish();
		}
	}
	
	

}
