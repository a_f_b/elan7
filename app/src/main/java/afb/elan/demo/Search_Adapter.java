package afb.elan.demo;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import afb.elan.demo.Db.SQLHelper;
import de.hdodenhof.circleimageview.CircleImageView;

public class Search_Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;

    public List<SQLHelper.SearchNode> nodesList;
    private boolean animationEnabled = false;
    int lastPosition=-1;

    final static int EXPERT_TYPE_NORMAL=1;
    final static int EXPERT_TYPE_NOTE =2;
    Typeface tfsiavash, tfyekan, tfnazanin ;



    private class NoramlViewHolder extends RecyclerView.ViewHolder {
        public TextView title, date;
        public CircleImageView image;

        public NoramlViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            date = (TextView) view.findViewById(R.id.date);
            image=(CircleImageView)view.findViewById(R.id.imageView1);
        }
    }

    private class NoteViewHolder extends RecyclerView.ViewHolder {
        public TextView title, date, content;
        public CircleImageView image;

        public NoteViewHolder(View view) {
            super(view);
            content = (TextView) view.findViewById(R.id.content);
            title = (TextView) view.findViewById(R.id.title);
            date = (TextView) view.findViewById(R.id.date);
            image=(CircleImageView)view.findViewById(R.id.imageView1);
        }
    }

    public Search_Adapter(Context context, List<SQLHelper.SearchNode> moviesList) {
        this.nodesList = moviesList;
        this.context = context;
        tfsiavash = Typeface.createFromAsset(context.getAssets(), "fonts/iransanse.ttf");
        tfyekan = Typeface.createFromAsset(context.getAssets(), "fonts/yekan.ttf");
        tfnazanin = Typeface.createFromAsset(context.getAssets(), "fonts/nazanin.ttf");
        //tftraffic = Typeface.createFromAsset(context.getAssets(), "fonts/traffic.ttf");

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        switch (viewType){
            case EXPERT_TYPE_NORMAL:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.search_row_normal, parent, false);
                return new NoramlViewHolder(itemView);
            case EXPERT_TYPE_NOTE:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.search_row_note, parent, false);
                return new NoteViewHolder(itemView);

        }
        return null;
    }

    @Override
    public void onViewDetachedFromWindow(RecyclerView.ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.itemView.clearAnimation();
    }



    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int pos=holder.getAdapterPosition();
        SQLHelper.SearchNode movie = nodesList.get(pos);

        switch (movie.type){
            case SQLHelper.SearchNode
                    .TYPE_DATE:
                ((NoramlViewHolder)holder).title.setText(movie.content+" "+movie.title);
                ((NoramlViewHolder)holder).date.setText(movie.date);
                if (movie.image != null) {
                    ((NoramlViewHolder) holder).image.setImageBitmap(BitmapFactory
                            .decodeByteArray(movie.image, 0, movie.image.length));
                    //holder.image.setBackgroundColor(Color.BLACK);
                } else {
                    ((NoramlViewHolder) holder).image.setImageResource(R.drawable.blank_user);
                    //holder.image.setBackgroundColor(Color.TRANSPARENT);
                }


                //((NoteViewHolder)holder).field.setTypeface(tfnazanin);

                if (animationEnabled){
                    Animation animation = AnimationUtils.loadAnimation(context,
                            (pos > lastPosition) ? R.anim.up_from_bottom
                                    : R.anim.down_from_top);
                    holder.itemView.startAnimation(animation);
                }

                lastPosition = pos;

                break;


            case SQLHelper.SearchNode
                    .TYPE_NOTE:
                ((NoteViewHolder)holder).title.setText(movie.title);
                ((NoteViewHolder)holder).content.setText(movie.content);
                ((NoteViewHolder)holder).date.setText(movie.date);

                if (animationEnabled){
                    Animation animation = AnimationUtils.loadAnimation(context,
                            (pos > lastPosition) ? R.anim.up_from_bottom
                                    : R.anim.down_from_top);
                    holder.itemView.startAnimation(animation);
                }

                lastPosition = pos;

                break;
        }

    }

    public void setAnimationEnabled(boolean _anim){
        animationEnabled=_anim;
        Log.e("enabledanim","anim"+animationEnabled);

    }


    @Override
    public int getItemCount() {
        return nodesList.size();
    }

    @Override
    public int getItemViewType(int position) {

                return nodesList.get(position).type;
    }

    public void searchByString(String filter){
        SQLHelper cdbo = new SQLHelper(context);

        cdbo.openDb();
        nodesList.clear();
        nodesList=cdbo.searchByString(filter);
        Log.e(""+nodesList.size(),nodesList.toString());
        cdbo.closeDb();
        notifyDataSetChanged();

    }
}
