package afb.elan.demo;

import java.util.ArrayList;


import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.w3c.dom.Text;

import afb.elan.demo.Db.SQLHelper;
import afb.elan.demo.Services.SnoozeReceiver;
import afb.elan.demo.calendar.SolarDate;

public class PopupBirthday extends AppCompatActivity implements OnItemClickListener, OnClickListener{
	
	String currentSolarDay=null;
	ArrayList<SQLHelper.SearchNode> currentList;
	//ArrayList<String> currentLabels;
	RecyclerView recyclerView;
	Search_Adapter nodesAdapter;
	MediaPlayer mp;
	boolean withSnooze=false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		/*withSnooze=getIntent().hasExtra("withSnooze");
		if (withSnooze)
		  {
			setContentView(R.layout.popup_birthday_with_snooze);
			Button bSnooze=(Button)findViewById(R.id.button_snooze);
			bSnooze.setOnClickListener(this);
		  }
		else
			setContentView(R.layout.popup_birthday_panel);


		
		//currentLabels=new ArrayList<String>();
		
		mp = MediaPlayer.create(this , R.raw.chirst);
		Bundle bundle=getIntent().getExtras();
		if (bundle!=null){
			currentSolarDay=getIntent().getStringExtra("currentSolarDay");
			fetchListFromDb();
			if (getIntent().getBooleanExtra("makeSound",true))
			       mp.start();
			setTitle(getIntent().getStringExtra("title"));
			
			if (getIntent().hasExtra("fromWidget_tomorrow"))
				Toast.makeText(this, getResources().getString(R.string.notificationTomorrowBody), Toast.LENGTH_SHORT).show();
		}*/
		requestWindowFeature(Window.FEATURE_NO_TITLE); //before
		getWindow().getAttributes().format = PixelFormat.TRANSLUCENT;
		setContentView(R.layout.popup_birthday_panel);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			Window window = getWindow();
			window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			window.setStatusBarColor(getResources().getColor(R.color.orange));
		}

		currentList=new ArrayList<SQLHelper.SearchNode>();
		SolarDate sd = SolarDate.newInstance(SolarDate.DATE_KIND_SOLAR);
		currentSolarDay = sd.getMMDD();
		SharedPreferences shp = PreferenceManager
				.getDefaultSharedPreferences(getApplicationContext());
		boolean sound = shp.getBoolean("prefSoundEnabled", true);
		fetchListFromDb();
		if (sound){
			mp = MediaPlayer.create(this , R.raw.chirst);
			mp.start();

		}
		((TextView)findViewById(R.id.tv_title)).setText("لیست رویدادهای امروز");

		

		/*
		else
			{
			BirthdayRecord br=new BirthdayRecord();
			br.setContent(1, "ata", "fallah", "1391/01/01", null, null, null, null, "091123456789", null);
			currentList.add(br);			
			}
		*/


		setupViews();
	}
	
	
	private void fetchListFromDb(){
		currentList.clear();
		//currentLabels.clear();
		SharedPreferences shp = PreferenceManager
				.getDefaultSharedPreferences(this);
		boolean sendSms = shp
				.getBoolean("prefSmsSendEnabled", true);
		SQLHelper cdbo=new SQLHelper(this);
		/*cdbo.openDb();
		cdbo.fetchTodayBirthdays(currentSolarDay);
		BirthdayRecord br=cdbo.readNext();
		if (sendSms)
			while (br.getId()!=-1){
				currentList.add(br);
				//currentLabels.add(br.getDateDiscription(PopupBirthday.this)+" "+br.getSmsStatusForPopup(PopupBirthday.this));
				br=cdbo.readNext();
			}
		else
			while (br.getId()!=-1){
				currentList.add(br);
				//currentLabels.add(br.getDateDiscription(PopupBirthday.this));
				br=cdbo.readNext();
			}			
		
		cdbo.closeDb();*/

		cdbo.openDb();
		currentList=cdbo.fetchTodayNodes(currentSolarDay);
		cdbo.closeDb();


	}

	
	@SuppressWarnings("unchecked")
	private void setupViews()
	{
		recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

		nodesAdapter=new Search_Adapter(this,currentList);
		nodesAdapter.setAnimationEnabled(false);
		//mAdapter.setHasStableIds(true);


		recyclerView.setHasFixedSize(true);
		//mLayoutManager = new LinearLayoutManager(SearchActivity.this, LinearLayoutManager.HORIZONTAL, false);
		// = new GridLayoutManager(SearchActivity.this,1,LinearLayoutManager.HORIZONTAL,false);
		LinearLayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
		recyclerView.setLayoutManager(mLayoutManager);
		recyclerView.setAdapter(nodesAdapter);
		recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this, recyclerView, new RecyclerTouchListener.ClickListener() {
			@Override
			public void onClick(View view, int position) {


				if (nodesAdapter.nodesList.get(position).type== SQLHelper.SearchNode.TYPE_DATE){
					Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("elanapp://afb.elan.demo/date/?id="+nodesAdapter.nodesList.get(position).id));
					startActivity(browserIntent);
				}
				else
				if (nodesAdapter.nodesList.get(position).type== SQLHelper.SearchNode.TYPE_NOTE){
					Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("elanapp://afb.elan.demo/note/?id="+nodesAdapter.nodesList.get(position).id));
					startActivity(browserIntent);
				}




			}

			@Override
			public void onLongClick(View view, int position) {




			}
		}));

		recyclerView.setAdapter(nodesAdapter);


		ImageView bReturn=(ImageView)findViewById(R.id.im_close);
		bReturn.setOnClickListener(this);
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int pos1, long pos2) {
		// TODO Auto-generated method stub
		
		if (currentList.get(pos1).type== SQLHelper.SearchNode.TYPE_DATE){
			Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("elanapp://afb.elan.demo/date/?id="+currentList.get(pos1).id));
			startActivity(browserIntent);
		}
		else
		if (currentList.get(pos1).type== SQLHelper.SearchNode.TYPE_NOTE){
			Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("elanapp://afb.elan.demo/note/?id="+currentList.get(pos1).id));
			startActivity(browserIntent);
		}

		//cdbb.closeDb();
		
		//Intent commingi=new Intent(this,InfoDialog.class);
		//startActivity(commingi);


	}


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId())
		{
		case R.id.im_close:
			finish();
			break;
		case R.id.button_snooze:
			Intent intent = new Intent(this, SnoozeReceiver.class);
			intent.setAction(SnoozeReceiver.actionString);
			intent.putExtra("currentSolarDay", currentSolarDay);
		    PendingIntent pendingIntent = PendingIntent.getBroadcast(this.getApplicationContext(), 234324243, intent, 0);
		    AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
		    alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()
		        + (600000), pendingIntent);
		    Toast.makeText(this, getResources().getString(R.string.snoozeDone),
		        Toast.LENGTH_LONG).show();
		    finish();
			break;
		}
		
	}


	@Override
	protected void onNewIntent(Intent intent) {
		// TODO Auto-generated method stub
		super.onNewIntent(intent);
		finish();
		startActivity(intent);
	}
	
	/*
	class MyArrayAdapter extends ArrayAdapter {

		@SuppressWarnings("unchecked")
		public MyArrayAdapter(Context context) {
			super(PopupBirthday.this, android.R.layout.simple_list_item_1,android.R.id.text1, currentList);
			// TODO Auto-generated constructor stub
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			// return super.getView(position, convertView, parent);
			if (convertView == null) {
				LayoutInflater li = getLayoutInflater();
				convertView = li.inflate(android.R.layout.simple_list_item_1,
						null);
			}
			TextView tv=(TextView)findViewById(android.R.id.text1);
			tv.setText(currentList.get(position).getDateDiscription(PopupBirthday.this));
			return convertView;
		}

	}
	*/
	
	
	
	
}
