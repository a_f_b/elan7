package afb.elan.demo.Services.scheduler;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import afb.elan.demo.Db.SQLHelper;
import afb.elan.demo.MyNotification;
import afb.elan.demo.R;
import afb.elan.demo.Services.DemoReceiver;
import afb.elan.demo.Services.DemoService;
import afb.elan.demo.calendar.SolarDate;

/**
 * JobService to be scheduled by the JobScheduler.
 * start another service
 */
public class TestJobService extends JobService {
    private static final String TAG = "SyncService";
    DemoReceiver dmr=null;
    public boolean registered=false;
    IntentFilter filter;


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public boolean onStartJob(JobParameters params) {
        /*Intent service = new Intent(getApplicationgetApplicationContext()(), DemoService.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            getApplicationgetApplicationContext()().startForegroundService(service);
        } else {
            getApplicationgetApplicationContext()().startService(service);
        }*/
        Log.e("jobscheduler", "jobscheduler.onStartJob()");
        //checkReceiver();
        onReceive();
        Util.scheduleJob(getApplicationContext()); // reschedule the job
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        /*try {
            unregisterReceiver(dmr);
            Intent restartServiceIntent = new Intent(getApplicationgetApplicationContext()(),this.getClass());
            restartServiceIntent.setPackage(getPackageName());
            startService(restartServiceIntent);
            dmr=null;
            Log.e("jobscheduler", "jobscheduler.onStopJob()");
        }catch (IllegalArgumentException e2){

        }*/

        return true;
    }


    static final String LOGGING_TAG = "MyDemo";
    String currentSolarDay = null, tommorowSolarDay=null;;
    ArrayList<SQLHelper.SearchNode> currentList;
    int time_to_popup_hour = 12, time_to_popup_minute=0;
    boolean sound,showToday,showTomorrow,needCheck=true;
    public final static int BROADCAST_SEND_SMS=0x1234322;
    public final static int BROADCAST_NOTIFY_TODAY=0x1234323;


    Handler h = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            super.handleMessage(msg);
            switch (msg.what){
                case BROADCAST_SEND_SMS:  //send sms

                    for (int i=0;i<currentList.size();i++)
                    {
                        if (currentList.get(i).type!= SQLHelper.SearchNode.TYPE_DATE) continue;

                        final int j=i;
                        if (currentList.get(j).sms.length()>0){
                            Thread th=new Thread(new Runnable() {

                                @Override
                                public void run() {
                                    // TODO Auto-generated method stub

                                    SmsManager sms = SmsManager.getDefault();
                                    String SENT = "SMS_SENT";
                                    String DELIVERED = "ELAN_SMS_DELIVERED";

                                    String addresses[]=currentList.get(j).mobile.split(";");
                                    int i=100;
                                    SharedPreferences shp = PreferenceManager
                                            .getDefaultSharedPreferences(getApplicationContext());
                                    boolean deliverySms = shp
                                            .getBoolean("prefSmsDeliveryEnabled", true);
                                    for (String cs:addresses)
                                    {
                                        ContentValues values = new ContentValues();
                                        values.put("address", cs);
                                        Calendar c = Calendar.getInstance();
                                        values.put("date", c.getTimeInMillis());
                                        values.put("read", 1);
                                        values.put("status", -1);
                                        values.put("type", 2);
                                        values.put("body", currentList.get(j).sms);
                                        Log.d("sms", "sms");
                                        Uri inserted = getApplicationContext().getContentResolver().insert(
                                                Uri.parse("content://sms"), values);
                                        PendingIntent sentPI=null;
                                        PendingIntent deliveredPI = null;
                                        if (deliverySms){
                                            sentPI = PendingIntent.getBroadcast(getApplicationContext(),
                                                    i, new Intent(SENT), PendingIntent.FLAG_UPDATE_CURRENT);
                                            Intent di=new Intent(DELIVERED);
                                            di.putExtra("addedUri", inserted.toString());
                                            deliveredPI = PendingIntent.getBroadcast(getApplicationContext(), i, di, PendingIntent.FLAG_UPDATE_CURRENT);
                                        }
                                        sms.sendTextMessage(cs, null, currentList.get(j).sms, sentPI, deliveredPI);
                                        i++;
                                    }










                                }
                            });
                            th.run();
                        }



                    }


                {
                    String title=getApplicationContext().getResources().getString(R.string.app_name);
                    String body=getApplicationContext().getResources().getString(R.string.notificationSmsSent);

                    NotificationManager nm = (NotificationManager) getApplicationContext()
                            .getSystemService(getApplicationContext().NOTIFICATION_SERVICE);

                    PendingIntent pi = PendingIntent.getActivity(getApplicationContext(), 222222,
                            new Intent(), PendingIntent.FLAG_ONE_SHOT);
                    Notification n = new Notification(R.drawable.ic_launcher_foreground,
                            getApplicationContext().getResources().getString(R.string.app_name), System.currentTimeMillis());

                    n.defaults = Notification.DEFAULT_ALL;
                    n.flags |= Notification.FLAG_AUTO_CANCEL;
                    nm.notify(0x265267, n);
                }

                break;


            }

        }
    };


    public void onReceive() {
        //if (intent.getAction().equals(DemoService.AFB_ELAN_STUDIO_DEMORECEIVER)) {
        Calendar c = Calendar.getInstance();

        currentList = new ArrayList<SQLHelper.SearchNode>();
        SharedPreferences shp = PreferenceManager
                .getDefaultSharedPreferences(getApplicationContext());
        sound = shp.getBoolean("prefSoundEnabled", true);
        showToday = shp.getBoolean("prefShowTodayListEnabled", true);
        time_to_popup_hour=shp.getInt("defaultTimeHour",12);
        time_to_popup_minute=shp.getInt("defaultTimeMinute",00);
        needCheck=shp.getBoolean("needCheck", true);

        String hour=""+c.get(Calendar.HOUR_OF_DAY);
        if (hour.length()<2) hour="0"+hour;
        String min=""+c.get(Calendar.MINUTE);
        if (min.length()<2) min="0"+min;
        Log.e("DemoR",hour + ":"+min);
        //Toast.makeText(getApplicationContext(),hour + ":"+min,Toast.LENGTH_SHORT).show();


            if (time_to_popup_hour==c.get(Calendar.HOUR_OF_DAY))
                if (time_to_popup_minute==c.get(Calendar.MINUTE)) {
                    if (needCheck) {
                        SharedPreferences.Editor editor = shp.edit();
                        editor.putBoolean("needCheck",false);
                        editor.commit();
                        needCheck=false;
                        SolarDate sd = SolarDate.newInstance(SolarDate.DATE_KIND_SOLAR);
                        currentSolarDay = sd.getMMDD();

                        //////////////fetchListFromDb()//////////////
                        currentList.clear();
                        SQLHelper cdbo = new SQLHelper(getApplicationContext());
                        cdbo.openDb();
                        currentList=cdbo.fetchTodayNodes(currentSolarDay);
                        cdbo.closeDb();
                        Log.e("DemoR","checklist");
                        Log.e("showToday",showToday+"");
                        Log.e("currentList",currentList.size()+"");

                        if ((showToday) && (currentList.size() > 0)) {

                            boolean sendSms = shp
                                    .getBoolean("prefSmsSendEnabled", false);
                            String showType=shp.getString("prefShowTodayListType", "1");
                            if (showType.equals("1")){
                                Log.e("list",currentList.get(0).toString());

                                Vibrator vibrate = (Vibrator) this.getApplicationContext()
                                        .getSystemService(getApplicationContext().VIBRATOR_SERVICE);
                                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("elanapp://afb.elan.demo/popup/?id="+sd.toString()));
                                startActivity(browserIntent);
                                vibrate.vibrate(1500);
                            }
                            else
                            {
                                Log.e("notif",currentList.get(0).toString());
                                boolean sound = shp.getBoolean("prefSoundEnabled", true);
                                if (sound){
                                    MediaPlayer mp = MediaPlayer.create(this , R.raw.chirst);
                                    mp.start();
                                }
                                for (int i=0;i<currentList.size();i++){
                                    MyNotification mn=new MyNotification(getApplicationContext());
                                    mn.showNotification(currentList.get(i),1010+i);

                                }

                            }
                            if (sendSms) {
                                Message msg = new Message();
                                msg.what = BROADCAST_SEND_SMS;
                                this.h.sendMessage(msg);

                            }

                        }



                    }
                }
                else
                    if (!needCheck){
                        SharedPreferences.Editor editor = shp.edit();
                        editor.putBoolean("needCheck",true);
                        editor.commit();

                    };

        //}

    }



}
