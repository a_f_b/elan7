package afb.elan.demo.Services;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import afb.elan.demo.R;

public class DeliverReceiver extends BroadcastReceiver{

	@Override
	public void onReceive(Context context, Intent in) {
		// TODO Auto-generated method stub    
		switch (getResultCode())
            {
                case Activity.RESULT_OK:
                    Toast.makeText(context, context.getResources().getString(R.string.smsDelivered),
                            Toast.LENGTH_SHORT).show();
					ContentValues values = new ContentValues();					
					values.put("status", 1);
					Log.d("sms", "delivered");
					context.getContentResolver().update(
							Uri.parse(in.getExtras().getString("addedUri")), values,null,null);
                    break;
                case Activity.RESULT_CANCELED:
                    Toast.makeText(context, context.getResources().getString(R.string.smsNotDelivered), 
                            Toast.LENGTH_SHORT).show();
                    break;                        
            }
        
	}
	
	

}
