package afb.elan.demo.Services;

import java.util.ArrayList;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Vibrator;
import android.preference.PreferenceManager;

import afb.elan.demo.Db.SQLHelper;
import afb.elan.demo.R;

public class SnoozeReceiver extends BroadcastReceiver {
	String currentSolarDay = null;
	ArrayList<SQLHelper.SearchNode> currentList;
	Context context;
	boolean sound;
	public final static String actionString="afb.elan.demo.snooze";



	@Override
	public void onReceive(Context context, Intent intent) {
		this.context=context;
		if (intent.getAction().compareTo(actionString) == 0) {

			SharedPreferences shp = PreferenceManager
					.getDefaultSharedPreferences(context);
			sound = shp.getBoolean("prefSoundEnabled", true);



			currentSolarDay=intent.getExtras().getString("currentSolarDay");
			currentList.clear();
			SQLHelper cdbo = new SQLHelper(context);
			cdbo.openDb();
			currentList=cdbo.fetchTodayNodes(currentSolarDay);
			cdbo.closeDb();





						Vibrator vibrate = (Vibrator) this.context
								.getSystemService(Context.VIBRATOR_SERVICE);
						Intent i = new Intent();
						i.setClassName("afb.elan.demo",
								"afb.elan.demo.PopupBirthday");
						i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						i.putExtra("currentSolarDay", currentSolarDay);
						i.putExtra("makeSound", sound);
						i.putExtra("withSnooze", true);
						i.putExtra("title", "یادآوری رویدادهای "+currentSolarDay);
						context.startActivity(i);
						vibrate.vibrate(1500);





			
		}

	}




}
