package afb.elan.demo.Services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import afb.elan.demo.Services.DemoService;

public class StartupReceiver extends BroadcastReceiver {

	public StartupReceiver(){
		
	}
	
	@Override
	public void onReceive(Context context, Intent intent) {
		
		//if (intent.getAction().compareTo(Intent.ACTION_BOOT_COMPLETED) == 0) {
			//Log.v(LOGGING_TAG, "DemoReceiver.onReceive(ACTION_BOOT_COMPLETED)");
			//SharedPreferences shp=PreferenceManager.getDefaultSharedPreferences(context);
			//boolean b=shp.getBoolean("prefServiceAutoStart", true);
			//if (b)
				context.startService(new Intent(context, DemoService.class));
		//}

	}

}
