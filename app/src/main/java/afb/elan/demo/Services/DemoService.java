package afb.elan.demo.Services;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.util.Log;

import afb.elan.demo.MainActivity;
import afb.elan.demo.R;

public class DemoService extends Service {
	 static final String LOGGING_TAG = "MyDemo";
	DemoReceiver dmr;
	public boolean registered=false;
	IntentFilter filter;
	 @Override
	 public IBinder onBind(Intent intent) {
		 Log.e(LOGGING_TAG, "DemoService.onBind()");
		 //startAlarm();
		 return null;
	 }
	 
	 
	 
	 @Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
		Log.e(LOGGING_TAG, "DemoService.onStartCommand()");
		//onTaskRemoved(intent);
		startAlarm();
		return START_STICKY;
	}



	 @Override
	 public void onCreate() {
		 super.onCreate();
		 Log.e(LOGGING_TAG, "DemoService.onCreate()");
		 //startForeground(1,new Notification());
		 //startAlarm();
	 }

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		Log.e(LOGGING_TAG, "DemoService.onDestroy()");
		try {
			restartService();
		}catch (IllegalArgumentException e2){

		}

		super.onDestroy();
	}

	@Override
	public void onLowMemory() {
		super.onLowMemory();
	}

	@Override
	public void onTaskRemoved(Intent rootIntent) {
		Log.e(LOGGING_TAG, "DemoService.onTaskRemoved()");

		try {
			unregisterReceiver(dmr);
			Intent restartServiceIntent = new Intent(getApplicationContext(),this.getClass());
			restartServiceIntent.setPackage(getPackageName());
			startService(restartServiceIntent);
		}catch (IllegalArgumentException e2){

		}
		super.onTaskRemoved(rootIntent);
	}

	private void startAlarm() {
		try {
			try {
				unregisterReceiver(dmr);
			}catch (IllegalArgumentException e2){

			}
			dmr=new DemoReceiver(this);
			filter=createIntentFilter();
			registerReceiver(
					dmr,
					filter);
			registered=true;
			/*if (timer!=null)
				timer.cancel();
			timer = new Timer();
			timer.scheduleAtFixedRate(new TimerTask() {

				@Override
				public void run() {
					// Do your task
					Intent i=new Intent(AFB_ELAN_STUDIO_DEMORECEIVER);
					sendBroadcast(i);

				}

			}, 0, 10000);*/

		} catch(IllegalArgumentException e) {

			e.printStackTrace();
			try {
				unregisterReceiver(dmr);
				registerReceiver(
						dmr,
						filter);
			}catch (IllegalArgumentException e2){

			}
		}
	}

	public static final String AFB_ELAN_STUDIO_DEMORECEIVER = "afb.elan.demo.DEMORECEIVER";
	public static final String AFB_ELAN_STUDIO_RESTART_SERVICE = "afb.elan.demo.RESETSERV";

	private static IntentFilter createIntentFilter() {
		IntentFilter filter = new IntentFilter();
		filter.addAction(AFB_ELAN_STUDIO_DEMORECEIVER);
		filter.addAction(Intent.ACTION_TIME_TICK);
		return filter;
	}

	private void restartService(){
		Intent intent=new Intent(DemoService.AFB_ELAN_STUDIO_RESTART_SERVICE);
		sendBroadcast(intent);
		unregisterReceiver(dmr);

	}





}
