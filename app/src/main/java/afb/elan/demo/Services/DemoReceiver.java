package afb.elan.demo.Services;

import java.util.ArrayList;
import java.util.Calendar;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

import afb.elan.demo.Db.SQLHelper;
import afb.elan.demo.MyNotification;
import afb.elan.demo.PopupBirthday;
import afb.elan.demo.R;
import afb.elan.demo.calendar.SolarDate;

public class DemoReceiver extends BroadcastReceiver {
	static final String LOGGING_TAG = "MyDemo";
	String currentSolarDay = null, tommorowSolarDay=null;;
	ArrayList<SQLHelper.SearchNode> currentList;
	Context context;
	int time_to_popup_hour = 12, time_to_popup_minute=0;
	boolean sound,showToday,showTomorrow,needCheck=true;
	public final static int BROADCAST_SEND_SMS=0x1234322;
	public final static int BROADCAST_NOTIFY_TODAY=0x1234323;


	Handler h = new Handler() {
		@Override
		public void handleMessage(Message msg) {

			super.handleMessage(msg);
			switch (msg.what){
			case 0x1234321:  // notify tomorrow list
				try {
					Intent intent = new Intent();
					intent.setClassName("afb.elan.demo",
							"afb.elan.demo.PopupBirthday");
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					intent.putExtra("currentSolarDay", (String) msg.obj);
					intent.putExtra("makeSound", false);
					
					// intent.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);

					SolarDate sd = SolarDate.newInstance(SolarDate.DATE_KIND_SOLAR);
					sd.appendDay((short) 1);
					intent.putExtra("title", sd.toString());
					
					NotificationManager nm = (NotificationManager) context
							.getSystemService(Context.NOTIFICATION_SERVICE);

					PendingIntent pi = PendingIntent.getActivity(context, 111111,
							intent, PendingIntent.FLAG_UPDATE_CURRENT);
					Notification n = new Notification(R.drawable.converter_row,
							context.getResources().getString(R.string.app_name), System.currentTimeMillis());
					n.defaults = Notification.DEFAULT_ALL;
					n.flags |= Notification.FLAG_AUTO_CANCEL;
					nm.notify(0x235266, n);

				} catch (Exception e) {
					e.printStackTrace();
				}
				break;
				
			case BROADCAST_SEND_SMS:  //send sms
				
				for (int i=0;i<currentList.size();i++)
				{
					if (currentList.get(i).type!= SQLHelper.SearchNode.TYPE_DATE) continue;
					
					final int j=i;
					if (currentList.get(j).sms.length()>0){
						  Thread th=new Thread(new Runnable() {
								
								@Override
								public void run() {
									// TODO Auto-generated method stub
									
									SmsManager sms = SmsManager.getDefault();
									String SENT = "SMS_SENT";
									String DELIVERED = "ELAN_SMS_DELIVERED";
									
									String addresses[]=currentList.get(j).mobile.split(";");
									int i=100;
									SharedPreferences shp = PreferenceManager
											.getDefaultSharedPreferences(context);
									boolean deliverySms = shp
											.getBoolean("prefSmsDeliveryEnabled", true);
									for (String cs:addresses)
									{
										ContentValues values = new ContentValues();
										values.put("address", cs);
										Calendar c = Calendar.getInstance();
										values.put("date", c.getTimeInMillis());
										values.put("read", 1);
										values.put("status", -1);
										values.put("type", 2);
										values.put("body", currentList.get(j).sms);
										Log.d("sms", "sms");
										Uri inserted = context.getContentResolver().insert(
												Uri.parse("content://sms"), values);
										PendingIntent sentPI=null;
										PendingIntent deliveredPI = null;
										if (deliverySms){
											sentPI = PendingIntent.getBroadcast(context,
													i, new Intent(SENT), PendingIntent.FLAG_UPDATE_CURRENT);
											Intent di=new Intent(DELIVERED);
											di.putExtra("addedUri", inserted.toString());
											deliveredPI = PendingIntent.getBroadcast(context, i, di, PendingIntent.FLAG_UPDATE_CURRENT);
										}										
										sms.sendTextMessage(cs, null, currentList.get(j).sms, sentPI, deliveredPI);
										i++;
									}
									
									


									
									
									

									

								}
							});
							th.run();
					}
					

				
				}
			
				
				{
				String title=context.getResources().getString(R.string.app_name);
				String body=context.getResources().getString(R.string.notificationSmsSent);

				NotificationManager nm = (NotificationManager) context
						.getSystemService(Context.NOTIFICATION_SERVICE);

				PendingIntent pi = PendingIntent.getActivity(context, 222222,
						new Intent(), PendingIntent.FLAG_ONE_SHOT);
				Notification n = new Notification(R.drawable.ic_launcher_foreground,
						context.getResources().getString(R.string.app_name), System.currentTimeMillis());
				
				n.defaults = Notification.DEFAULT_ALL;
				n.flags |= Notification.FLAG_AUTO_CANCEL;
				nm.notify(0x265267, n);
				}
				
				break;
				
			case BROADCAST_NOTIFY_TODAY:  // notify today list
				try {
					Intent intent = new Intent();
					intent.setClassName("afb.elan.demo",
							"afb.elan.demo.PopupBirthday");
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					intent.putExtra("currentSolarDay", (String) msg.obj);
					intent.putExtra("makeSound", false);
					intent.putExtra("withSnooze", true);
					// intent.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);

					SolarDate sd = SolarDate.newInstance(SolarDate.DATE_KIND_SOLAR);
					intent.putExtra("title", sd.toString());
					

					NotificationManager nm = (NotificationManager) context
							.getSystemService(Context.NOTIFICATION_SERVICE);

					PendingIntent pi = PendingIntent.getActivity(context, 333333,
							intent, PendingIntent.FLAG_UPDATE_CURRENT);
					Notification n = new Notification(R.drawable.ic_launcher_foreground,
							context.getResources().getString(R.string.app_name), System.currentTimeMillis());
					n.defaults = Notification.DEFAULT_ALL;
					n.flags |= Notification.FLAG_AUTO_CANCEL;
					nm.notify(0x265268, n);
					if (sound){
						//MediaPlayer mp = MediaPlayer.create(context , R.raw.chirst);
						//mp.start();
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
				break;	
				
			}

		}
	};



	public DemoReceiver(Context context) {

		this.context = context;
		Calendar c = Calendar.getInstance();

		SolarDate sd = SolarDate.newInstance(SolarDate.DATE_KIND_GEORGIAN);
		currentSolarDay = sd.getMMDD().toString();
		sd.appendDay((short) 1);
		tommorowSolarDay = sd.getMMDD();
		currentList = new ArrayList<SQLHelper.SearchNode>();
		SharedPreferences shp = PreferenceManager
				.getDefaultSharedPreferences(context);
		sound = shp.getBoolean("prefSoundEnabled", true);
		showToday = shp.getBoolean("prefShowTodayListEnabled", true);
		time_to_popup_hour=shp.getInt("defaultTimeHour",12);
		time_to_popup_minute=shp.getInt("defaultTimeMinute",00);
		//fetchListFromDb();

	}

	@Override
	public void onReceive(Context context, Intent intent) {
			//if (intent.getAction().equals(DemoService.AFB_ELAN_STUDIO_DEMORECEIVER)) {
		    if (intent.getAction().equals(DemoService.AFB_ELAN_STUDIO_RESTART_SERVICE)){
				context.startService(new Intent(context,DemoService.class));
				return;

			}
			if (intent.hasExtra("needCheck"))
			{
				Log.e("DemoR","needCheck");
				needCheck=intent.getBooleanExtra("needCheck",true);
				SharedPreferences shp = PreferenceManager
						.getDefaultSharedPreferences(context);
				sound = shp.getBoolean("prefSoundEnabled", true);
				showToday = shp.getBoolean("prefShowTodayListEnabled", true);
				time_to_popup_hour=shp.getInt("defaultTimeHour",12);
				time_to_popup_minute=shp.getInt("defaultTimeMinute",00);
				return;
			}
			Calendar c = Calendar.getInstance();
			String hour=""+c.get(Calendar.HOUR_OF_DAY);
			if (hour.length()<2) hour="0"+hour;
			String min=""+c.get(Calendar.MINUTE);
			if (min.length()<2) min="0"+min;
			Log.e("DemoR",hour + ":"+min);
			Toast.makeText(context,hour + ":"+min,Toast.LENGTH_SHORT).show();


			if (needCheck)
			if (time_to_popup_hour==c.get(Calendar.HOUR_OF_DAY))
			if (time_to_popup_minute==c.get(Calendar.MINUTE)){
				needCheck=false;


				SolarDate sd = SolarDate.newInstance(SolarDate.DATE_KIND_SOLAR);
				currentSolarDay = sd.getMMDD();

				//////////////fetchListFromDb()//////////////
				currentList.clear();
				SQLHelper cdbo = new SQLHelper(context);
				cdbo.openDb();
				currentList=cdbo.fetchTodayNodes(currentSolarDay);
				cdbo.closeDb();
				Log.e("DemoR","checklist");
				Log.e("showToday",showToday+"");
				Log.e("currentList",currentList.size()+"");

				if ((showToday) && (currentList.size() > 0)) {

					SharedPreferences shp = PreferenceManager
							.getDefaultSharedPreferences(context);
					boolean sendSms = shp
							.getBoolean("prefSmsSendEnabled", false);
					String showType=shp.getString("prefShowTodayListType", "2");
					if (showType.equals("1")){
						Log.e("list",currentList.get(0).toString());

						Vibrator vibrate = (Vibrator) this.context
								.getSystemService(Context.VIBRATOR_SERVICE);
						Intent i = new Intent();
						i.setClassName("afb.elan.demo", "afb.elan.demo.PopupBirthday");
						i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						i.putExtra("currentSolarDay", currentSolarDay);
						i.putExtra("makeSound", sound);
						i.putExtra("withSnooze", true);
						i.putExtra("title", context.getResources().getString(R.string.notificationTodayBody)+" "+sd.toString());
						context.startActivity(i);
						vibrate.vibrate(1500);
					}
					else
					{
						/*Message msg = new Message();
						msg.what = BROADCAST_NOTIFY_TODAY;
						msg.obj=currentSolarDay;
						this.h.sendMessage(msg);*/
						Log.e("notif",currentList.get(0).toString());

						for (int i=0;i<currentList.size();i++){
							MyNotification mn=new MyNotification(context);
							mn.showNotification(currentList.get(i),1010+i);

						}
						
					}
					if (sendSms) {
						Message msg = new Message();
						msg.what = BROADCAST_SEND_SMS;
						//this.h.sendMessage(msg);

					}

				}

			}
			else needCheck=true;

		//}

	}




}
