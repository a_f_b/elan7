package afb.elan.demo;

import android.Manifest;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.widget.Toolbar;

import android.os.Handler;
import android.util.Base64;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Menu;

import com.google.android.material.navigation.NavigationView;

import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;

import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.getbase.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


import afb.elan.demo.Db.NoteObject;
import afb.elan.demo.Services.DemoService;
import afb.elan.demo.classes.Utils;
import afb.elan.demo.login.LoginActivity;
import afb.elan.demo.Services.scheduler.Util;
import afb.elan.demo.ui.DatesFragment;
import afb.notes.studio.NoteEditor;
import afb.notes.studio.NotesFragment;
import dev.shreyaspatil.MaterialDialog.BottomSheetMaterialDialog;
import smartdevelop.ir.eram.showcaseviewlib.GuideView;
import smartdevelop.ir.eram.showcaseviewlib.config.DismissType;
import smartdevelop.ir.eram.showcaseviewlib.listener.GuideListener;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    DrawerLayout drawerLayout;
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private FCViewPager viewPager;
    FloatingActionButton fam;
    DatesFragment datesFragment;
    NotesFragment notesFragment;
    ActionBarDrawerToggle mDrawerToggle;
    NavigationView navigationView=null;
    public static final String LOGIN_WITH_EMAIL = "afb.elan.demo.LOGIN.EMAIL";
    public static final String ELAN_LOGOUT = "afb.elan.demo.LOGOUT";
    public static final String ELAN_FINISH = "afb.elan.demo.FINISH";
    public static final String ELAN_UPDATE_NOTE = "afb.elan.demo.UPDATE.NOTE";
    public static final String ELAN_INSERT_NOTE = "afb.elan.demo.INSERT.NOTE";
    public static final String ELAN_DELETE_NOTE = "afb.elan.demo.DELETE.NOTE";
    public static final String ELAN_UPDATE_DATE = "afb.elan.demo.UPDATE.DATE";
    public static final String ELAN_INSERT_DATE = "afb.elan.demo.INSERT.DATE";
    public static final String ELAN_DELETE_DATE = "afb.elan.demo.DELETE.DATE";
    private int showMode;
    private BaseActivityReceiver baseActivityReceiver = new BaseActivityReceiver();
    public static final IntentFilter INTENT_FILTER = createIntentFilter();
    private static IntentFilter createIntentFilter() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(LOGIN_WITH_EMAIL);
        filter.addAction(ELAN_FINISH);
        filter.addAction(ELAN_UPDATE_NOTE);
        filter.addAction(ELAN_INSERT_NOTE);
        filter.addAction(ELAN_DELETE_NOTE);
        filter.addAction(ELAN_UPDATE_DATE);
        filter.addAction(ELAN_INSERT_DATE);
        filter.addAction(ELAN_DELETE_DATE);
        return filter;
    }
    protected void registerBaseActivityReceiver() {
        registerReceiver(baseActivityReceiver, INTENT_FILTER);
    }
    protected void unRegisterBaseActivityReceiver() {
        unregisterReceiver(baseActivityReceiver);
    }


    public class BaseActivityReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(ELAN_FINISH)) {
                finish();
            }
            else
            if (intent.getAction().equals(LOGIN_WITH_EMAIL))
            {
                if (navigationView!=null){
                    View headerview = navigationView.getHeaderView(0);
                    String email ="";
                    email=intent.getStringExtra("email");
                    String session=intent.getStringExtra("session");

                    if (!email.isEmpty())
                    {
                        ((TextView)headerview.findViewById(R.id.tv_login)).setText(email);
                        SharedPreferences someData = getSharedPreferences("data", 0);
                        SharedPreferences.Editor editor = someData.edit();
                        editor.putString("add",Base64.encodeToString(email.getBytes(), Base64.URL_SAFE));
                        editor.putString("sessionkey",session);
                        editor.remove("code");
                        editor.commit();
                        isLogined=true;
                        loginedEmail=email;
                        Toast.makeText(MainActivity.this, getResources().getString(R.string.loginSuccess), Toast.LENGTH_SHORT).show();


                    }

                }

            }
            else
            if (intent.getAction().equals(ELAN_UPDATE_NOTE))
            {
                    Bundle basket=intent.getExtras();
                    NoteObject no= (NoteObject) basket.getSerializable("NoteObject");
                    notesFragment.refreshList(ELAN_UPDATE_NOTE,no);
            }
            else
            if (intent.getAction().equals(ELAN_INSERT_NOTE))
            {
                Bundle basket=intent.getExtras();
                NoteObject no= (NoteObject) basket.getSerializable("NoteObject");
                notesFragment.refreshList(ELAN_INSERT_NOTE,no);

            }
            if (intent.getAction().equals(ELAN_DELETE_NOTE))
            {
                Bundle basket=intent.getExtras();
                NoteObject no= (NoteObject) basket.getSerializable("NoteObject");
                notesFragment.refreshList(ELAN_DELETE_NOTE,no);

            }
            else
            if (intent.getAction().equals(ELAN_UPDATE_DATE))
            {
                datesFragment.reLoad();

            }
            else
            if (intent.getAction().equals(ELAN_INSERT_DATE))
            { datesFragment.reLoad(); }
            else
            if (intent.getAction().equals(ELAN_DELETE_DATE))
            { datesFragment.reLoad(); }

        }
    }

    @Override
    public void finish() {
        //if (guest)
        unRegisterBaseActivityReceiver();
        super.finish();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String languageToLoad  = "fa"; // your language
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        setContentView(R.layout.activity_main);

        navigationView = (NavigationView) findViewById(R.id.nav_view);

        navigationView.setNavigationItemSelectedListener(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // For navigation bar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        getSupportActionBar().setHomeButtonEnabled(true);
        //getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_TITLE | ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_HOME_AS_UP);
        drawerLayout=(DrawerLayout)findViewById(R.id.drawer_layout) ;
        mDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout,toolbar, R.string.app_name, R.string.app_name) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                //if(getSupportActionBar() != null)
                    //etSupportActionBar().setIcon(R.drawable.ic_action_home);
                mDrawerToggle.syncState();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                //if(getSupportActionBar() != null)
                    //getSupportActionBar().setIcon(R.drawable.ic_action_search);
                mDrawerToggle.syncState();

            }
        };
        drawerLayout.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        //startService(new Intent(this, MyPushListener.class));
        viewPager = (FCViewPager) findViewById(R.id.mviewpager);
        setupViewPager(viewPager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();

        fam = (FloatingActionButton) findViewById(R.id.fam);
        fam.setTag(1);

        fam.setOnClickListener(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) Util.scheduleJob(this); else
        startService(new Intent(getApplicationContext(),DemoService.class));
        //ContextCompat.startForegroundService(getApplicationContext(),new Intent(getApplicationContext(), DemoService.class));
        //Intent intent = new Intent(this, DemoService.class);
        //bindService(intent, m_serviceConnection, BIND_AUTO_CREATE);


        registerBaseActivityReceiver();

    }
    /*DemoService m_service;

    private ServiceConnection m_serviceConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            m_service = ((DemoService.MyBinder)service).getService();
        }

        public void onServiceDisconnected(ComponentName className) {
            m_service = null;
        }
    };*/


    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        datesFragment =new DatesFragment();
        datesFragment.setOnNotifySelectedItemChanged(new DatesFragment.OnNotifySelectedItemChanged() {
            @Override
            public void notifySelectedItemChanged(int selectedCount) {
                if (selectedCount==0){
                    hideOption(R.id.action_delete);
                    showOption(R.id.action_search);showOption(R.id.action_guide);
                }
                else
                if (isOptionVisibled(R.id.action_search)){
                    hideOption(R.id.action_search);hideOption(R.id.action_guide);
                    showOption(R.id.action_delete);
                }
                if (selectedCount==1)
                    showOption(R.id.action_edit);
                else
                    hideOption(R.id.action_edit);



            }
        });
        notesFragment =new NotesFragment();
        notesFragment.setOnNotifySelectedItemChanged(new DatesFragment.OnNotifySelectedItemChanged() {
            @Override
            public void notifySelectedItemChanged(int selectedCount) {
                if (selectedCount==0){
                    hideOption(R.id.action_delete);
                    showOption(R.id.action_search);showOption(R.id.action_guide);
                }
                else
                if (isOptionVisibled(R.id.action_search)){
                    hideOption(R.id.action_search);hideOption(R.id.action_guide);
                    showOption(R.id.action_delete);
                }
                if (selectedCount==1)
                    showOption(R.id.action_edit);
                else
                    hideOption(R.id.action_edit);



            }
        });





        adapter.addFrag(datesFragment, "منسابت ها");
        //adapter.addFrag(expert_fragment_horizontal, "افقی");
        adapter.addFrag(notesFragment, "یادداشت ها");
        viewPager.setOffscreenPageLimit(2);
        viewPager.setAdapter(adapter);
    }

    private boolean isLogined=false;
    private String loginedEmail="";
    private void setupTabIcons() {
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        if (navigationView!=null) {
            View headerview = navigationView.getHeaderView(0);
            SharedPreferences someData = getSharedPreferences("data", 0);
            String email=someData.getString("add","");
            email = new String(Base64.decode(email,Base64.URL_SAFE));
            showMode= someData.getInt("showMode",R.id.nav_row);
            if (showMode!=R.id.nav_row){
                navigationView.getMenu().findItem(R.id.nav_grid).setChecked(true);
            }

            if (!email.isEmpty() && Utils.isValidEmail(email)) {
                ((TextView)headerview.findViewById(R.id.tv_login)).setText(email);
                isLogined=true;
                loginedEmail=email;
            }

            ((TextView) headerview.findViewById(R.id.tvVersion)).setText(getString(R.string.app_name) + " " + BuildConfig.VERSION_CODE);
            ((TextView) headerview.findViewById(R.id.tv_login)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!isLogined){
                        Intent login=new Intent(MainActivity.this, LoginActivity.class);
                        startActivity(login);

                    }
                    else
                    {
                        AlertDialog.Builder builder = (android.os.Build.VERSION.SDK_INT < 11) ? new AlertDialog.Builder(
                                MainActivity.this) : new AlertDialog.Builder(MainActivity.this,
                                AlertDialog.THEME_HOLO_LIGHT);

                        AlertDialog ad = builder.create();
                        ad.setMessage(getResources().getString(R.string.logOutMessage));
                        ad.setTitle("");
                        ad.setButton(getResources().getString(R.string.yes),
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int arg1) {
                                        // TODO Auto-generated method stub
                                        SharedPreferences someData = getSharedPreferences("data", 0);
                                        SharedPreferences.Editor editor = someData.edit();
                                        editor.remove("add");
                                        editor.commit();
                                        loginedEmail="";
                                        isLogined=false;
                                        ((TextView) headerview.findViewById(R.id.tv_login)).setText(getResources().getString(R.string.login));

                                        Toast.makeText(MainActivity.this,getResources().getString(R.string.logOutSuccess),Toast.LENGTH_SHORT).show();

                                        dialog.dismiss();
                                    }
                                });

                        ad.setButton2(getResources().getString(R.string.no),
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int arg1) {
                                        // TODO Auto-generated method stub
                                        dialog.dismiss();
                                    }
                                });
                        ad.show();


                    }
                }
            });

        }


        View tabOne = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        ((TextView)tabOne.findViewById(R.id.tab)).setText("مناسبت ها");
        ((ImageView)tabOne.findViewById(R.id.tabIcon)).setImageResource(R.drawable.ic_action_dates_light);
        tabLayout.getTabAt(0).setCustomView(tabOne);


        View tabTwo = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        ((TextView)tabTwo.findViewById(R.id.tab)).setText("یادداشت ها");
        ((ImageView)tabTwo.findViewById(R.id.tabIcon)).setImageResource(R.drawable.ic_action_todolist_light);
        tabLayout.getTabAt(1).setCustomView(tabTwo);

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 1)
                    datesFragment.exitEditMode();
                else
                    notesFragment.exitEditMode();

            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });




    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == SMS_PERMISSION)
        {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
            {
                Toast.makeText(this, "sms permission granted", Toast.LENGTH_LONG).show();
                startActivity(new Intent(this,AddContact.class));
            }
            else
            {
                Toast.makeText(this, "sms permission denied", Toast.LENGTH_LONG).show();
            }
        }
    }


    private static final int SMS_PERMISSION = 100;
    private static final int REQUEST_ADD_NOTE = 101;
    private static final int REQUEST_UPDATE_NOTE = 102;
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {

        }

    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()){
            case R.id.fam:


                switch (viewPager.getCurrentItem()){
                    case 0:
                        if (checkSelfPermission(Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED)
                        {
                            requestPermissions(new String[]{Manifest.permission.SEND_SMS}, SMS_PERMISSION);
                        }
                        else
                        {
                            intent=new Intent(this,AddContact.class);
                            startActivity(intent);
                        }                        break;
                    case 1:
                        Intent i = new Intent(this, NoteEditor.class);
                        i.putExtra("mode", "add");
                        startActivityForResult(i, REQUEST_ADD_NOTE);

                        //intent=new Intent(this,Backup.class);
                        //startActivity(intent);
                        break;

                }

                break;
        }
    }





    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_row) {
            if (showMode== item.getItemId()) return false;
            showMode= item.getItemId();
            drawerLayout.closeDrawers();
            datesFragment.reLoad(DatesFragment.VIEW_TYPE_LINEAR);
            notesFragment.reLoad(NotesFragment.VIEW_TYPE_LINEAR);
            SharedPreferences someData = getSharedPreferences("data", 0);
            SharedPreferences.Editor editor = someData.edit();
            editor.putInt("showMode",R.id.nav_row);
            editor.commit();

        }

        if (id == R.id.nav_grid) {
            if (showMode== item.getItemId()) return false;
            showMode= item.getItemId();
            drawerLayout.closeDrawers();
            datesFragment.reLoad(DatesFragment.VIEW_TYPE_GRID);
            notesFragment.reLoad(NotesFragment.VIEW_TYPE_GRID);
            SharedPreferences someData = getSharedPreferences("data", 0);
            SharedPreferences.Editor editor = someData.edit();
            editor.putInt("showMode",R.id.nav_grid);
            editor.commit();

        }

        if (id == R.id.nav_backup) {
            if (!isLogined){
                Toast.makeText(this, "برای استفاده از این سرویس ابتدا می بایست به برنامه ورود کنید", Toast.LENGTH_SHORT).show();
                Intent login=new Intent(MainActivity.this, LoginActivity.class);
                startActivity(login);

            }
            else
            {
                Intent intent=new Intent(MainActivity.this,Backup.class);
                startActivity(intent);

            }

        }

        if (id == R.id.nav_setting) {
            Intent intent=new Intent(MainActivity.this,Settings.class);
            startActivity(intent);
        }

        if (id == R.id.nav_help) {
            Intent intent=new Intent(MainActivity.this,Help.class);
            startActivity(intent);
        }


        return true;
    }


    private Menu menu = null;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        this.menu = menu;
        getMenuInflater().inflate(R.menu.main, menu);
        if (menu!=null){
            Runnable showguide=new Runnable() {
                @Override
                public void run() {
                    showGuideHomePage();
                }
            };
            navigationView.postDelayed(showguide,1000);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_edit:
                if (viewPager.getCurrentItem()==0) datesFragment.performEdit(); else notesFragment.performEdit();
                break;
            case R.id.action_search:
                Intent intent = new Intent(this, SearchActivity.class);
                startActivity(intent);

                break;
            case R.id.action_delete:
                afb.elan.MaterialDialog.BottomSheetMaterialDialog mBottomSheetDialog = new afb.elan.MaterialDialog.BottomSheetMaterialDialog.Builder(MainActivity.this)
                        .setTitle("حذف")
                        .setMessage("آیا از حذف موارد انتخاب شده اطمینان دارید؟")
                        .setCancelable(true)
                        .setAnimation(R.raw.questionmark)
                        .setPositiveButton("حذف", R.drawable.ic_action_delete, new BottomSheetMaterialDialog.OnClickListener() {
                            @Override
                            public void onClick(dev.shreyaspatil.MaterialDialog.interfaces.DialogInterface dialogInterface, int which) {
                                if (viewPager.getCurrentItem()==0) datesFragment.performDelete(); else notesFragment.performDelete();
                                dialogInterface.dismiss();
                            }
                        })
                        .setNegativeButton("صرف نظر", R.drawable.ic_action_no, new BottomSheetMaterialDialog.OnClickListener() {
                            @Override
                            public void onClick(dev.shreyaspatil.MaterialDialog.interfaces.DialogInterface dialogInterface, int which) {
                                dialogInterface.dismiss();
                            }
                        })
                        .build();

                // Show Dialog
                mBottomSheetDialog.show();
                break;
            case R.id.action_guide:
                SharedPreferences guidestatus;
                SharedPreferences.Editor editor;
                guidestatus = getSharedPreferences("guidestatus", 0);
                editor = guidestatus.edit();
                editor.putInt("fragmenthome", 0);
                editor.commit();
                guideView=null;
                showGuideHomePage();
                break;

        }

        return super.onOptionsItemSelected(item);
    }

/*
    @Override
    public boolean onSupportNavigateUp() {
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }*/

    public void hideOption(int id) {
        MenuItem item = menu.findItem(id);
        item.setVisible(false);
    }

    public void showOption(int id) {
        MenuItem item = menu.findItem(id);
        item.setVisible(true);

    }

    public void setOptionTitle(int id, String title) {
        MenuItem item = menu.findItem(id);
        item.setTitle(title);
    }

    public void setOptionIcon(int id, int iconRes) {
        MenuItem item = menu.findItem(id);
        item.setIcon(iconRes);
    }

    public boolean isOptionVisibled(int id) {
        MenuItem item = menu.findItem(id);
        return  item.isVisible();
    }

    private boolean doubleBackToExitPressedOnce=false;
    @Override
    public void onBackPressed() {
        if (guideView!=null)
        {
            guideViewBackpressed=true;
            guideView.dismiss();
            return;

        }

        if (drawerLayout.isDrawerOpen(GravityCompat.START)){
            drawerLayout.closeDrawers();
            return;
        }

        if (datesFragment.selectedItemCount>0){
            datesFragment.exitEditMode();
            return;
        }
        if (notesFragment.selectedItemCount>0){
            notesFragment.exitEditMode();
            return;
        }



        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "برای خروج دوباره کلیک کنید", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }



    GuideView guideView;
    Boolean guideViewBackpressed;
    public void showGuideHomePage() {
        guideViewBackpressed=false;
        SharedPreferences someData = getSharedPreferences("guidestatus", 0);

        final int status = someData.getInt("fragmenthome", 0);
        final SharedPreferences.Editor editor = someData.edit();
        Typeface iran = Typeface.createFromAsset(getAssets(), "fonts/iransanse.ttf");
        View fieldtab;
        switch (status) {

/*
            case 7: {
                MenuItem item = menu.findItem(R.id.action_message);
                if (!item.isVisible()) {
                    editor.putInt("fragmenthome", status + 1);
                    editor.commit();
                    showGuideHomePage();
                    break;
                }
                fieldtab = findViewById(R.id.action_message);
                guideView = new GuideView.Builder(TabbedActivity.this)
                        .setTitle("صندوق پیام")
                        .setContentText("پیام های مدیریت اینجا نمایش داده می شن")
                        .setTargetView(fieldtab)
                        .setContentTypeFace(iran)
                        .setTitleTypeFace(iran)//optional
                        .setDismissType(DismissType.anywhere) //optional
                        .setGuideListener(new GuideListener() {
                            @Override
                            public void onDismiss(View view) {
                                if (guideViewBackpressed) {
                                    editor.putInt("fragmenthome", 100);
                                    editor.commit();
                                    guideView = null;
                                } else {
                                    editor.putInt("fragmenthome", status + 1);
                                    editor.commit();
                                    showGuideHomePage();

                                }
                            }
                        })
                        .build();
                guideView.show();
            }
            break;

            case 8: {
                fieldtab = getNavButtonView(toolbar);
                if (fieldtab == null) {
                    editor.putInt("fragmenthome", status + 1);
                    editor.commit();
                    showGuideHomePage();

                }
                guideView = new GuideView.Builder(TabbedActivity.this)
                        .setTitle("منوی مدیریتی شما اینجاست")
                        .setContentText("ثبت نام کن\nپروفایلت رو تکمیل کن و درآمد داشته باش")
                        .setTargetView(fieldtab)
                        .setContentTypeFace(iran)
                        .setTitleTypeFace(iran)//optional
                        .setDismissType(DismissType.anywhere) //optional
                        .setGuideListener(new GuideListener() {
                            @Override
                            public void onDismiss(View view) {
                                if (guideViewBackpressed) {
                                    editor.putInt("fragmenthome", 100);
                                    editor.commit();
                                    guideView = null;
                                } else {
                                    editor.putInt("fragmenthome", status + 1);
                                    editor.commit();
                                    drawerLayout.openDrawer(Gravity.START);
                                    showGuideHomePage();

                                }
                            }
                        })
                        .build();
                guideView.show();

            }
            break;
*/
            case 0:
            {
                if (!drawerLayout.isDrawerOpen(GravityCompat.START)) drawerLayout.openDrawer(GravityCompat.START);
                drawerLayout.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        View headerview = navigationView.getHeaderView(0);
                        View fieldtab= ((LinearLayout)headerview.findViewById(R.id.layout_icon));
                        guideView=new GuideView.Builder(MainActivity.this)
                                .setTitle("معرفی")
                                .setContentText("اعلان یه برنامه رایگانه که می تونی یادداشت ها و مناسبت های مهم زندگیت رو داخلش ثبت کنی و براش یادآور تنظیم کنی تا در موعد مقرر بهت یادآوری کنه و یا حتی به صورت خودکار به دوستات پیامک مناسب ارسال کنه. اعلان دارای ویجت هم هست و می تونی با ویجت هاش لیست مناسبت های هر روز رو و یا هر کدام از یادداشت ها رو روی صفحه ی اصلی گوشی دم دست داشته باشی. با اعلان می تونی از تمام این اطلاعات روی فضای ابری مخصوص خودت نسخه پشتیبان تهیه کنی و در دستگاه های مختلف به راحتی اونها رو بازیابی کنی")
                                .setTargetView(fieldtab)
                                .setContentTypeFace(iran)
                                .setTitleTypeFace(iran)//optional
                                .setDismissType(DismissType.anywhere) //optional
                                .setGuideListener(new GuideListener() {
                                    @Override
                                    public void onDismiss(View view) {
                                        if (guideViewBackpressed){
                                            editor.putInt("fragmenthome", 100);
                                            editor.commit();
                                            guideView=null;
                                        }
                                        else
                                        {
                                            editor.putInt("fragmenthome", status + 1);
                                            editor.commit();
                                            showGuideHomePage();

                                        }
                                    }
                                })
                                .build();
                        guideView.show();

                    }
                },500);

                }
            break;
            case 1:
            {
                if (!drawerLayout.isDrawerOpen(GravityCompat.START)) drawerLayout.openDrawer(GravityCompat.START);
                View headerview = navigationView.getHeaderView(0);
                fieldtab= ((TextView)headerview.findViewById(R.id.tv_login));
                guideView=new GuideView.Builder(MainActivity.this)
                        .setTitle("ورود با ایمیل")
                        .setContentText("از اینجا با وارد کردن آدرس ایمیل می تونی به راحتی به سرور متصل بشی. برای استفاده از سرویس پشتیبان گیری ابری ورود به برنامه با ایمیل ضروریه")
                        .setTargetView(fieldtab)
                        .setContentTypeFace(iran)
                        .setTitleTypeFace(iran)//optional
                        .setDismissType(DismissType.anywhere) //optional
                        .setGuideListener(new GuideListener() {
                            @Override
                            public void onDismiss(View view) {
                                if (guideViewBackpressed){
                                    editor.putInt("fragmenthome", 100);
                                    editor.commit();
                                    guideView=null;
                                }
                                else
                                {
                                    editor.putInt("fragmenthome", status + 1);
                                    editor.commit();
                                    showGuideHomePage();

                                }
                            }
                        })
                        .build();
                guideView.show();

            }
            break;

            case 2:
            {
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) drawerLayout.closeDrawer(GravityCompat.START);
                fieldtab = findViewById(R.id.fam);
                guideView=new GuideView.Builder(MainActivity.this)
                        .setTitle("ثبت رویداد")
                        .setContentText("با لمس این گزینه به راحتی می تونی مناسبت های مهم زندگیت یا یادداشت هایی که نیاز داری همیشه دم دستت باشن رو ثبت کنی تا در زمان مناسب بهت یادآوری و برات نمایش داده بشن")
                        .setTargetView(fieldtab)
                        .setContentTypeFace(iran)
                        .setTitleTypeFace(iran)//optional
                        .setDismissType(DismissType.anywhere) //optional
                        .setGuideListener(new GuideListener() {
                            @Override
                            public void onDismiss(View view) {
                                if (guideViewBackpressed){
                                    editor.putInt("fragmenthome", 100);
                                    editor.commit();
                                    guideView=null;
                                }
                                else
                                {
                                    editor.putInt("fragmenthome", status + 1);
                                    editor.commit();
                                    showGuideHomePage();

                                }
                            }
                        })
                        .build();
                guideView.show();

            }
            break;

            case 3: // راهنمای جستجو
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) drawerLayout.closeDrawer(GravityCompat.START);
                //viewPager.setCurrentItem(0);
                fieldtab = ((ViewGroup) tabLayout.getChildAt(0)).getChildAt(0);
                guideView = new GuideView.Builder(MainActivity.this)
                        .setTitle("صفحه مناسبت ها")
                        .setContentText("مناسبت هایی که ثبت کردی تو این صفحه بهت نمایش داده می شن. با لمس روی هر کدوم می تونی اونها رو مشاهده، ویرایش، یا حذف کنی")
                        .setTargetView(fieldtab)
                        .setContentTypeFace(iran)
                        .setTitleTypeFace(iran)//optional
                        .setDismissType(DismissType.anywhere) //optional
                        .setGuideListener(new GuideListener() {
                            @Override
                            public void onDismiss(View view) {
                                if (guideViewBackpressed) {
                                    editor.putInt("fragmenthome", 100);
                                    editor.commit();
                                    guideView = null;
                                } else {
                                    editor.putInt("fragmenthome", status + 1);
                                    editor.commit();
                                    showGuideHomePage();

                                }
                            }
                        })
                        .build();
                guideView.show();
                break;


            case 4: // راهنمای جستجو
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) drawerLayout.closeDrawer(GravityCompat.START);
                //viewPager.setCurrentItem(1);
                fieldtab = ((ViewGroup) tabLayout.getChildAt(0)).getChildAt(1);
                guideView = new GuideView.Builder(MainActivity.this)
                        .setTitle("صفحه یادداشت ها")
                        .setContentText("یادداشت هایی رو که ذخیره کردی تو این صفحه بهت نمایش داده می شن. اینجا هم با لمس روی هر کدوم می تونی اونها رو مشاهده و ویرایش کنی. برای حذف یادداشت هم کافیه روی عنوانش لمس کنی و نگه داری تا شکلک سطل زباله اون بالا بهت نمایش داده بشه")
                        .setTargetView(fieldtab)
                        .setContentTypeFace(iran)
                        .setTitleTypeFace(iran)//optional
                        .setDismissType(DismissType.anywhere) //optional
                        .setGuideListener(new GuideListener() {
                            @Override
                            public void onDismiss(View view) {
                                if (guideViewBackpressed) {
                                    editor.putInt("fragmenthome", 100);
                                    editor.commit();
                                    guideView = null;
                                } else {
                                    editor.putInt("fragmenthome", status + 1);
                                    editor.commit();
                                    showGuideHomePage();

                                }
                            }
                        })
                        .build();
                guideView.show();
                break;



            case 5:{
                /*MenuItem item = menu.findItem(R.id.action_search);
                if (!item.isVisible()) {
                    editor.putInt("fragmenthome", status + 1);
                    editor.commit();
                    showGuideHomePage();
                    break;
                }*/
                fieldtab = findViewById(R.id.action_search);
                guideView=new GuideView.Builder(MainActivity.this)
                        .setTitle("دنبال چیزی می گردی؟")
                        .setContentText("اگر دنبال مناسبت یا یادداشت خاصی می گردی از این بخش می تونی به راحتی جستجو و پیداشون کنی")
                        .setTargetView(fieldtab)
                        .setContentTypeFace(iran)
                        .setTitleTypeFace(iran)//optional
                        .setDismissType(DismissType.anywhere) //optional
                        .setGuideListener(new GuideListener() {
                            @Override
                            public void onDismiss(View view) {
                                if (guideViewBackpressed){
                                    editor.putInt("fragmenthome", 100);
                                    editor.commit();
                                    guideView=null;
                                }
                                else
                                {
                                    editor.putInt("fragmenthome", status + 1);
                                    editor.commit();
                                    showGuideHomePage();

                                }
                            }
                        })
                        .build();
                guideView.show();

            }
            break;







        }


    }

}