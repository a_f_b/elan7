package afb.elan.demo;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Contacts;
import android.provider.MediaStore;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mikelau.croperino.Croperino;
import com.mikelau.croperino.CroperinoConfig;
import com.mikelau.croperino.CroperinoFileUtil;

import afb.elan.demo.Db.BirthdayRecord;
import afb.elan.demo.Db.SQLHelper;
import afb.elan.demo.Services.DemoService;
import afb.elan.demo.calendar.SolarDate;
import afb.elan.demo.calendar.SolarDateDialog;
import afb.elan.demo.classes.Field;
import dev.shreyaspatil.MaterialDialog.BottomSheetMaterialDialog;

public class AddContact extends AppCompatActivity implements OnClickListener, OnTouchListener {

    Button bAdd, bCancel;
    EditText etName, etFamily, etEmail, etMobile,
            etPhone, etSmsBody;
    TextView tvDate;
    boolean dataAdded = false;
    boolean editMode = false;
    boolean imageloaded = false;
    long editingId = -1;
    TextView tv;
    byte[] _image = null;
    private static int CONTACT_PERMISSION=1010;

    private final static int dkSolar = 1, dkGeorgian = 0;
    final static int PICK_CONTACT = 0x1221;
    final static int SELECT_PICTURE = 0x1222;
    final static int CAMERA_TAKE_PICTURE = 0x1223;
    final static int REQUEST_CROP_ICON = 0x1224;
    int current__dateType = -1;
    ImageView iv;
    String lastCustomDate = "";
    private boolean fieldsChanged;

    private SolarDate solarDate;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub

        super.onCreate(savedInstanceState);

        //WindowManager.LayoutParams lp = getWindow().getAttributes();
        //lp.windowAnimations = R.style.SlideFromLeftToLeft;
        //getWindow().setAttributes(lp);
        setContentView(R.layout.addcontact);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // For navigation bar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_TITLE | ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_HOME_AS_UP);
        }


        bAdd = (Button) findViewById(R.id.bSave);
        bCancel = (Button) findViewById(R.id.bCancelAdd);
        bAdd.setOnClickListener(this);
        bCancel.setOnClickListener(this);

        TextView tv_loadFromContacts = (TextView) findViewById(R.id.tv_loadFromContacts);
        ImageView ivContact = (ImageView) findViewById(R.id.ivContact);
        tv_loadFromContacts.setOnClickListener(this);
        ivContact.setOnClickListener(this);


        iv = (ImageView) findViewById(R.id.im_addingcontacnt);
        etName = (EditText) findViewById(R.id.etName);
        etFamily = (EditText) findViewById(R.id.etFamily);
        etPhone = (EditText) findViewById(R.id.etPhone);
        etMobile = (EditText) findViewById(R.id.etMobile);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etSmsBody = (EditText) findViewById(R.id.etSmsBody);
        tv = (TextView) findViewById(R.id.tv_addingLayoutTitle);
        tvDate=(TextView)findViewById(R.id.tvDate);
        Calendar ca = Calendar.getInstance();
        /*solarDate = new SolarDate();
        solarDate.setdate(SolarDate.DATE_KIND_GEORGIAN,ca.get(Calendar.YEAR), ca.get(Calendar.MONTH) + 1,
                ca.get(Calendar.DAY_OF_MONTH));*/
        solarDate=SolarDate.newInstance(SolarDate.DATE_KIND_SOLAR);
        tvDate.setText(solarDate.toString());


        tvDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                SolarDateDialog sdd=new SolarDateDialog(AddContact.this);
                sdd.setPersianDate(solarDate.toString());
                sdd.setOnSublitListener(new SolarDateDialog.onSubmitListener() {

                    @Override
                    public void onSubmit(String date) {
                        // TODO Auto-generated method stub
                        solarDate.setSolarDateFromString(date);
                        tvDate.setText(solarDate.toString());
                        fieldsChanged=true;
                    }
                });
                sdd.show();

            }
        });

        setupAutoCompleteText();

        if (getIntent().hasExtra("editMode"))
            editMode = getIntent().getExtras().getBoolean("editMode");
        if (editMode) {
            Bundle bundle = getIntent().getExtras();
            // edit mode

            // bAdd.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.saveedit2),
            // null, null, null);
            editingId = bundle.getLong("editingId");
            /*
             * etName.setText(bundle.getString("name"));
             * etFamily.setText(bundle.getString("family"));
             * etMobile.setText(bundle.getString("mobile"));
             * etPhone.setText(bundle.getString("phone"));
             * etEmail.setText(bundle.getString("email")); current__dateType =
             * bundle.getInt("dateType");
             * sp_dateType.setSelection(current__dateType);
             * etSmsBody.setText(bundle.getString("smsBody")); String bdate =
             * bundle.getString("birthday"); String byear = bdate.substring(0,
             * 4); String bmonth = bdate.substring(5, 7); String bday =
             * bdate.substring(8); etYear.setText(byear);
             * etMonth.setText(bmonth); etDay.setText(bday);
             */

            SQLHelper c = new SQLHelper(this);
            c.openDb();
            BirthdayRecord br = c.fetchRecordById(editingId);
            c.closeDb();
            // _image=b.getImage();
            if (br.getImage() != null) {
                _image = br.getImage();
                iv.setImageBitmap(BitmapFactory.decodeByteArray(br.getImage(),
                        0, br.getImage().length));
                iv.setBackgroundColor(Color.BLACK);
                imageloaded = true;
            }




            etName.setText(br.getName());
            etFamily.setText(br.getFamily());
            etMobile.setText(br.getMobile());
            etPhone.setText(br.getPhone());
            etEmail.setText(br.getEmail());
            current__dateType = br.getDateType();
            lastCustomDate = br.getCustomDateExp();
            tvDate.setText(br.getBirthday_Date());
            solarDate.setSolarDateFromString(br.getBirthday_Date());
            tvDateExp.setText(br.getCustomDateExp());


            //if (current__dateType==4) etDateExp.setText(br.getCustomDateExp());
            //else etDateExp.setText(et_dateType.getText().toString());
            //Toast.makeText(this, br.getCustomDateExp(), 1000).show();
            etSmsBody.setText(br.getSmsBody());
            String bdate = br.getBirthday_Date();
            String byear = bdate.substring(0, 4);
            String bmonth = bdate.substring(5, 7);
            String bday = bdate.substring(8);
            tv.setText(getResources().getString(R.string.edit));
            // bLoad.setEnabled(false);
        } else {
            BirthdayRecord lbr = null;
            if ((lbr = (BirthdayRecord) getLastNonConfigurationInstance()) != null) {
                etName.setText(lbr.getName());
                etFamily.setText(lbr.getFamily());
                etMobile.setText(lbr.getMobile());
                etPhone.setText(lbr.getPhone());
                etEmail.setText(lbr.getEmail());
                current__dateType = lbr.getDateType();
                etSmsBody.setText(lbr.getSmsBody());
                tvDate.setText(lbr.getBirthday_Date());
                tvDateExp.setText(lbr.getCustomDateExp());

                if (lbr.hasImage()) {
                    _image = lbr.getImage();
                    iv.setImageBitmap(BitmapFactory.decodeByteArray(lbr.getImage(),
                            0, lbr.getImage().length));
                    iv.setBackgroundColor(Color.BLACK);
                    imageloaded = true;

                }
            }
        }

        registerForContextMenu(iv);
        iv.setOnClickListener(this);

        fieldsChanged=false;
        TextWatcher textWatcher = new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                fieldsChanged=true;
            }
        };
        etName.addTextChangedListener(textWatcher);
        etFamily.addTextChangedListener(textWatcher);
        etPhone.addTextChangedListener(textWatcher);
        etEmail.addTextChangedListener(textWatcher);
        tvDate.addTextChangedListener(textWatcher);
        tvDateExp.addTextChangedListener(textWatcher);

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenuInfo menuInfo) {
        // TODO Auto-generated method stub
        // super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.addcontact_image_context, menu);

    }


    private boolean checkContactPremission(){
        boolean result= ContextCompat.checkSelfPermission(this,Manifest.permission.READ_CONTACTS)==PackageManager.PERMISSION_GRANTED;
        return  result;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void requestContactPermission(){
        //permissions to request
        if (!checkContactPremission()){
            String[] permission = {Manifest.permission.READ_CONTACTS};

            requestPermissions(permission, CONTACT_PERMISSION);

        }
        else pickContactIntent();
    }

    private void pickContactIntent(){
        //intent to pick contact
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(intent, PICK_CONTACT);
    }
    @Override
    public void finish() {
        // we need to override this to performe the animtationOut on each
        // finish.
        if (dataAdded){
            Intent intent=new Intent(MainActivity.ELAN_INSERT_DATE);
            sendBroadcast(intent);

        }
        super.finish();
        // disable default animation
		/*(if (startActivityForResultLoaded) {
			WindowManager.LayoutParams lp = getWindow().getAttributes();
			lp.windowAnimations = R.style.SlideFromLeftToLeft;
			getWindow().setAttributes(lp);
		}

		overridePendingTransition(0, 0);*/

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_CAMERA_PERMISSION_CODE)
        {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
            {
                Toast.makeText(this, "camera permission granted", Toast.LENGTH_LONG).show();
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_TAKE_PICTURE);
            }
            else
            {
                Toast.makeText(this, "camera permission denied", Toast.LENGTH_LONG).show();
            }
        }
        else if (requestCode == CONTACT_PERMISSION){
            if (grantResults.length >0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                //permission granted, can pick contact now
                pickContactIntent();
            }
            else {
                //permission denied
                Toast.makeText(this, "Permission denied...", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public Uri uri;
    final static int GET_IMAGE_FOR_PROFILE=100;
    final static int CROP_IMAGE_FOR_PROFILE=200;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;

    public void GetImageFromGallery(int mode){

        Intent GalIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(Intent.createChooser(GalIntent, "Select Image From Gallery"), mode);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.addcontact_actions, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                bAdd.performClick();
                break;
            case android.R.id.home:
                bCancel.performClick();
                break;
        }

        return super.onOptionsItemSelected(item);
    }



    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        Intent back = new Intent();
        switch (v.getId()) {

            case R.id.im_addingcontacnt:
                PopupMenu popup = new PopupMenu(this,iv);
                popup.getMenuInflater().inflate(R.menu.loadimage, popup.getMenu());
                //Utils.setForceShowIcon(popup);

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public boolean onMenuItemClick(MenuItem mi) {
                        // TODO Auto-generated method stub
                        if (mi.getItemId()==R.id.action_loadphoto){
                            GetImageFromGallery(GET_IMAGE_FOR_PROFILE);
                        }
                        else
                        /*if (mi.getItemId()==R.id.action_loadcamera) {
                            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
                            {
                                requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
                            }
                            else
                            {
                                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                                startActivityForResult(cameraIntent, CAMERA_TAKE_PICTURE);
                            }

                        }
                        else*/
                        if (mi.getItemId()==R.id.action_clear) {
                            _image = null;
                            imageloaded = false;
                            iv.setBackgroundColor(Color.TRANSPARENT);
                            iv.setImageResource(R.drawable.blank_user);
                        }



                        /*else if (mi.getItemId()==R.id.action_loadphoto2){
                            lastCroperinoCall=CROP_IMAGE_FOR_PROFILE;
                            new CroperinoConfig("IMG_" + System.currentTimeMillis() + ".jpg", "/temp/Pictures", "/sdcard/temp/Pictures");
                            CroperinoFileUtil.verifyStoragePermissions(getActivity());
                            CroperinoFileUtil.setupDirectory(getActivity());
                            Croperino.prepareGallery(getActivity());
                        }*/
                        else if (mi.getItemId()==R.id.action_loadcamera){
                            new CroperinoConfig("IMG_" + System.currentTimeMillis() + ".jpg", "/temp/Pictures", "/sdcard/temp/Pictures");
                            CroperinoFileUtil.verifyStoragePermissions(AddContact.this);
                            CroperinoFileUtil.setupDirectory(AddContact.this);
                            //Croperino.prepareChooser(getActivity(), "Capture photo...", ContextCompat.getColor(getActivity(), android.R.color.background_dark));

                            //Camera
                            try {
                                Croperino.prepareCamera(AddContact.this);
                            } catch(Exception e) {
                                //e.printStackTrace;
                            }

                        }

                        return false;
                    }
                });

                popup.show();
                break;

            case R.id.bSave:
                boolean errorCheck=false;

                if (tvDateExp.getText().toString().isEmpty()) {

                    tvDateExp.setError(getResources().getString(R.string.fillIt));
                    errorCheck=true;
                    tvDateExp.requestFocus();
                }
                if (etName.getText().toString().length() == 0) {

                    etName.setError(getResources().getString(R.string.fillIt));
                    errorCheck=true;
                }
                if (etFamily.getText().toString().length() == 0) {
                    etFamily.setError(getResources().getString(R.string.fillIt));
                    errorCheck=true;
                }
                if (!errorCheck){
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(etName.getWindowToken(), 0);
                    //sd.setdate(iyear, imonth, iday);
                    // AddingDialog ad = new AddingDialog(this, sd);
                    setupAddingDialog(solarDate);
                }

                break;
            case R.id.bCancelAdd:

                if (!editMode){
                    if (etName.getText().toString().length()
                            + etFamily.getText().toString().length() > 0){
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(etName.getWindowToken(), 0);
                        setupCancelDialog();
                    }
                    else
                        finish();
                } else {
                    if (fieldsChanged){
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(etName.getWindowToken(), 0);
                        setupCancelDialog();
                    }
                    else finish();
                }

                break;
            case R.id.ivContact:
            case R.id.tv_loadFromContacts:
                requestContactPermission();
                /*Intent intent = new Intent(Intent.ACTION_PICK,
                        Contacts.CONTENT_URI);

                WindowManager.LayoutParams lp = getWindow().getAttributes();
                lp.windowAnimations = android.R.style.Animation_Activity;
                getWindow().setAttributes(lp);
                startActivityForResult(intent, PICK_CONTACT);
                overridePendingTransition(0, 0);*/

                break;

        }

    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        // super.onBackPressed();
        /*
         * if (dataAdded) setResult(RESULT_OK); else setResult(RESULT_CANCELED);
         * finish();
         */
        bCancel.performClick();
    }

    private void setupCancelDialog() {
        afb.elan.MaterialDialog.BottomSheetMaterialDialog mBottomSheetDialog = new afb.elan.MaterialDialog.BottomSheetMaterialDialog.Builder(this)
                .setTitle("خروج")
                .setMessage(getResources().getString(R.string.cancelSaveDialog))
                .setCancelable(true)
                .setAnimation(R.raw.questionmark)
                .setPositiveButton(getResources().getString(R.string.yes), R.drawable.ic_action_exit, new BottomSheetMaterialDialog.OnClickListener() {
                    @Override
                    public void onClick(dev.shreyaspatil.MaterialDialog.interfaces.DialogInterface dialogInterface, int which) {
                        saveData(solarDate);
                        dialogInterface.dismiss();
                        finish();
                    }
                })
                .setNegativeButton("صرف نظر", R.drawable.ic_action_no, new BottomSheetMaterialDialog.OnClickListener() {
                    @Override
                    public void onClick(dev.shreyaspatil.MaterialDialog.interfaces.DialogInterface dialogInterface, int which) {
                        dialogInterface.dismiss();
                        finish();
                    }
                })
                .build();

        // Show Dialog
        mBottomSheetDialog.show();


    }

    private void saveData(SolarDate sd){
        String name = etName.getText().toString();
        String family = etFamily.getText().toString();
        String phone = etPhone.getText().toString();
        String mobile = etMobile.getText().toString();
        String email = etEmail.getText().toString();
        String smsbody = etSmsBody.getText().toString();

        String birthday_date = sd.toString();
        String birthday_day = sd.getMMDD();
        int solarValue = sd.getDayValue();
        sd.changeDateKind(SolarDate.DATE_KIND_GEORGIAN);
        String birthday_date_g = sd.toString();
        String birthday_day_g = sd.getMMDD();

        BirthdayRecord br = new BirthdayRecord();
        br.setContent(-1, name, family, birthday_date,
                birthday_day, birthday_date_g, birthday_day_g,
                mobile, phone, email, current__dateType,
                smsbody, _image, solarValue, tvDateExp.getText().toString());
        SQLHelper cdbo = new SQLHelper(
                AddContact.this);

        if (editMode) {

            // editmode

            cdbo.openDb();
            cdbo.UpdateRecord(editingId, br);
            cdbo.closeDb();

            Toast.makeText(
                    AddContact.this,
                    getResources().getString(
                            R.string.recordUpdated), Toast.LENGTH_SHORT)
                    .show();

            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(
                    etName.getWindowToken(), 0);

            dataAdded=true;

            finish();

        } else {
            cdbo.openDb();
            cdbo.AddRecord(br);
            cdbo.closeDb();


            etName.setText("");
            etFamily.setText("");
            etPhone.setText("");
            etMobile.setText("");
            etEmail.setText("");
            etSmsBody.setText("");
            Toast.makeText(AddContact.this, getResources().getString(R.string.recordAdded),
                    Toast.LENGTH_SHORT).show();

            dataAdded = true;
            if (imageloaded) {
                imageloaded = false;
                iv.setImageResource(R.drawable.blank_user);
                _image = null;
            }

            Intent intent=new Intent(DemoService.AFB_ELAN_STUDIO_DEMORECEIVER);
            intent.putExtra("needCheck",true);
            sendBroadcast(intent);


            etName.requestFocus();

        }

    }

    private void setupAddingDialog(SolarDate solardate) {
        if (editMode){
            saveData(solardate);
            return;
        }
        String message=getResources().getString(R.string.savingMeesage);

        afb.elan.MaterialDialog.BottomSheetMaterialDialog mBottomSheetDialog = new afb.elan.MaterialDialog.BottomSheetMaterialDialog.Builder(this)
                .setTitle("")
                .setMessage(message)
                .setCancelable(true)
                .setAnimation(R.raw.questionmark)
                .setPositiveButton(getResources().getString(R.string.yes), R.drawable.ic_action_yes, new BottomSheetMaterialDialog.OnClickListener() {
                    @Override
                    public void onClick(dev.shreyaspatil.MaterialDialog.interfaces.DialogInterface dialogInterface, int which) {
                        saveData(solardate);
                        dialogInterface.dismiss();

                    }
                })
                .setNegativeButton("صرف نظر", R.drawable.ic_action_no, new BottomSheetMaterialDialog.OnClickListener() {
                    @Override
                    public void onClick(dev.shreyaspatil.MaterialDialog.interfaces.DialogInterface dialogInterface, int which) {
                        dialogInterface.dismiss();
                    }
                })
                .build();

        // Show Dialog
        mBottomSheetDialog.show();



    }




    private Bitmap decodeBitmapFromFile(File f, int maxSize) {
        try {
            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= maxSize
                    || o.outHeight / scale / 2 >= maxSize)
                scale *= 2;

            // Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {
        }
        return null;
    }

    private Bitmap resizeBitmap(Bitmap bmp, int size) {
        try {
            // Decode image size
            int width = 0, height = 0;
            if (bmp.getWidth() > bmp.getHeight()) {
                width = size;
                height = (int) (size * bmp.getHeight() / bmp.getWidth());
                Log.d("width", width + "x" + height);
            } else {
                height = size;
                width = (int) (size * bmp.getWidth() / bmp.getHeight());
                Log.d("width", width + "x" + height);
            }
            Bitmap b = Bitmap.createScaledBitmap(bmp, width, height, false);
            return b;
        } catch (Exception e) {
        }
        return null;
    }

    private byte[] decodeBayteArrayFromBitmap(Bitmap bmp, int compresstion) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, compresstion, stream);
        return stream.toByteArray();
    }

    @Override
    public boolean onTouch(View v, MotionEvent me) {
        // TODO Auto-generated method stub
        // Toast.makeText(this, ""+me.getAction(), 1000).show();
        switch (me.getAction()) {
            case MotionEvent.ACTION_DOWN:
                ((TextView) v).setTextColor(Color.RED);
                // ((TextView)v).setBackgroundColor(getResources().getColor(R.color.blue));

                break;
            case MotionEvent.ACTION_UP:
                ((TextView) v).setTextColor(getResources().getColor(R.color.orange_light));
                // ((TextView)v).setBackgroundColor(Color.TRANSPARENT);

                break;

        }

        return false;
    }
    public void ImageCropFunction(int mode) {

        // Image Crop Code
        try {
            Intent CropIntent = new Intent("com.android.camera.action.CROP");

            CropIntent.setDataAndType(uri, "image/*");

            CropIntent.putExtra("crop", "true");
            CropIntent.putExtra("outputX", 320);
            CropIntent.putExtra("outputY", 320);
            CropIntent.putExtra("aspectX", 1);
            CropIntent.putExtra("aspectY", 1);
            CropIntent.putExtra("scaleUpIfNeeded", true);
            CropIntent.putExtra("return-data", true);

            startActivityForResult(CropIntent, mode);

        } catch (ActivityNotFoundException e) {

        }
    }

    public static Bitmap cropBitmapToSquare(Bitmap bitmap, boolean recycle) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();

        Bitmap result = null;/*w  w  w. j  av  a2s . co m*/
        if (height > width) {
            result = Bitmap.createBitmap(bitmap, 0, height / 2 - width / 2,
                    width, width);
        } else {
            result = Bitmap.createBitmap(bitmap, width / 2 - height / 2, 0,
                    height, height);
        }
        if (recycle) {
            bitmap.recycle();
        }
        return result;
    }

    @SuppressLint("Range")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != Activity.RESULT_OK)
            return;

        if (requestCode == GET_IMAGE_FOR_PROFILE) {

            if (data != null) {

                uri = data.getData();

                ImageCropFunction(CROP_IMAGE_FOR_PROFILE);

            }
        }
        if (requestCode == CROP_IMAGE_FOR_PROFILE) {

            if (data != null) {

                Bundle bundle = data.getExtras();

                final Bitmap selectedBitmap = bundle.getParcelable("data");

                if (selectedBitmap != null) {
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    selectedBitmap.compress(Bitmap.CompressFormat.JPEG, 95, stream); //compress to which format you want.
                    _image = stream.toByteArray();
                    iv.setBackgroundColor(Color.BLACK);
                    iv.setImageBitmap(BitmapFactory.decodeByteArray(_image, 0,
                            _image.length));
                    imageloaded = true;
                }
            }
        }

            switch (requestCode) {
                case CroperinoConfig.REQUEST_TAKE_PHOTO:

                        Croperino.runCropImage(CroperinoFileUtil.getTempFile(), this, true, 1, 1, 0, 0);

                    break;
                case CroperinoConfig.REQUEST_PICK_FILE:
                        CroperinoFileUtil.newGalleryFile(data, this);
                        Croperino.runCropImage(CroperinoFileUtil.getTempFile(), this, true, 1, 1, 0, 0);

                    break;
                case CroperinoConfig.REQUEST_CROP_PHOTO:
                        try {
                            Uri i = Uri.fromFile(CroperinoFileUtil.getTempFile());
                            Bitmap temp= null;
                            temp = MediaStore.Images.Media.getBitmap(getContentResolver(),i);
                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
                            temp.compress(Bitmap.CompressFormat.JPEG, 95, stream); //compress to which format you want.
                            _image = stream.toByteArray();
                            iv.setBackgroundColor(Color.BLACK);
                            iv.setImageBitmap(BitmapFactory.decodeByteArray(_image, 0,
                                    _image.length));
                            imageloaded = true;

                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    break;

                case CAMERA_TAKE_PICTURE:


                    if (_image != null)
					_image = null;
				_image = decodeBayteArrayFromBitmap(
						resizeBitmap((Bitmap) data.getExtras().get("data"), 480),
						0);
				iv.setBackgroundColor(Color.BLACK);
				Bitmap bmp=BitmapFactory.decodeByteArray(_image, 0,
                        _image.length);

				iv.setImageBitmap(cropBitmapToSquare(bmp,true));
				imageloaded = true;
				break;



                case PICK_CONTACT:

                        /*Cursor cursor1, cursor2;

                        //get data from intent
                        Uri uri = data.getData();

                        cursor1 = getContentResolver().query(uri, null, null, null, null);

                        if (cursor1.moveToFirst()) {
                            //get contact details
                            String contactId = cursor1.getString(cursor1.getColumnIndex(ContactsContract.Contacts._ID));
                            String contactName = cursor1.getString(cursor1.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                            String contactThumnail = cursor1.getString(cursor1.getColumnIndex(ContactsContract.Contacts.PHOTO_THUMBNAIL_URI));
                            String idResults = cursor1.getString(cursor1.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
                            int idResultHold = Integer.parseInt(idResults);

                            etMobile.setText(idResults);
                            etName.setText(contactName);

                            if (idResultHold == 1) {
                                cursor2 = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                        null,
                                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId,
                                        null,
                                        null
                                );
                                //a contact may have multiple phone numbers
                                while (cursor2.moveToNext()) {
                                    //get phone number
                                    String contactNumber = cursor2.getString(cursor2.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                                    //set details

                                    etMobile.append("\nPhone: " + contactNumber);
                                    //before setting image, check if have or not
                                    if (contactThumnail != null) {
                                        iv.setImageURI(Uri.parse(contactThumnail));
                                    } else {
                                        //iv.setImageResource(R.drawable.blank_user);
                                    }
                                }
                                cursor2.close();
                            }
                            cursor1.close();
                        }
                        break;*/


				Uri contactData = data.getData();
				Cursor c = managedQuery(contactData, null, null, null, null);
				try {
					if (c.moveToFirst()) {

						String id = c
								.getString(c
										.getColumnIndexOrThrow(Contacts._ID));


						/////////////////////////////////// load phone and mobile number ///////////////////////////////////////
						if (c.getString(
								c.getColumnIndex(Contacts.HAS_PHONE_NUMBER))
								.equalsIgnoreCase("1")) {

							Cursor phones = getContentResolver()
									.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
											null,
											ContactsContract.CommonDataKinds.Phone.CONTACT_ID
													+ " = " + id, null, null);
							// phones.moveToFirst();
							String phone="";
							String mobile="";
							while (phones.moveToNext()) {
								String number = phones
										.getString(phones
												.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DATA1));
								number=number.replaceAll("-", "");
								number=number.replaceAll(" ", "");
								if (phones
										.getString(
												phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE))
										.equals("2"))
									//etMobile.setText();
								{ if (mobile.isEmpty()) mobile=number; else mobile=mobile+"; "+number; }
								else
									//etPhone.setText(number.replaceAll("-", ""));
								{ if (phone.isEmpty()) phone=number; else phone=phone+"; "+number; }
							}
							etMobile.setText(mobile);
							etPhone.setText(phone);

						}

						/////////////////////////////////// load email ///////////////////////////////////////
						Cursor emailc = getContentResolver()
								.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI,
										null,
										ContactsContract.CommonDataKinds.Email.CONTACT_ID
												+ " = " + id, null, null);
						while (emailc.moveToNext()) {
							String email = emailc
									.getString(emailc
											.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
							etEmail.setText(email);
						}

						/////////////////////////////////// load name and family ///////////////////////////////////////
						Cursor namesc = getContentResolver().query(
								ContactsContract.Data.CONTENT_URI, null,
								ContactsContract.Data.CONTACT_ID + " = " + id,
								null, null);
						if (namesc.moveToFirst()) {
							String name = namesc
									.getString(namesc
											.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME));
							String family = namesc
									.getString(namesc
											.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME));
							String middle = namesc
									.getString(namesc
											.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.StructuredName.MIDDLE_NAME));

							if (name==null) name="";
							if (family==null) family="";
							if (middle!=null)
								  if (middle.length()>0)
									  family = middle + " " + family;
							if ((name+" "+family).length()<4){
								String parts[]=(namesc
										.getString(namesc
												.getColumnIndexOrThrow(Contacts.DISPLAY_NAME))).split(" ");
								if (parts.length>0){
									name=parts[0];
									family="";
									for (int i=1;i<parts.length;i++)
										  family=family+parts[i]+" ";
								}


							}
							etName.setText(name);
							etFamily.setText(family);

						}

						/////////////////////////////////// load photo ///////////////////////////////////////
						Uri contactUri = ContentUris.withAppendedId(
								Contacts.CONTENT_URI, Long.valueOf(id));
						Uri photoUri = Uri.withAppendedPath(contactUri,
								Contacts.Photo.CONTENT_DIRECTORY);
						Cursor photoc = getContentResolver().query(photoUri,
								new String[] { Contacts.Photo.PHOTO }, null,
								null, null);

						if (photoc != null)
							try {
								if (photoc.moveToFirst()) {
									byte[] pdata = photoc.getBlob(0);
									if (pdata != null) {
										// InputStream is= new
										// ByteArrayInputStream(pdata);
										_image = pdata;
										iv.setImageBitmap(BitmapFactory
												.decodeByteArray(pdata, 0,
														pdata.length));

										imageloaded = true;
									}
								}
							} finally {
								photoc.close();
							}

					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				break;

            }

        fieldsChanged = true;

    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }




    public static class AutoCompleteAdapter extends ArrayAdapter<Field> {
        private LayoutInflater mLayoutInflater;
        ArrayList<Field> items;
        private ArrayList<Field> itemsAll;
        private ArrayList<Field> suggestions;
        Context basecontext;


        public AutoCompleteAdapter(Context context, int viewResourceId, ArrayList<Field> items) {
            super(context, viewResourceId, items);
            mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            this.items = items;
            this.itemsAll = (ArrayList<Field>) items.clone();
            this.suggestions = new ArrayList<Field>();
            this.basecontext=context;
        }



        @Override
        public Filter getFilter() {
            return nameFilter;
        }

        Filter nameFilter = new Filter() {
            @Override
            public String convertResultToString(Object resultValue) {
                String str = ((Field)(resultValue)).toString();
                return str;
            }
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                if(constraint != null) {
                    suggestions.clear();
                    for (Field customer : itemsAll) {
                        //if(customer.toString().toLowerCase().startsWith(constraint.toString().toLowerCase())){
                        if(customer.toString().toLowerCase().contains(constraint.toString().toLowerCase())){
                            suggestions.add(customer);
                        }
                    }
                    FilterResults filterResults = new FilterResults();
                    filterResults.values = suggestions;
                    filterResults.count = suggestions.size();
                    return filterResults;
                } else {
                    return new FilterResults();
                }
            }
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                ArrayList<Field> filteredList = (ArrayList<Field>) results.values;
                if(results != null && results.count > 0) {
                    clear();
                    for (Field c : filteredList) {
                        add(c);
                    }
                    notifyDataSetChanged();
                }
            }
        };




        @Override
        public long getItemId(int position) {
            return position;
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
        /*Country country = getItem(position);
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.item_country, null);
        }
        ImageView imageView = (ImageView) convertView.findViewById(R.id.image);
        imageView.setImageResource(country.getResId());
        return convertView;*/
            final ViewHolder holder;
            if (convertView == null) {
                convertView = mLayoutInflater.inflate(R.layout.autocomplete_field_row, parent, false);
                holder = new ViewHolder();
                holder.imageView1 = (ImageView) convertView.findViewById(R.id.imageView1);
                holder.tv_name = (TextView) convertView.findViewById(R.id.tv_name);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            Field field=items.get(position);

            if (field.image_res_id > 0) {
                holder.imageView1.setImageResource(field.image_res_id);
                holder.imageView1.setVisibility(View.VISIBLE);
            } else
                holder.imageView1.setVisibility(View.GONE);

            holder.tv_name.setText(field.name);
            //holder.tv_group.setText(field.parent_name);

            return convertView;
        }

        private class ViewHolder {
            public ImageView imageView1;
            public TextView tv_name;
        }

    }

    AutoCompleteTextView tvDateExp;

    public void setupAutoCompleteText(){

        String[] dates=getResources().getStringArray(R.array.dateTypes);
        String[] ress=getResources().getStringArray(R.array.dateTypes_images);
        ArrayList<Field> fields=new ArrayList<>();
        for (String temp:dates){
            Field f=new Field(temp,-1);
            fields.add(f);

        }

        AutoCompleteAdapter autoCompleteAdapter = new AutoCompleteAdapter(this,R.layout.autocomplete_field_row, fields);
        tvDateExp = (AutoCompleteTextView) findViewById(R.id.tvDateExp);
        tvDateExp.setAdapter(autoCompleteAdapter);
        tvDateExp.setThreshold(2);
        tvDateExp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        tvDateExp.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                //etDateExp.setText(fields.get(i).toString());



            }
        });

    }
}
