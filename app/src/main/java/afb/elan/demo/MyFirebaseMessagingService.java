package afb.elan.demo;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import co.pushe.plus.Pushe;
import co.pushe.plus.fcm.FcmHandler;
import co.pushe.plus.fcm.PusheFCM;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private FcmHandler fcmHandler = Pushe.getPusheService(PusheFCM.class).getFcmHandler();

    @Override
    public void onMessageReceived(@NonNull RemoteMessage message) {
        super.onMessageReceived(message);
        if (fcmHandler.onMessageReceived(message)) {
            // Message is for Pushe
            Log.e("Pushe Title","title");

            return;
        }
        super.onMessageReceived(message);
        if (message.getNotification()!=null){
            String title=message.getNotification().getTitle();
            String body=message.getNotification().getBody();
            Log.e("FCM Notification Title",title);
            Log.e("FCM Notification Body",body);
        }

    }



    @Override
    public void onNewToken(String s) {
        fcmHandler.onNewToken(s);
        super.onNewToken(s);

        // Token is refreshed
    }

    @Override
    public void onMessageSent(String s) {
        fcmHandler.onMessageSent(s);
        super.onMessageSent(s);

        // Message sent
    }

    @Override
    public void onDeletedMessages() {
        fcmHandler.onDeletedMessages();
        super.onDeletedMessages();

        // Message was deleted
    }

    @Override
    public void onSendError(String s, Exception e) {
        fcmHandler.onSendError(s, e);
        super.onSendError(s, e);

        // Error sent
    }
}
