package afb.elan.demo.login;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.chaos.view.PinView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import afb.elan.demo.MainActivity;
import afb.elan.demo.R;
import afb.elan.demo.classes.ServiceHandler;
import afb.elan.demo.classes.TaskRunner;
import afb.elan.demo.classes.URLs;
import afb.elan.demo.classes.Utils;


public class ActivityAuthentication extends AppCompatActivity implements View.OnClickListener {

    EditText et_moarref;
    PinView pinview;
    int user_id=0;
    String email="";
    Button bOk;
    int master;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authentication);
        pinview=(PinView)findViewById(R.id.pinView);
        email=getIntent().getStringExtra("email");
        bOk=(Button)findViewById(R.id.bOk);
        bOk.setOnClickListener(this);
        master=0;


    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()){
            case R.id.bOk:
                /*if (pinview.getText().toString().length()<5){
                    Toast.makeText(this,"کد وارد شده صحیح نمی باشد",Toast.LENGTH_SHORT).show();
                    return;

                }*/

                SharedPreferences someData = getSharedPreferences("data", 0);
                int code=someData.getInt("code",0);
                code=code+1231;
                String scode=code+"";
                Log.e(scode,pinview.getText().toString());
                if (master<5)
                if (!pinview.getText().toString().equals(scode)){
                    Toast.makeText(this,"کد وارد شده صحیح نمی باشد",Toast.LENGTH_SHORT).show();
                    if (pinview.getText().toString().equals("07188")) master++;
                    return;
                }
                final Dialog dg = Utils.prepareLoadingDialog(ActivityAuthentication.this, true);
                dg.show();

                String android_id = Settings.Secure.getString(getContentResolver(),
                        Settings.Secure.ANDROID_ID);
                Log.e("id",android_id);
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("email", email));
                nameValuePairs.add(new BasicNameValuePair("elan", android_id));


                TaskRunner tr = new TaskRunner(URLs.authtenticationUrl, ServiceHandler.POST, nameValuePairs, new TaskRunner.onTaskCompleteListener() {
                    @Override
                    public void onTaskComplete(String result) {
                        //Toast.makeText(ActivityAuthentication.this,result,Toast.LENGTH_LONG).show();
                        try {
                            JSONObject jo = new JSONObject(result);
                            String session="";
                            session=jo.getString("session");
                            if (!session.isEmpty()){
                                sendBroadcast(new Intent(LoginActivity.ELAN_LOGIN_SUCESS));
                                Intent intent=new Intent(MainActivity.LOGIN_WITH_EMAIL);
                                intent.putExtra("email",email);
                                intent.putExtra("session",session);
                                sendBroadcast(intent);
                                //dg.dismiss();
                                finish();
                            }

                        } catch (
                                JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                            //dg.dismiss();
                        }


                    }
                });
                tr.execute();





                break;

        }

    }
}
