package afb.elan.demo.login;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import afb.elan.demo.R;
import afb.elan.demo.classes.ServiceHandler;
import afb.elan.demo.classes.TaskRunner;
import afb.elan.demo.classes.URLs;
import afb.elan.demo.classes.Utils;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

	String returned_username=null,returned_pass=null;
	LoginDialog ld;
	EditText email;

	public static final String ELAN_LOGIN_SUCESS = "ELAN.LOGIN.SUCESS";
	private BaseActivityReceiver baseActivityReceiver = new BaseActivityReceiver();
	public static final IntentFilter INTENT_FILTER = createIntentFilter();
	private static IntentFilter createIntentFilter() {
		IntentFilter filter = new IntentFilter();
		filter.addAction(ELAN_LOGIN_SUCESS);
		return filter;
	}
	protected void registerBaseActivityReceiver() {
		registerReceiver(baseActivityReceiver, INTENT_FILTER);
	}
	protected void unRegisterBaseActivityReceiver() {
		unregisterReceiver(baseActivityReceiver);
	}

	public class BaseActivityReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals(ELAN_LOGIN_SUCESS)) {
				finish();
			}
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		AppCompatButton login=(AppCompatButton) findViewById(R.id.bOk);
		login.setOnClickListener(this);
		email=(EditText) findViewById(R.id.et_email);
		registerBaseActivityReceiver();
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()){
			case R.id.bOk:
				if (email.getText().toString().isEmpty()) {

					email.setError(getResources().getString(R.string.fillIt));
					email.requestFocus();
					break;
				}

				final Dialog dg = Utils.prepareLoadingDialog(this, true);
				dg.show();

				String android_id = Settings.Secure.getString(getContentResolver(),
						Settings.Secure.ANDROID_ID);
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
				nameValuePairs.add(new BasicNameValuePair("email", email.getText().toString()));
				nameValuePairs.add(new BasicNameValuePair("elan", android_id));


				TaskRunner tr = new TaskRunner(URLs.signInUrl, ServiceHandler.POST, nameValuePairs, new TaskRunner.onTaskCompleteListener() {
					@Override
					public void onTaskComplete(String result) {
						//Toast.makeText(LoginActivity.this,result,Toast.LENGTH_LONG).show();
						try {
							JSONObject jo = new JSONObject(result);
							int code= jo.getInt("code");
							if (code>0){
								SharedPreferences someData = getSharedPreferences("data", 0);
								SharedPreferences.Editor editor = someData.edit();
								//editor.putString("email",email.getText().toString());
								editor.putInt("code",code-1231);
								editor.commit();
								dg.dismiss();
								Intent auth=new Intent(LoginActivity.this, ActivityAuthentication.class);
								auth.putExtra("email",email.getText().toString());
								startActivity(auth);
								finish();
							}

						} catch (
					JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						dg.dismiss();
					}


					}
				});
				tr.execute();

				break;
		}
	}

	@Override
	public void finish() {
		//if (guest)
		unRegisterBaseActivityReceiver();
		super.finish();

	}

	private class LoginDialog extends Dialog {
		
		EditText etEmail,etPass;

		public LoginDialog(Context context, int theme) {
			super(context, theme);
			requestWindowFeature(Window.FEATURE_NO_TITLE);
			setContentView(R.layout.login_dialog);
			setCanceledOnTouchOutside(true);	
			setupViews();
			// TODO Auto-generated constructor stub
		}


		public LoginDialog(Context context) {
			super(context);
			requestWindowFeature(Window.FEATURE_NO_TITLE);
			setContentView(R.layout.login_dialog);
			setCanceledOnTouchOutside(true);
			setupViews();
			// TODO Auto-generated constructor stub
		}


		private void setupViews() {
			// TODO Auto-generated method stub
			Button bLogin=(Button)findViewById(R.id.bLogin);
			Button bSignup=(Button)findViewById(R.id.bSignup);
			Button bDismiss=(Button)findViewById(R.id.bDismissLoginDialog);
			TextView tv_forgot=(TextView)findViewById(R.id.tv_forgot);
			etEmail=(EditText)findViewById(R.id.et_username);
			etPass=(EditText)findViewById(R.id.et_password);
			SharedPreferences someData=getSharedPreferences("birthdata", 0);
			etEmail.setText(someData.getString("loginEmail", ""));
			etEmail.selectAll();
		}


		@Override
		public void dismiss() {
			// TODO Auto-generated method stub
			super.dismiss();
			//if (returned_username==null) 
				finish();
		}





		
	}
	
	

	









	
}
