package afb.elan.demo;



import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.widget.Toast;

public class SmsSender extends Activity{
	

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		String phoneNumber = getIntent().getExtras().getString("phoneNumber");
		//String phoneNumber = "09354077452";
		String message=getIntent().getExtras().getString("smsBody");

		// ---sends an SMS message to another device--
			String SENT = "SMS_SENT";
			String DELIVERED = "SMS_DELIVERED";
			PendingIntent sentPI = PendingIntent.getBroadcast(getBaseContext(),
					0, new Intent(SENT), 0);
			PendingIntent deliveredPI = PendingIntent.getBroadcast(
					getBaseContext(), 0, new Intent(DELIVERED), 0);
			// ---when the SMS has been sent---
			registerReceiver(new BroadcastReceiver() {
				public void onReceive(Context arg0, Intent arg1) {
					switch (getResultCode()) {
					case Activity.RESULT_OK:
						Toast.makeText(getBaseContext(), "SMS sent",
								Toast.LENGTH_SHORT).show();
						break;
					case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
						Toast.makeText(getBaseContext(), "Generic failure",
								Toast.LENGTH_SHORT).show();
						break;
					case SmsManager.RESULT_ERROR_NO_SERVICE:
						Toast.makeText(getBaseContext(), "No service",
								Toast.LENGTH_SHORT).show();
						break;
					case SmsManager.RESULT_ERROR_NULL_PDU:
						Toast.makeText(getBaseContext(), "Null PDU",
								Toast.LENGTH_SHORT).show();
						break;
					case SmsManager.RESULT_ERROR_RADIO_OFF:
						Toast.makeText(getBaseContext(), "Radio off",
								Toast.LENGTH_SHORT).show();
						break;
					}
				}
			}, new IntentFilter(SENT));
	registerReceiver(new BroadcastReceiver() {
		@Override
		public void onReceive(Context arg0, Intent arg1) {
			switch (getResultCode()) {
			case Activity.RESULT_OK:
				Toast.makeText(getBaseContext(), "SMS delivered",
						Toast.LENGTH_SHORT).show();
				break;
			case Activity.RESULT_CANCELED:
				Toast.makeText(getBaseContext(), "SMS not delivered",
						Toast.LENGTH_SHORT).show();
				break;
			}
		}
	}, new IntentFilter(DELIVERED));
	SmsManager sms = SmsManager.getDefault();
	sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
	finish();
	
/*
	String ussdCode = "*" + "123" + Uri.encode("#");
	startActivity(new Intent("android.intent.action.CALL", Uri.parse("tel:" + ussdCode)));
	*/
}
	
	public SmsSender(Context c, String targetNumber, String smsBody){
		


		// ---sends an SMS message to another device--
			String SENT = "SMS_SENT";
			String DELIVERED = "SMS_DELIVERED";
			PendingIntent sentPI = PendingIntent.getBroadcast(c,
					0, new Intent(SENT), 0);
			PendingIntent deliveredPI = PendingIntent.getBroadcast(
					c, 0, new Intent(DELIVERED), 0);
			// ---when the SMS has been sent---
			// ---when the SMS has been delivered---
			/*registerReceiver(new BroadcastReceiver() {
				@Override
				public void onReceive(Context arg0, Intent arg1) {
					switch (getResultCode()) {
					case Activity.RESULT_OK:
						Toast.makeText(getBaseContext(), "SMS delivered",
								Toast.LENGTH_SHORT).show();
						break;
					case Activity.RESULT_CANCELED:
						Toast.makeText(getBaseContext(), "SMS not delivered",
								Toast.LENGTH_SHORT).show();
						break;
					}
				}
			}, new IntentFilter(DELIVERED));*/
			SmsManager sms = SmsManager.getDefault();
			sms.sendTextMessage(targetNumber, null, smsBody, sentPI, deliveredPI);
		
	}
	

		
	/*
		String ussdCode = "*" + "123" + Uri.encode("#");
		startActivity(new Intent("android.intent.action.CALL", Uri.parse("tel:" + ussdCode)));
		*/

}
