package afb.elan.demo.Db;

import java.io.Serializable;

public class NoteObject implements Serializable {
	private static final long serialVersionUID = -1849501982023482504L;


	public int note_id=0;
	public String title;
	public String body;
	public String pass;
	public int deleted;
	public String date;
	public String time;
	public String date_created;
	private boolean _checked;

	public NoteObject(){ }


	public NoteObject(int _id,String _title,String _body){
		note_id=_id;
		title=_title;
		body=_body;
		pass="";
		date="";
		time="";
		date_created="";
		_checked=false;

	}

	public void setContent(int _id,String _title,String _body,String _pass,String _date,String _time, String _date_created){
		 note_id=_id;
		title=_title;
		body=_body;
		pass=_pass;
		date=_date;
		time=_time;
		 date_created=_date_created;

	}

	public boolean hasAlarm(){
		if (date==null) return false;
		if (date.isEmpty()) return false;
		return true;
	}

	public boolean isProtected(){
		if (pass==null) return false;
		if (pass.isEmpty()) return false;
		return true;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return title;
	}

	public boolean getChecked(){
		return _checked;
	}

	public void setChecked(boolean checked){
		_checked=checked;
	}

}


	


