package afb.elan.demo.Db;


import android.content.Context;

import java.io.Serializable;

import afb.elan.demo.R;


public class BirthdayRecord implements Serializable {
	private static final long serialVersionUID = -1749504982023482503L;

	private int _id;
	private String _name;
	private String _family;
	private String _birthday_date;
	private String _birthday_date_g;
	private String _birthday_day;
	private String _birthday_day_g;
	private String _mobile;
	private String _phone;
	private String _email;
	private boolean _checked;
	private int _dateType;
	private String _smsbody;
	private byte[] _image;
	private boolean _hasImage;
	private int _solarValue;
	private String _custom_date_exp;
	
	
	public BirthdayRecord(){
		_id=-1;
		_name=null;
		_family=null;
		_birthday_date=null;
		_birthday_date_g=null;
		_birthday_day=null;
		_birthday_day_g=null;
		_mobile=null;
		_phone=null;
		_email=null;
		_checked=false;
		_dateType=-1;
		_smsbody=null;
		_image=null;
		_hasImage=false;
		_solarValue=-1;
		_custom_date_exp=null;
	}

	public BirthdayRecord(int id, String name){
		_id=id;
		_name=name;
		_family=name;
		_birthday_date=null;
		_birthday_date_g=null;
		_birthday_day=null;
		_birthday_day_g=null;
		_mobile=name;
		_phone=null;
		_email=null;
		_checked=false;
		_dateType=-1;
		_smsbody=null;
		_image=null;
		_hasImage=false;
		_solarValue=-1;
		_custom_date_exp=null;
	}
	
	public void setContent(int id,String name,String family,String birthday_date,String birthday_day,String birthday_date_g,String birthday_day_g,String mobile,String phone,String email, int dateType,String smsbody,byte[] image,int solarValue, String custom_date_exp)
	{
		_id=id;
		_name=name;
		_family=family;
		_birthday_date=birthday_date;
		_birthday_date_g=birthday_date_g;
		_birthday_day=birthday_day;
		_birthday_day_g=birthday_day_g;
		_mobile=mobile;
		_phone=phone;
		_email=email;
		_dateType=dateType;
		_smsbody=smsbody;
		_image=image;
		_hasImage=(image!=null);
		_solarValue=solarValue;
		_custom_date_exp=custom_date_exp;
	}
	
	public int getId(){
		return _id;
	}
	
	public String getName(){
		return _name;
	}
	
	public String getFullname(){
		return _name+" "+_family;
	}	
	
	public String getFamily(){
		return _family;
	}
	
	public String getBirthday_Date(){
		return _birthday_date;
	}
	
	public String getBirthday_Date_georgian(){
		return _birthday_date_g;
	}
	
	public String getBirthday_Day_georgian(){
		return _birthday_day_g;
	}	
	
	public String getBirthday_Day(){
		return _birthday_day;
	}	
	
	public String getMobile(){
		return _mobile;
	}
	
	public String getPhone(){
		return _phone;
	}
	
	public String getEmail(){
		return _email;
	}
	
	public boolean getChecked(){
		return _checked;
	}
	
	public void setChecked(boolean checked){
		_checked=checked;
	}
	
	public int getDateType(){
		return _dateType;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		//Context a = null;
		//String s[]=a.getResources().getStringArray(R.array.dateTypes);
		return _name+" "+_family;
	}
	
	public String getSmsBody(){
		return _smsbody;
	}	
	
	/*public byte[] getImageAsByteArray(){
		if (_image==null)
			return null;
		else
		{
			int bytes = _image.getWidth()*_image.getHeight()*4; //calculate how many bytes our image consists of. Use a different value than 4 if you don't use 32bit images.

			ByteBuffer buffer = ByteBuffer.allocate(bytes); //Create a new buffer
			_image.copyPixelsToBuffer(buffer); //Move the byte data to the buffer

			byte[] array = buffer.array();
			return array;
		}
	}*/
	
	public byte[] getImage(){
		return _image;
	}
	
	public boolean hasImage(){
		return _hasImage;
	}
	
	public int getSolarValue(){
		return _solarValue;
	}

	public String getDateDiscription(Context context) {
		// TODO Auto-generated method stub
		//if (_dateType==4)
			return _custom_date_exp+" "+getFullname();
		//else
		    //return context.getResources().getStringArray(R.array.dateTypes)[_dateType]+" "+getFullname();
	}
	
	public String getSmsStatusForPopup(Context context) {
		// TODO Auto-generated method stub
		//String dt[]=context.getResources().getStringArray(R.array.notificationTypesValues);
		if (_smsbody.isEmpty())
			return "";
		else
			return context.getResources().getString(R.string.haveSmsPopup);
	}	
	
	public String getSmsStatus(Context context) {
		// TODO Auto-generated method stub
		//String dt[]=context.getResources().getStringArray(R.array.notificationTypesValues);
		if (_smsbody==null)
			return context.getResources().getString(R.string.dontHaveSms);
		else
			if (_smsbody.isEmpty())
				return context.getResources().getString(R.string.dontHaveSms);
			else
				return context.getResources().getString(R.string.haveSms);
	}
	
	public String getCustomDateExp(){
		return _custom_date_exp;
	}
	
}
