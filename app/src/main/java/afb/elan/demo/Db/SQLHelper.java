package afb.elan.demo.Db;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

import afb.elan.demo.classes.Utils;

public class SQLHelper {
	
	public static final String KEY_ID="_id";
	public static final String KEY_NAME="_name";
	public static final String KEY_FAMILY="_family";
	public final static String KEY_BIRTHDAY_DATE="_birthday_date";
	public final static String KEY_BIRTHDAY_DAY="_birthday_day";
	public final static String KEY_BIRTHDAY_DATE_G="_birthday_date_georgian";
	public final static String KEY_BIRTHDAY_DAY_G="_birthday_day_georgian";
	public final static String KEY_MOBILE="_mobile";
	public final static String KEY_PHONE="_phone";
	public final static String KEY_EMAIL="_email";
	public final static String KEY_DATETYPE="_datetype";
	public final static String KEY_SMSBODY="_smsbody";
	public final static String KEY_IMAGE="_image";
	public final static String KEY_SOLARVALUE="_solar_day_value";
	public final static String KEY_CUSTOM_DATE_EXP="_custom_date_exp";


	public static final String NOTES_KEY_ROW = "_id";
	public static final String NOTES_KEY_TITLE = "N_Title";
	public static final String NOTES_KEY_BODY = "N_Body";
	public static final String NOTES_KEY_DATE = "N_Date";
	public static final String NOTES_KEY_TIME = "N_Time";
	public static final String NOTES_KEY_DATECREATED = "N_DateCreated";
	public static final String NOTES_KEY_PASS = "N_Pass";
	
	
	public final static String SORT_ORDER_REMAIN="remain";

	
	public final static String DB_NAME="contactsDb";
	private final static String DB_TABLE="_dates";
	private static final String NOTES_DB_TABLE = "Notes";
	private final static int DB_VERSION=9;

	private SQLiteDatabase ourDb;
	private dbHelper ourhelper;
	private Context context;
	private Cursor cursor;
	private String sort_order;
	
	private class dbHelper extends SQLiteOpenHelper{

		public dbHelper(Context context) {
			super(context, DB_NAME, null, DB_VERSION);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void onCreate(SQLiteDatabase sdb) {
			// TODO Auto-generated method stub
			sdb.execSQL("CREATE TABLE " + DB_TABLE + " ( " 
					+ KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " 
					+ KEY_NAME + " TEXT NOT NULL, " 
					+ KEY_FAMILY + " TEXT NOT NULL, "
					+ KEY_BIRTHDAY_DATE + " TEXT NOT NULL, " 
					+ KEY_BIRTHDAY_DAY  + " TEXT NOT NULL, " 
					+ KEY_BIRTHDAY_DATE_G + " TEXT NOT NULL, "
					+ KEY_BIRTHDAY_DAY_G + " TEXT NOT NULL, "
					+ KEY_MOBILE + " TEXT, " 
					+ KEY_PHONE + " TEXT, " 
					+ KEY_EMAIL + " TEXT, "
					+ KEY_DATETYPE+" INTEGER, "
					+ KEY_SMSBODY+" TEXT, "
					+ KEY_IMAGE + " BLOB, "
					+ KEY_SOLARVALUE + " INTEGER, "
					+ KEY_CUSTOM_DATE_EXP + " TEXT); ");

			sdb.execSQL("CREATE TABLE " + NOTES_DB_TABLE + " ( " + NOTES_KEY_ROW
					+ " INTEGER PRIMARY KEY AUTOINCREMENT, " + NOTES_KEY_TITLE
					+ " TEXT NOT NULL, " + NOTES_KEY_BODY + " TEXT NOT NULL, "
					+ NOTES_KEY_DATE + " TEXT NOT NULL, "+ NOTES_KEY_DATECREATED + " TEXT NOT NULL, " + NOTES_KEY_TIME
					+ " TEXT NOT NULL, " + NOTES_KEY_PASS
					+ " TEXT NOT NULL); ");

		}

		@Override
		public void onUpgrade(SQLiteDatabase sdb, int oldVersion, int newVersion) {
			// TODO Auto-generated method stub
			if (oldVersion<newVersion)
			{
			}
			//onCreate(sdb);			
		}

		@Override
		public void onDowngrade(SQLiteDatabase db, int oldVersion,
				int newVersion) {
			// TODO Auto-generated method stub
			super.onDowngrade(db, oldVersion, newVersion);
		}	
		
		
		
	} // end of dbHelper class
	
	public SQLHelper(Context c, String sort_order) {
		context = c;
		this.sort_order=sort_order;
	}

	public SQLHelper(Context c) {
		context = c;
		this.sort_order=KEY_BIRTHDAY_DAY;
	}
	
	public SQLHelper openDb()  throws SQLException{
		ourhelper = new dbHelper(context);
		ourDb = ourhelper.getWritableDatabase();
		return this;

	}
	
	public int getVersion(){
		return ourDb.getVersion();
	}
	
	public void closeDb(){
		ourhelper.close();
	}
	
	public String getSortOrder(){
		return sort_order;
	}
	
	public void setSortOrder(String sort_order){
		this.sort_order=sort_order;
	}
	
	/*public long AddRecord(String name, String family, String birthday_date,String birthday_day,String birthday_date_g,String birthday_day_g, String mobile, String phone, String email, int dateType,String smsbody) {
		ContentValues cv = new ContentValues();
		cv.put(KEY_NAME, name);
		cv.put(KEY_FAMILY, family);
		cv.put(KEY_BIRTHDAY_DATE, birthday_date);
		cv.put(KEY_BIRTHDAY_DAY, birthday_day);
		cv.put(KEY_BIRTHDAY_DATE_G, birthday_date_g);
		cv.put(KEY_BIRTHDAY_DAY_G, birthday_day_g);
		cv.put(KEY_MOBILE, mobile);
		cv.put(KEY_PHONE, phone);
		cv.put(KEY_EMAIL, email);
		cv.put(KEY_DATETYPE, dateType);
		cv.put(KEY_SMSBODY, smsbody);
		return ourDb.insert(DB_TABLE, null, cv);
	}	*/
	
	public long AddRecord(BirthdayRecord br) {
		ContentValues cv = new ContentValues();
		cv.put(KEY_NAME, br.getName());
		cv.put(KEY_FAMILY, br.getFamily());
		cv.put(KEY_BIRTHDAY_DATE, br.getBirthday_Date());
		cv.put(KEY_BIRTHDAY_DAY, br.getBirthday_Day());
		cv.put(KEY_BIRTHDAY_DATE_G, br.getBirthday_Date_georgian());
		cv.put(KEY_BIRTHDAY_DAY_G, br.getBirthday_Day_georgian());
		cv.put(KEY_MOBILE, br.getMobile());
		cv.put(KEY_PHONE, br.getPhone());
		cv.put(KEY_EMAIL, br.getEmail());
		cv.put(KEY_DATETYPE, br.getDateType());
		cv.put(KEY_SMSBODY, br.getSmsBody());
		cv.put(KEY_IMAGE, br.getImage());
		cv.put(KEY_SOLARVALUE, br.getSolarValue());
		cv.put(KEY_CUSTOM_DATE_EXP, br.getCustomDateExp());
		
		return ourDb.insert(DB_TABLE, null, cv);
	}	
	
	public void UpdateRecord(long R_ID, BirthdayRecord br) throws SQLException{
		// TODO Auto-generated method stub
		ContentValues cv = new ContentValues();
		cv.put(KEY_NAME, br.getName());
		cv.put(KEY_FAMILY, br.getFamily());
		cv.put(KEY_BIRTHDAY_DATE, br.getBirthday_Date());
		cv.put(KEY_BIRTHDAY_DAY, br.getBirthday_Day());
		cv.put(KEY_BIRTHDAY_DATE_G, br.getBirthday_Date_georgian());
		cv.put(KEY_BIRTHDAY_DAY_G, br.getBirthday_Day_georgian());
		cv.put(KEY_MOBILE, br.getMobile());
		cv.put(KEY_PHONE, br.getPhone());
		cv.put(KEY_EMAIL, br.getEmail());
		cv.put(KEY_DATETYPE, br.getDateType());
		cv.put(KEY_SMSBODY, br.getSmsBody());
		cv.put(KEY_IMAGE, br.getImage());
		cv.put(KEY_SOLARVALUE, br.getSolarValue());
		cv.put(KEY_CUSTOM_DATE_EXP, br.getCustomDateExp());
		ourDb.update(DB_TABLE, cv, KEY_ID + "=" + R_ID, null);	
	}
	
	public boolean DeleteRecord(long R_ID) {
		// TODO Auto-generated method stub
		ourDb.delete(DB_TABLE, KEY_ID+"="+R_ID, null);
		return true;

	}
	
	public void startRead(int current_day_value) {
		// TODO Auto-generated method stub
		
		if (sort_order.equals(SORT_ORDER_REMAIN))
		{
			String sql="select *,(case when _solar_day_value-"+current_day_value+">=0 then _solar_day_value-"+current_day_value+" else _solar_day_value-"+current_day_value+"+365 end) as remain_days from "+DB_TABLE+" order by remain_days";
			cursor=ourDb.rawQuery(sql, null);
		}
		else
		{
			String[] columns=new String[]{KEY_ID,KEY_NAME,KEY_FAMILY,KEY_BIRTHDAY_DATE,KEY_BIRTHDAY_DAY,KEY_BIRTHDAY_DATE_G,KEY_BIRTHDAY_DAY_G,KEY_MOBILE,KEY_PHONE,KEY_EMAIL,KEY_DATETYPE,KEY_SMSBODY,KEY_IMAGE,KEY_SOLARVALUE,KEY_CUSTOM_DATE_EXP};
			cursor=ourDb.query(DB_TABLE, columns, null, null, null, null, sort_order);
		}
		
		cursor.moveToFirst();
	}
	
	public BirthdayRecord readNext() {
		// TODO Auto-generated method stub
		BirthdayRecord br=new BirthdayRecord();
		if(!cursor.isAfterLast()){
			br.setContent(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), 
					      cursor.getString(4), cursor.getString(5),cursor.getString(6), cursor.getString(7), cursor.getString(8), cursor.getString(9),cursor.getInt(10), cursor.getString(11), cursor.getBlob(12),cursor.getInt(13), cursor.getString(14));
			cursor.moveToNext();
		}
		return br;

	}
	
	public void fetchTodayBirthdays(String currentDay) {
		// TODO Auto-generated method stub
		String[] columns=new String[]{KEY_ID,KEY_NAME,KEY_FAMILY,KEY_BIRTHDAY_DATE,KEY_BIRTHDAY_DAY,KEY_BIRTHDAY_DATE_G,KEY_BIRTHDAY_DAY_G,KEY_MOBILE,KEY_PHONE,KEY_EMAIL,KEY_DATETYPE,KEY_SMSBODY,KEY_IMAGE,KEY_SOLARVALUE,KEY_CUSTOM_DATE_EXP};
		
		cursor=ourDb.query(DB_TABLE, columns, KEY_BIRTHDAY_DAY+"="+"\""+currentDay+"\"", null, null, null, KEY_BIRTHDAY_DATE);		
		cursor.moveToFirst();
	}


	public ArrayList<SearchNode> fetchTodayNodes(String currentDay){
		String raw="select * from (select _id, "+SearchNode.TYPE_DATE+" as ttype,_custom_date_exp || ' ' || _name || ' ' || _family as title,_mobile,_smsbody,_birthday_date as _date,_image from _dates "+
				"union all select _id, "+SearchNode.TYPE_NOTE+" as ttype,N_TITLE as title,'' as _mobile,N_BODY as _smsbody,N_Date as _date, null as _image from Notes) as Table1 "+
				"where _date like '%"+currentDay+"'";
		cursor=ourDb.rawQuery(raw, null);
		ArrayList<SearchNode> temp=new ArrayList();
		SearchNode sn;
		cursor.moveToFirst();
		while (!cursor.isAfterLast()){
			sn=new SearchNode();
			sn.id=cursor.getInt(0);
			sn.type=cursor.getInt(1);
			sn.title=cursor.getString(2);
			sn.mobile=cursor.getString(3);
			sn.sms=cursor.getString(4);
			sn.date=cursor.getString(5);
			sn.image=cursor.getBlob(6);
			temp.add(sn);
			cursor.moveToNext();
		}

		return temp;

	}



	public void fetchNextDaysBirthdays(int nextDayValue) {
		// TODO Auto-generated method stub
		String[] columns=new String[]{KEY_ID,KEY_NAME,KEY_FAMILY,KEY_BIRTHDAY_DATE,KEY_BIRTHDAY_DAY,KEY_BIRTHDAY_DATE_G,KEY_BIRTHDAY_DAY_G,KEY_MOBILE,KEY_PHONE,KEY_EMAIL,KEY_DATETYPE,KEY_SMSBODY,KEY_IMAGE,KEY_SOLARVALUE,KEY_CUSTOM_DATE_EXP};
		
		cursor=ourDb.query(DB_TABLE, columns, KEY_SOLARVALUE+"="+nextDayValue, null, null, null, KEY_BIRTHDAY_DATE);		
		cursor.moveToFirst();
	}
	
	public void startReadWithFilter(String filter) {
		// TODO Auto-generated method stub
		
		String currentSelection="(_name like ?) or (_family like ?)";
		String[] currentSelectionArgs=new String[] {"%"+filter+"%","%"+filter+"%"};
		//String new_sort_order=(!sort_order.equals("remain"))?sort_order:KEY_BIRTHDAY_DATE;
		
		
		if (sort_order.equals(SORT_ORDER_REMAIN))
		{
			//String sql="select _id,_name,_family,_birthday_date,_birthday_day,_birthday_date_georgian,_birthday_day_georgian,_mobile,_phone,_email,_datetype,_smsbody,_solar_day_value,_custom_date_exp,(case when _solar_day_value-"+current_day_value+">=0 then _solar_day_value-"+current_day_value+" else _solar_day_value-"+current_day_value+"+365 end) as remain_days from "+DB_TABLE+" order by remain_days";
			//cursor=ourDb.rawQuery(sql, null);
			String[] columns=new String[]{KEY_ID,KEY_NAME,KEY_FAMILY,KEY_BIRTHDAY_DATE,KEY_BIRTHDAY_DAY,KEY_BIRTHDAY_DATE_G,KEY_BIRTHDAY_DAY_G,KEY_MOBILE,KEY_PHONE,KEY_EMAIL,KEY_DATETYPE,KEY_SMSBODY,KEY_IMAGE,KEY_SOLARVALUE,KEY_CUSTOM_DATE_EXP};
			cursor=ourDb.query(DB_TABLE, columns, currentSelection, currentSelectionArgs, null, null, KEY_BIRTHDAY_DATE);	
		}
		else
		{
			String[] columns=new String[]{KEY_ID,KEY_NAME,KEY_FAMILY,KEY_BIRTHDAY_DATE,KEY_BIRTHDAY_DAY,KEY_BIRTHDAY_DATE_G,KEY_BIRTHDAY_DAY_G,KEY_MOBILE,KEY_PHONE,KEY_EMAIL,KEY_DATETYPE,KEY_SMSBODY,KEY_IMAGE,KEY_SOLARVALUE,KEY_CUSTOM_DATE_EXP};
			cursor=ourDb.query(DB_TABLE, columns, currentSelection, currentSelectionArgs, null, null, sort_order);		
		
		}
		cursor.moveToFirst();
	}

	public ArrayList<BirthdayRecord> fetchAllRecords(int current_day_value) {
		if (sort_order.equals(SORT_ORDER_REMAIN))
		{
			String sql="select *,(case when _solar_day_value-"+current_day_value+">=0 then _solar_day_value-"+current_day_value+" else _solar_day_value-"+current_day_value+"+365 end) as remain_days from "+DB_TABLE+" order by remain_days";
			cursor=ourDb.rawQuery(sql, null);
		}
		else
		{
			String[] columns=new String[]{KEY_ID,KEY_NAME,KEY_FAMILY,KEY_BIRTHDAY_DATE,KEY_BIRTHDAY_DAY,KEY_BIRTHDAY_DATE_G,KEY_BIRTHDAY_DAY_G,KEY_MOBILE,KEY_PHONE,KEY_EMAIL,KEY_DATETYPE,KEY_SMSBODY,KEY_IMAGE,KEY_SOLARVALUE,KEY_CUSTOM_DATE_EXP};
			cursor=ourDb.query(DB_TABLE, columns, null, null, null, null, sort_order);
		}
		ArrayList<BirthdayRecord> temp=new ArrayList();
		BirthdayRecord br;
		cursor.moveToFirst();
		while (!cursor.isAfterLast()){
			br=new BirthdayRecord();
			br.setContent(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3),
					cursor.getString(4), cursor.getString(5),cursor.getString(6), cursor.getString(7), cursor.getString(8), cursor.getString(9),cursor.getInt(10), cursor.getString(11), cursor.getBlob(12),cursor.getInt(13), cursor.getString(14));
			temp.add(br);
			cursor.moveToNext();
		}

		return temp;
	}
	
	public BirthdayRecord fetchRecordById(long id) {
		BirthdayRecord br=new BirthdayRecord();
		String[] columns=new String[]{KEY_ID,KEY_NAME,KEY_FAMILY,KEY_BIRTHDAY_DATE,KEY_BIRTHDAY_DAY,KEY_BIRTHDAY_DATE_G,KEY_BIRTHDAY_DAY_G,KEY_MOBILE,KEY_PHONE,KEY_EMAIL,KEY_DATETYPE,KEY_SMSBODY,KEY_IMAGE,KEY_SOLARVALUE,KEY_CUSTOM_DATE_EXP};
		cursor=ourDb.query(DB_TABLE, columns, KEY_ID+"="+id, null, null, null, null);		
		cursor.moveToFirst();
		if(!cursor.isAfterLast()){
			br.setContent(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), 
					      cursor.getString(4), cursor.getString(5),cursor.getString(6), cursor.getString(7), cursor.getString(8), cursor.getString(9),cursor.getInt(10), cursor.getString(11), cursor.getBlob(12),cursor.getInt(13), cursor.getString(14));
			return br;
		}
		else return null;
	}
	

	public void startReadOrderByRemainingDays(int current_day_value) {
		// TODO Auto-generated method stub
		
		String sql="select *,(case when _solar_day_value-"+current_day_value+">=0 then _solar_day_value-"+current_day_value+" else _solar_day_value-"+current_day_value+"+365 end) as remain_days from "+DB_TABLE+" order by remain_days";
		cursor=ourDb.rawQuery(sql, null);	
		cursor.moveToFirst();
	}	

	
	public void startReadWithoutImage(int current_day_value) {
		// TODO Auto-generated method stub
		if (sort_order.equals(SORT_ORDER_REMAIN))
		{
			String sql="select _id,_name,_family,_birthday_date,_birthday_day,_birthday_date_georgian,_birthday_day_georgian,_mobile,_phone,_email,_datetype,_smsbody,_solar_day_value,_custom_date_exp,(case when _solar_day_value-"+current_day_value+">=0 then _solar_day_value-"+current_day_value+" else _solar_day_value-"+current_day_value+"+365 end) as remain_days from "+DB_TABLE+" order by remain_days";
			cursor=ourDb.rawQuery(sql, null);
		}
		else
		{
			String[] columns=new String[]{KEY_ID,KEY_NAME,KEY_FAMILY,KEY_BIRTHDAY_DATE,KEY_BIRTHDAY_DAY,KEY_BIRTHDAY_DATE_G,KEY_BIRTHDAY_DAY_G,KEY_MOBILE,KEY_PHONE,KEY_EMAIL,KEY_DATETYPE,KEY_SMSBODY,KEY_SOLARVALUE,KEY_CUSTOM_DATE_EXP};
			cursor=ourDb.query(DB_TABLE, columns, null, null, null, null, sort_order);				
		}
	
		cursor.moveToFirst();
	}	
	
	public BirthdayRecord readNextWithoutImage() {
		// TODO Auto-generated method stub
		BirthdayRecord br=new BirthdayRecord();
		if(!cursor.isAfterLast()){
			br.setContent(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), 
					      cursor.getString(4), cursor.getString(5),cursor.getString(6), cursor.getString(7), cursor.getString(8), cursor.getString(9),cursor.getInt(10), cursor.getString(11), null,cursor.getInt(12), cursor.getString(13));
			cursor.moveToNext();
		}
		return br;

	}
	
	public Cursor getCursor(){
		return cursor;
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public long AddNoteRecoord(NoteObject no) {
		ContentValues cv = new ContentValues();
		cv.put(NOTES_KEY_TITLE, no.title);
		cv.put(NOTES_KEY_BODY, no.body);
		cv.put(NOTES_KEY_DATECREATED, Utils.getCurrentDate());
		cv.put(NOTES_KEY_DATE, no.date);
		cv.put(NOTES_KEY_TIME, no.date);
		cv.put(NOTES_KEY_PASS, "");
		return ourDb.insert(NOTES_DB_TABLE, null, cv);
	}

	public int getNoteMaxId()
	{

		Cursor tmpCur=ourDb.rawQuery("SELECT MAX(_id) AS max_id FROM Notes", null);
		int result=0;

		for(tmpCur.moveToFirst();!tmpCur.isAfterLast();tmpCur.moveToNext()){
			result= tmpCur.getInt(0);
		}

		return result;
	}




	public ArrayList<NoteObject> fetchAllNotesRecords() {
		// TODO Auto-generated method stub
		String[] columns=new String[]{NOTES_KEY_ROW,NOTES_KEY_TITLE,NOTES_KEY_BODY,NOTES_KEY_PASS,NOTES_KEY_DATE,NOTES_KEY_TIME,NOTES_KEY_DATECREATED};
		cursor=ourDb.query(NOTES_DB_TABLE, columns, null, null, null, null, NOTES_KEY_ROW+" DESC");

		ArrayList<NoteObject> temp=new ArrayList();
		NoteObject no;
		cursor.moveToFirst();
		while (!cursor.isAfterLast()){
			no=new NoteObject();
			no.setContent(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3),
					cursor.getString(4), cursor.getString(5),cursor.getString(6));
			temp.add(no);
			cursor.moveToNext();
		}

		return temp;
	}

	public NoteObject fetchNoteRecord(int R_ID) {
		// TODO Auto-generated method stub
		String[] columns=new String[]{NOTES_KEY_ROW,NOTES_KEY_TITLE,NOTES_KEY_BODY,NOTES_KEY_PASS,NOTES_KEY_DATE,NOTES_KEY_TIME,NOTES_KEY_DATECREATED};
		cursor=ourDb.query(NOTES_DB_TABLE, columns, NOTES_KEY_ROW+"="+R_ID, null, null, null, null);
		cursor.moveToFirst();
		NoteObject no=new NoteObject();
		if(!cursor.isBeforeFirst()){
			no.setContent(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3),
					cursor.getString(4), cursor.getString(5),cursor.getString(6));
			//cursor.moveToNext();
		}
		return no;


	}

	public boolean DeleteNoteRecord(int R_ID) {
		// TODO Auto-generated method stub
		ourDb.delete(NOTES_DB_TABLE, NOTES_KEY_ROW+"="+R_ID, null);
		return true;

	}

	public void UpdateNoteRecord(NoteObject no) throws SQLException{
		// TODO Auto-generated method stub
		ContentValues cv = new ContentValues();
		cv.put(NOTES_KEY_TITLE, no.title);
		cv.put(NOTES_KEY_BODY, no.body);
		cv.put(NOTES_KEY_DATE, no.date);
		cv.put(NOTES_KEY_TIME, no.date);
		cv.put(NOTES_KEY_PASS, no.pass);
		Log.e("editing",no.note_id+"-"+no.title);
		ourDb.update(NOTES_DB_TABLE, cv, NOTES_KEY_ROW + "=" + no.note_id, null);
	}

	public void UpdateNoteTitle(int R_ID, String nTitle) throws SQLException{
		// TODO Auto-generated method stub
		ContentValues cvUpdate = new ContentValues();
		cvUpdate.put(NOTES_KEY_TITLE, nTitle);
		//cvUpdate.put(KEY_TITLE, getTitle(nBody));
		ourDb.update(NOTES_DB_TABLE, cvUpdate, NOTES_KEY_ROW + "=" + R_ID, null);
	}

	public void SetNotePass(int R_ID, String nPass) throws SQLException{
		// TODO Auto-generated method stub
		ContentValues cvUpdate = new ContentValues();
		cvUpdate.put(NOTES_KEY_PASS, nPass);
		//cvUpdate.put(KEY_TITLE, getTitle(nBody));
		ourDb.update(NOTES_DB_TABLE, cvUpdate, NOTES_KEY_ROW + "=" + R_ID, null);
	}

	public static class SearchNode{
		public final static int TYPE_DATE=1;
		public final static int TYPE_NOTE=2;
		public int id=-1;
		public int type=-1;
		public String title="";
		public String content="";
		public String date="";
		public String mobile="";
		public String sms="";
		public byte[] image=null;

		@Override
		public String toString() {
			return title;
		}
	}

	public ArrayList<SearchNode> searchByString(String search){
		String raw="select * from (select _id, "+SearchNode.TYPE_DATE+" as ttype,_custom_date_exp as content,_name || ' ' || _family as title,_birthday_date as _date,_image from _dates "+
				"union all select _id, "+SearchNode.TYPE_NOTE+" as ttype,N_BODY as content,N_TITLE as title,N_DateCreated as _date, null as _image from Notes) as Table1 "+
				"where (content like '%"+search+"%') or (title like '%"+search+"%')";
		cursor=ourDb.rawQuery(raw, null);
		ArrayList<SearchNode> temp=new ArrayList();
		SearchNode sn;
		cursor.moveToFirst();
		while (!cursor.isAfterLast()){
			sn=new SearchNode();
			sn.id=cursor.getInt(0);
			sn.type=cursor.getInt(1);
			sn.content=cursor.getString(2);
			sn.title=cursor.getString(3);
			sn.date=cursor.getString(4);
			sn.image=cursor.getBlob(5);
			temp.add(sn);
			cursor.moveToNext();
		}

		return temp;

	}


}
