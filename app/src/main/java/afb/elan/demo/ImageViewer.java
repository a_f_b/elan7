package afb.elan.demo;

import android.app.Activity;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.ImageView;

public class ImageViewer extends Activity {
	
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.image_viewer);
		ImageView im=(ImageView)findViewById(R.id.imageView1);
		byte[] b=getIntent().getExtras().getByteArray("image");
		im.setImageBitmap(BitmapFactory.decodeByteArray( b, 
		          0,b.length));
	}

}
