package afb.elan.demo.calendar;

import java.util.Calendar;


import android.content.Context;

import afb.elan.demo.R;

public class SolarDate {

	int mYear, mMonth, mDay,
			GregorianToSolar_Month = 0, GregorianToSolar_Day = 0;
	private int dateKind=0;
	//String Georgiandate, Solardate;
	short LeapMonth[] = { 2, 12 };
	// {esfand} , {February}
	short DaysOfMonths[][] = {
			{ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 },
			{ 31, 31, 31, 31, 31, 31, 30, 30, 30, 30, 30, 29 } };
	// { Far, Ord, Kho, Tir, Mor, Sha, Meh, Aba, Aza, Day, Bah,^Esf }, {
	// Jan,^Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec });
	int DaysToMonth[][] = {
			{ 0, 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365 },
			{ 0, 0, 31, 62, 93, 124, 155, 186, 216, 246, 276, 306, 336, 365 } };

	public final static int DATE_KIND_GEORGIAN =0;
	public final static int DATE_KIND_SOLAR =1;

	// { Far, Ord, Kho, Tir, Mor, Sha, Meh, Aba, Aza, Day, Bah,^Esf, *** },{
	// Jan,^Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec, *** });


	public static SolarDate newInstance(int _dateKind){
		Calendar ca = Calendar.getInstance();
		SolarDate sd=new SolarDate();
		sd.setdate(DATE_KIND_GEORGIAN,ca.get(Calendar.YEAR), ca.get(Calendar.MONTH) + 1,
				ca.get(Calendar.DAY_OF_MONTH));
		sd.changeDateKind(_dateKind);
		return sd;


	}
	public boolean setdate(int _dateKind,int y, int m, int d) {
		if (IsDateValid(_dateKind,y,m,d)){
			dateKind=_dateKind;
			mYear = y;
			mMonth = m;
			mDay = d;
			return true;

		}
		else
			return  false;

	}

	public boolean changeDateKind(int _dateKind){
		if (dateKind!=_dateKind){
			switch (_dateKind) {
				case DATE_KIND_GEORGIAN:
					SolarToGregorian(mYear, mMonth, mDay);
					break;
				case DATE_KIND_SOLAR:
					GregorianToSolar(mYear, mMonth, mDay);
					break;

			}
			return true;
		}
		return false;
	}
	
	public int getDay(){
		return mDay;
	}
	
	public int getMonth(){
		return mMonth;
	}
	
	public int getYear(){
		return mYear;
	}

	private int DaysOfMonth(int DateKind, int Year, int Month) {
		int i;
		if ((Year != 0) && (Month >= 1) && (Month <= 12)) {
			i = DaysOfMonths[DateKind][Month - 1];
			if ((Month == LeapMonth[DateKind]) && (IsLeapYear(DateKind, Year)))
				i++;
		} else
			i = 0;
		return (i);
	}

	public boolean IsDateValid(int DateKind, int Year, int Month, int Day) {
		boolean b;
		b = ((Year != 0) && (Month >= 1) && (Month <= 12) && (Day >= 1) && (Day <= DaysOfMonth(
				DateKind, Year, Month)));
		return (b);
	}

	/*public String toSolar() {
		String s;
		GregorianToSolar(mYear, mMonth, mDay);
		s = Integer.valueOf(mYear).toString() + "/"
				+ getMMDD();
		return (s);
	}*/
	
	public int getDayValue(){
		return DaysToMonth[dateKind][mMonth]+mDay;

	}
	
	public void appendDay(short day) {
		mDay=mDay+day;
		while (mDay>DaysOfMonths[dateKind][mMonth-1]){
			mDay=mDay-DaysOfMonths[dateKind][mMonth-1];
			mMonth=mMonth+1;
			if (mMonth>12){
				mMonth=1;
				mYear=mYear+1;
			}
		}
	}	
	
	public String getMMDD() {
		String s;
		String d=Integer.valueOf(mDay).toString();
		String m=Integer.valueOf(mMonth).toString();
		if (mDay<10) d="0"+d;
		if (mMonth<10) m="0"+m;
		s = m + "/" + d;
		return (s);
	}


	

	/*public String toGeorgian() {
		String s;
		SolarToGregorian(mYear, mMonth, mDay);
		s = Integer.valueOf(mYear).toString() + "/"
				+ Integer.valueOf(mMonth).toString() + "/"
				+ Integer.valueOf(mDay).toString();
		return (s);
	}*/

	public boolean IsLeapYear(int datekind, long year) {
		boolean b;
		if (datekind == DATE_KIND_SOLAR)
			b = ((((year + 38) * 31) % 128) <= 30);
		else
			b = ((year % 4) == 0)
					&& (((year % 100) != 0) || ((year % 400) == 0));
		return (b);

	}

	public int DaysToDate(int datekind, int Year, int Month, int Day) {
		// if IsDateValid(DateKind, Year, Month, Day) then
		int l = 0;
		l = DaysToMonth[datekind][Month] + Day;
		if ((Month > LeapMonth[datekind]) && IsLeapYear(datekind, Year))
			l++;
		return l;
	}

	public boolean GregorianToSolar(int Year, int Month, int Day) {
		int LeapDay, Days;
		boolean PrevGregorianLeap, b;
		// if IsDateValid(dkGregorian, Year, Month, Day) then
		b = false;

		PrevGregorianLeap = IsLeapYear(DATE_KIND_GEORGIAN, Year - 1);
		Days = DaysToDate(DATE_KIND_GEORGIAN, Year, Month, Day);
		Year = Year - 622;
		if (IsLeapYear(DATE_KIND_SOLAR, Year))
			LeapDay = 1;
		else
			LeapDay = 0;
		if ((PrevGregorianLeap) && (LeapDay == 1))
			Days = Days + 287;
		else
			Days = Days + 286;
		if (Days > (365 + LeapDay)) {
			Year++;
			Days = Days - (365 + LeapDay);
		}
		;
		GregorianToSolar_Month = Month;
		GregorianToSolar_Day = Day;
		b = DateOfDay(DATE_KIND_SOLAR, Days, Year, Month, Day);
		mYear = Year;
		mMonth = GregorianToSolar_Month;
		mDay = GregorianToSolar_Day;
		return (b);
	}

	public boolean SolarToGregorian(int Year, int Month, int Day) {

		int LeapDay, Days;
		boolean PrevSolarLeap;
		boolean Result;

		if (IsDateValid(DATE_KIND_SOLAR, Year, Month, Day)) {
			PrevSolarLeap = IsLeapYear(DATE_KIND_SOLAR, Year - 1);
			Days = DaysToDate(DATE_KIND_SOLAR, Year, Month, Day);
			Year = Year + 621;
			if (IsLeapYear(DATE_KIND_GEORGIAN, Year))
				LeapDay = 1;
			else
				LeapDay = 0;
			if ((PrevSolarLeap) && (LeapDay == 1))
				Days = Days + 80;
			else
				Days = Days + 79;
			if (Days > (365 + LeapDay)) {
				Year++;
				Days = Days - (365 + LeapDay);
			}
			;
			GregorianToSolar_Month = Month;
			GregorianToSolar_Day = Day;
			Result = DateOfDay(DATE_KIND_GEORGIAN, Days, Year, Month, Day);
			mYear = Year;
			mMonth = GregorianToSolar_Month;
			mDay = GregorianToSolar_Day;
		}

		else
			Result = false;
		return (Result);
	}

	private boolean DateOfDay(int DateKind, int Days, int Year, int Month,
			int Day) {

		int LeapDay, m;

		LeapDay = 0;
		Month = 0;
		Day = 0;
		for (m = 2; m <= 13; m++) {
			if ((m > LeapMonth[DateKind]) && IsLeapYear(DateKind, Year))
				LeapDay = 1;
			if (Days <= (DaysToMonth[DateKind][m] + LeapDay)) {
				Month = m - 1;
				if (Month <= LeapMonth[DateKind])
					LeapDay = 0;
				Day = Days - (DaysToMonth[DateKind][Month] + LeapDay);
				break;
			}
		}
		GregorianToSolar_Month = Month;
		GregorianToSolar_Day = Day;

		
		return true;
	}

	public void setSolarDateFromString(String date){
		String temp[]=date.split("/");
		mYear=Integer.valueOf(temp[0]);
		mMonth=Integer.valueOf(temp[1]);
		mDay=Integer.valueOf(temp[2]);
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		//return super.toString();
		return mYear+"/"+getMMDD();
	}
	
	public String getLongSolarDate(Context context){

		SolarDate s=new SolarDate();
		s.setdate(dateKind, mYear, mMonth, mDay);
		s.changeDateKind(DATE_KIND_GEORGIAN);
		Calendar c = Calendar.getInstance();
		c.set(Calendar.YEAR, s.getYear());
		c.set(Calendar.MONTH, s.getMonth()-1);
		c.set(Calendar.DAY_OF_MONTH, s.getDay());
		return mYear+" "+getMonthName(context)+" "+mDay+" "+ getDayName(c,context);
		//return s.toString();
	}
	
	private String getDayName(Calendar c,Context context) {
		//c = Calendar.getInstance();
		String days[]=context.getResources().getStringArray(R.array.solarDayNames);
		//if (l.contentEquals("2")) 
			switch (c.get(Calendar.DAY_OF_WEEK)) {
			case Calendar.SUNDAY:
				return days[1];
			case Calendar.MONDAY:
				return days[2];
			case Calendar.TUESDAY:
				return days[3];
			case Calendar.WEDNESDAY:
				return days[4];
			case Calendar.THURSDAY:
				return days[5];
			case Calendar.FRIDAY:
				return days[6];
			case Calendar.SATURDAY:
				return days[0];

		}
		return null;

	}
	
	private String getMonthName(Context context) {
		//c = Calendar.getInstance();
		String months[]=context.getResources().getStringArray(R.array.solarMonthNames);

		return months[mMonth-1];
	}	

}
