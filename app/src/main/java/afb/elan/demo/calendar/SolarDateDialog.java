package afb.elan.demo.calendar;

import android.content.Context;
import androidx.appcompat.app.AppCompatDialog;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;

import java.util.Calendar;

import afb.elan.demo.R;

public class SolarDateDialog extends AppCompatDialog implements View.OnClickListener{
	
	
	private onSubmitListener	mListener=null;
	public interface onSubmitListener {
		public void onSubmit(String date);
	}

	PersianDatePicker pdp;
	
	public SolarDateDialog(Context context) {
		super(context,R.style.AppTheme_DatePickerDialog);
		// TODO Auto-generated constructor stub
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.persian_calendar);
		setCancelable(true);
		setCanceledOnTouchOutside(true);
		pdp=(PersianDatePicker) findViewById(R.id.datePicker);
		PersianCalendar pc=new PersianCalendar();
		Calendar newDate = Calendar.getInstance();
		pc.setGregorianChange(newDate.getTime());
		pdp.setDisplayDate(newDate.getTime());
		pdp.yearNumberPicker.setMinValue(1300);
		pdp.yearNumberPicker.setMaxValue(1410);
		((Button) findViewById(R.id.bOk)).setOnClickListener(this);
		((Button) findViewById(R.id.bDismiss)).setOnClickListener(this);
		((ImageButton) findViewById(R.id.bCancel)).setOnClickListener(this);
		PersianCalendar pCal = pdp.getDisplayPersianDate();
		pCal.getPersianShortDate();
		
		
	}
	
	public void setPersianDate(String s){
		if (s.length()!=10) return;
		pdp.yearNumberPicker.setMinValue(1300);
		pdp.yearNumberPicker.setMaxValue(1410);
		int year=Integer.valueOf(s.substring(0, 4));
		int month=Integer.valueOf(s.substring(5, 7));
		int day=Integer.valueOf(s.substring(8, 10));
		pdp.yearNumberPicker.setValue(year);
		pdp.monthNumberPicker.setValue(month);
		pdp.dayNumberPicker.setValue(day);

		
		
	}
	
	public void setOnSublitListener(onSubmitListener l){
		mListener=l;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.bOk:
			PersianCalendar pCal = pdp.getDisplayPersianDate();
			if (mListener!=null) mListener.onSubmit(pCal.getPersianShortDate());
			dismiss();

			break;

		case R.id.bDismiss:
		case R.id.bCancel:
			dismiss();
			break;
		}
	}
	
	

}
