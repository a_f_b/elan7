package afb.elan.demo;

/**
 * Created by aTa on 4/18/2018.
 */
import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;



//import ir.tapsell.sdk.Tapsell;
//import ir.tapsell.sdk.TapsellConfiguration;

/**
 * Created by vamsi on 06-05-2017 for android custom font article
 */

public class ElanApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        //TapsellConfiguration tconfig = new TapsellConfiguration(this);
        //tconfig.setPermissionHandlerMode(TapsellConfiguration.PERMISSION_HANDLER_DISABLED);
        //Tapsell.initialize(this, tconfig, "fohigorldpqrprqqfaqeosobasaipapfgrdgigdhnltcajihprtmsbkgrgapfeefsjgnte");

        FontsOverride.overrideFont(this, "DEFAULT", "fonts/yekan.ttf");
        FontsOverride.overrideFont(this, "MONOSPACE", "fonts/iransanse.ttf");
        FontsOverride.overrideFont(this, "SERIF", "fonts/nazanin.ttf");
        createNotificationChannel();
        //FontsOverride.overrideFont(this, "SANS", "fonts/iransanse.ttf");
    }



    private void createNotificationChannel() {
        // If the Android Version is greater than Oreo,
        // then create the NotificationChannel
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String name = getString(R.string.app_name);
            String descriptionText = getString(R.string.family);

            NotificationChannel channel = new NotificationChannel(
                    "afb.elan.demo",
                    name,
                    NotificationManager.IMPORTANCE_DEFAULT
            );

            // Register the channel
            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(channel);
        }
    }

}