package afb.elan.demo;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import afb.elan.demo.Db.SQLHelper;
import afb.elan.demo.classes.DownloadFileFromURL;
import afb.elan.demo.classes.ServiceHandler;
import afb.elan.demo.classes.TaskRunner;
import afb.elan.demo.classes.URLs;
import afb.elan.demo.classes.Utils;

public class Backup extends AppCompatActivity implements View.OnClickListener {

    private static int WRITE_PERMISSION=1010;

    ArrayList<String> backups;
    ArrayList<View> backupsview;
    String restoringfilename="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.backup);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // For navigation bar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_TITLE | ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_HOME_AS_UP);
        }

        ((Button)findViewById(R.id.backup)).setOnClickListener(this);
        getBackUps();
    }


    private boolean checkWritePremission(){
        boolean result= ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)==PackageManager.PERMISSION_GRANTED;
        return  result;
    }




    public void refreshList(){
        ((LinearLayout)findViewById(R.id.layout)).removeAllViews();
        getBackUps();

    }
    public void getBackUps() {
        backups = new ArrayList<String>();
        backupsview= new ArrayList<>();
        SharedPreferences someData = getSharedPreferences("data", 0);
        String email=someData.getString("add","");
        String sessionkey=someData.getString("sessionkey","");
        email = new String(Base64.decode(email,Base64.URL_SAFE));

        if (email.isEmpty() || !Utils.isValidEmail(email)) {
            finish();
        }


        String android_id = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        nameValuePairs.add(new BasicNameValuePair("elan", android_id));
        nameValuePairs.add(new BasicNameValuePair("email", email));
        nameValuePairs.add(new BasicNameValuePair("session_key", sessionkey));
        TaskRunner tr = new TaskRunner(URLs.getBackups, ServiceHandler.POST, nameValuePairs, new TaskRunner.onTaskCompleteListener() {
            @Override
            public void onTaskComplete(String result) {
                // TODO Auto-generated method stub
                Log.e("backup",result);
                try {
                    JSONArray jArray = new JSONArray(result);
                    for (int i = 0; i < jArray.length(); i++) {
                        String j_data = jArray.getString(i);
                        backups.add(j_data);
                        //Log.e("j_data.toString()",j_data.toString());

                    }



                        //for (int j=1; j<50;j++)
                        for (int i = 0; i < backups.size(); i++) {
                            //JSONObject ab = abilities.getJSONObject(i);
                            View contactView = LayoutInflater.from(Backup.this).inflate(R.layout.studio_backup_row, null);

                            contactView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                            contactView.setId(i);
                            //switches[i].setBackground(getResources().getDrawable(R.drawable.ripple_effect));
                            ((TextView) contactView.findViewById(R.id.tvName)).setText(backups.get(i));
                            //switches[i].setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
                            //contacts[i].setTag(supports.get(i).id);
                            contactView.findViewById(R.id.ivDelete).setTag(backups.get(i));
                            contactView.findViewById(R.id.ivRestore).setTag(backups.get(i));

                            contactView.findViewById(R.id.ivDelete).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    String filename= (String)view.getTag();
                                    doDelete(filename);
                                }
                            });
                            contactView.findViewById(R.id.ivRestore).setOnClickListener(new View.OnClickListener() {
                                @RequiresApi(api = Build.VERSION_CODES.M)
                                @Override
                                public void onClick(View view) {
                                    restoringfilename= (String)view.getTag();
                                    doRestore(restoringfilename);
                                }
                            });
                            //((ImageView)contactView.findViewById(R.id.imageView)).setImageResource(R.drawable.ic_action_support);
                            //((ImageView)contactView.findViewById(R.id.delete)).setVisibility(View.GONE);

                            backupsview.add(contactView);
                            ((LinearLayout)findViewById(R.id.layout)).addView(contactView,i);

                        }
                    ((LinearLayout)findViewById(R.id.layout)).removeViewAt(backups.size());




                    //finish();
                    //etMobile.setText(result+"--"+tok+"");
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    //dg.dismiss();
                }
            }
        });
        tr.execute();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;


        }
        return super.onOptionsItemSelected(item);
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    private void doRestore(String filename){
        if (!checkWritePremission()){
            String[] permission = {Manifest.permission.WRITE_EXTERNAL_STORAGE};

            requestPermissions(permission, WRITE_PERMISSION);

        }
        else{
            DownloadFileFromURL dffu=new DownloadFileFromURL(this,filename,false);
            dffu.setonTaskCompleteListener(new DownloadFileFromURL.onTaskCompleteListener() {
                @Override
                public void onTaskComplete(String result) {
                    Intent in=new Intent(MainActivity.ELAN_FINISH);
                    sendBroadcast(in);
                    Toast.makeText(Backup.this, "بازیابی با موفقیت انجام شد. لطقا برنامه را مجددا راه اندازی نمایید", Toast.LENGTH_LONG).show();
                    finish();
                }
            });
            dffu.execute();
        }
    }

    public void doDelete(String filename){
        AlertDialog.Builder builder = (android.os.Build.VERSION.SDK_INT < 11) ? new AlertDialog.Builder(
                this) : new AlertDialog.Builder(this,
                AlertDialog.THEME_HOLO_LIGHT);

        AlertDialog ad = builder.create();
        ad.setTitle("");

        ad.setMessage("آیا مایل به حذف این فایل پشتیبان می باشید؟");
        ad.setButton(DialogInterface.BUTTON_POSITIVE,"بلی",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        final Dialog dg = Utils.prepareLoadingDialog(Backup.this, true);
                        dg.show();
                        SharedPreferences someData = getSharedPreferences("data", 0);
                        String email=someData.getString("add","");
                        email = new String(Base64.decode(email,Base64.URL_SAFE));

                        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                        nameValuePairs.add(new BasicNameValuePair("filename", filename));
                        nameValuePairs.add(new BasicNameValuePair("email", email));
                        nameValuePairs.add(new BasicNameValuePair("elan", "v2.0"));
                        TaskRunner tr = new TaskRunner(URLs.delete_backup, ServiceHandler.POST, nameValuePairs, new TaskRunner.onTaskCompleteListener() {
                            @Override
                            public void onTaskComplete(String result) {
                                Log.e("deleteresult",result);
                                refreshList();
                                dg.dismiss();

                            }
                        });
                        tr.execute();

                    }
                });
        ad.setButton(DialogInterface.BUTTON_NEGATIVE,"صرف نظر",new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        }

                );
        ad.show();
    }

    public void doBackup(){
        final Dialog dg = Utils.prepareLoadingDialog(this, true);
        dg.show();

        PackageManager m = getPackageManager();
        String s = getPackageName();
        PackageInfo p;
        try {
            p = m.getPackageInfo(s, 0);
            s = p.applicationInfo.dataDir;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        // Toast.makeText(Offline_map_project.this, s, 1000).show();
        File file = new File(s + "/databases" , SQLHelper.DB_NAME);

        int size = (int) file.length();
        byte[] byte_arr = new byte[size];
        String data="";
        try {
            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
            buf.read(byte_arr, 0, byte_arr.length);
            buf.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        if (byte_arr != null) {
            data = Base64.encodeToString(byte_arr, Base64.DEFAULT);
        }

        SharedPreferences someData = getSharedPreferences("data", 0);
        String email=someData.getString("add","");
        email = new String(Base64.decode(email,Base64.URL_SAFE));

        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        nameValuePairs.add(new BasicNameValuePair("data", data));
        nameValuePairs.add(new BasicNameValuePair("email", email));
        nameValuePairs.add(new BasicNameValuePair("elan", "v2.0"));


        TaskRunner tr = new TaskRunner(URLs.upload, ServiceHandler.POST, nameValuePairs, new TaskRunner.onTaskCompleteListener() {
            @Override
            public void onTaskComplete(String result) {
                refreshList();
                dg.dismiss();

            }
        });
        tr.execute();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == WRITE_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Permission Granted...", Toast.LENGTH_SHORT).show();
                DownloadFileFromURL dffu=new DownloadFileFromURL(this,restoringfilename,false);
                dffu.setonTaskCompleteListener(new DownloadFileFromURL.onTaskCompleteListener() {
                    @Override
                    public void onTaskComplete(String result) {
                        Intent in=new Intent(MainActivity.ELAN_FINISH);
                        sendBroadcast(in);
                        Toast.makeText(Backup.this, "بازیابی با موفقیت انجام شد. لطقا برنامه را مجددا راه اندازی نمایید", Toast.LENGTH_LONG).show();
                        finish();

                    }
                });
                dffu.execute();

            } else {
                //permission denied
                Toast.makeText(this, "Permission denied...", Toast.LENGTH_SHORT).show();
            }
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.backup:
                doBackup();
                break;
        }
    }
}
