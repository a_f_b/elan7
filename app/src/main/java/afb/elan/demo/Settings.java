package afb.elan.demo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.RadioGroup;
import android.widget.TimePicker;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.appcompat.widget.SwitchCompat;
import androidx.appcompat.widget.Toolbar;

import afb.elan.demo.Services.DemoService;

public class Settings extends AppCompatActivity implements TimePicker.OnTimeChangedListener, CompoundButton.OnCheckedChangeListener {
    TimePicker picker;
    boolean timechanged=false;
    SwitchCompat sound,alert,sms,delivery;
    AppCompatRadioButton notification,showlist;
    SharedPreferences shp;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.preferences);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // For navigation bar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_TITLE | ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_HOME_AS_UP);
        }


        shp = PreferenceManager
                .getDefaultSharedPreferences(this);
        sound=(SwitchCompat)findViewById(R.id.sound);
        alert=(SwitchCompat)findViewById(R.id.alert);
        sms=(SwitchCompat)findViewById(R.id.sms);
        delivery=(SwitchCompat)findViewById(R.id.delivery);
        notification=(AppCompatRadioButton)findViewById(R.id.notification);
        showlist=(AppCompatRadioButton)findViewById(R.id.showlist);
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            showlist.setClickable(false);
            showlist.setEnabled(false);
            showlist.setAlpha(0.5f);
            //Visibility(View.INVISIBLE);
        }*/
        sound.setChecked(shp.getBoolean("prefSoundEnabled",true));
        alert.setChecked(shp.getBoolean("prefShowTodayListEnabled",true));
        sms.setChecked(shp.getBoolean("prefSmsSendEnabled",true));
        delivery.setChecked(shp.getBoolean("prefSmsDeliveryEnabled",true));
        showlist.setChecked(shp.getString("prefShowTodayListType","1").equals("1"));
        notification.setChecked(!showlist.isChecked());
        sound.setChecked(shp.getBoolean("prefSoundEnabled",true));
        RadioGroup rg=(RadioGroup)findViewById(R.id.groupAlert);
        showlist.setEnabled(alert.isChecked());
        notification.setEnabled(alert.isChecked());
        rg.setAlpha(alert.isChecked()?1:0.5f);
        delivery.setEnabled(sms.isChecked());
        delivery.setAlpha(sms.isChecked()?1:0.5f);


        sound.setOnCheckedChangeListener(this);
        alert.setOnCheckedChangeListener(this);
        sms.setOnCheckedChangeListener(this);
        delivery.setOnCheckedChangeListener(this);
        notification.setOnCheckedChangeListener(this);
        showlist.setOnCheckedChangeListener(this);

        picker=(TimePicker)findViewById(R.id.timePicker1);
        picker.setIs24HourView(true);
        SharedPreferences someData = PreferenceManager
                .getDefaultSharedPreferences(this);
        int hour=someData.getInt("defaultTimeHour",12);
        int minute=someData.getInt("defaultTimeMinute",00);
        if (Build.VERSION.SDK_INT >= 23 ){
            picker.setHour(hour);
            picker.setMinute(minute);
        }
        else{
            picker.setCurrentHour(hour);
            picker.setCurrentMinute(minute);
        }
        picker.setOnTimeChangedListener(this);
    }

    @Override
    public void onTimeChanged(TimePicker timePicker, int i, int i1) {
        int hour, minute;
        if (Build.VERSION.SDK_INT >= 23 ){
            hour = picker.getHour();
            minute = picker.getMinute();
        }
        else{
            hour = picker.getCurrentHour();
            minute = picker.getCurrentMinute();
        }
        SharedPreferences someData = PreferenceManager
                .getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = someData.edit();
        editor.putInt("defaultTimeHour",hour);
        editor.putInt("defaultTimeMinute",minute);
        editor.commit();
        timechanged=true;


    }



    @Override
    public void finish() {
        //if (guest)
        if (timechanged){
            Intent intent=new Intent(DemoService.AFB_ELAN_STUDIO_DEMORECEIVER);
            intent.putExtra("needCheck",true);
            sendBroadcast(intent);

        }
        super.finish();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;


        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        SharedPreferences.Editor editor = shp.edit();


        switch (compoundButton.getId()){
            case R.id.sound:
                editor.putBoolean("prefSoundEnabled", compoundButton.isChecked());
                Log.e("setting","set");
                timechanged=true;
                break;
            case R.id.alert:
                editor.putBoolean("prefShowTodayListEnabled", compoundButton.isChecked());
                RadioGroup rg=(RadioGroup)findViewById(R.id.groupAlert);
                showlist.setEnabled(compoundButton.isChecked());
                notification.setEnabled(compoundButton.isChecked());
                rg.setAlpha(compoundButton.isChecked()?1:0.5f);
                timechanged=true;
                break;
            case R.id.sms:
                editor.putBoolean("prefSmsSendEnabled", compoundButton.isChecked());
                delivery.setEnabled(compoundButton.isChecked());
                delivery.setAlpha(compoundButton.isChecked()?1:0.5f);
                timechanged=true;
                break;
            case R.id.delivery:
                editor.putBoolean("prefSmsDeliveryEnabled", compoundButton.isChecked());
                timechanged=true;
                break;
            case R.id.notification:
                if (compoundButton.isChecked()) editor.putString("prefShowTodayListType", "2");
                timechanged=true;
                break;
            case R.id.showlist:
                if (compoundButton.isChecked()) editor.putString("prefShowTodayListType", "1");
                timechanged=true;
                break;
        }
        editor.commit();

    }
}
