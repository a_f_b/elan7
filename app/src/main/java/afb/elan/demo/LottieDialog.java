package afb.elan.demo;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;
import com.airbnb.lottie.LottieAnimationView;
import com.labters.lottiealertdialoglibrary.DialogTypes;
import com.labters.lottiealertdialoglibrary.R.anim;
import com.labters.lottiealertdialoglibrary.R.id;
import com.labters.lottiealertdialoglibrary.R.layout;

import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(
        mv = {1, 6, 0},
        k = 1,
        xi = 2,
        d1 = {"\u0000\\\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001:\u00017B¥\u0001\b\u0012\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\b\u0010\b\u001a\u0004\u0018\u00010\u0007\u0012\b\u0010\t\u001a\u0004\u0018\u00010\u0007\u0012\b\u0010\n\u001a\u0004\u0018\u00010\u0007\u0012\b\u0010\u000b\u001a\u0004\u0018\u00010\u0007\u0012\b\u0010\f\u001a\u0004\u0018\u00010\r\u0012\b\u0010\u000e\u001a\u0004\u0018\u00010\r\u0012\b\u0010\u000f\u001a\u0004\u0018\u00010\r\u0012\b\u0010\u0010\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u0011\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u0012\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u0013\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u0014\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u0015\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0016J\u000e\u0010.\u001a\u00020/2\u0006\u00100\u001a\u000201J\b\u00102\u001a\u00020/H\u0002J\u0012\u00103\u001a\u00020/2\b\u00104\u001a\u0004\u0018\u000105H\u0014J\b\u00106\u001a\u00020/H\u0002R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082.¢\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u0018X\u0082.¢\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082.¢\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u001bX\u0082.¢\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u001bX\u0082.¢\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u001fX\u0082.¢\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020\u0003X\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010!\u001a\u0004\u0018\u00010\u0007X\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\"\u001a\u0004\u0018\u00010\rX\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010#\u001a\u0004\u0018\u00010\u0007X\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010$\u001a\u0004\u0018\u00010\rX\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010%\u001a\u0004\u0018\u00010\u0007X\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010&\u001a\u0004\u0018\u00010\rX\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010'\u001a\u0004\u0018\u00010\u0007X\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010(\u001a\u0004\u0018\u00010\u0007X\u0082\u000e¢\u0006\u0002\n\u0000R\u0012\u0010)\u001a\u0004\u0018\u00010\u0005X\u0082\u000e¢\u0006\u0004\n\u0002\u0010*R\u0012\u0010\u0012\u001a\u0004\u0018\u00010\u0005X\u0082\u000e¢\u0006\u0004\n\u0002\u0010*R\u0012\u0010\u0013\u001a\u0004\u0018\u00010\u0005X\u0082\u000e¢\u0006\u0004\n\u0002\u0010*R\u0012\u0010\u0014\u001a\u0004\u0018\u00010\u0005X\u0082\u000e¢\u0006\u0004\n\u0002\u0010*R\u0012\u0010\u0015\u001a\u0004\u0018\u00010\u0005X\u0082\u000e¢\u0006\u0004\n\u0002\u0010*R\u0012\u0010\u0010\u001a\u0004\u0018\u00010\u0005X\u0082\u000e¢\u0006\u0004\n\u0002\u0010*R\u0012\u0010\u0011\u001a\u0004\u0018\u00010\u0005X\u0082\u000e¢\u0006\u0004\n\u0002\u0010*R\u000e\u0010+\u001a\u00020,X\u0082.¢\u0006\u0002\n\u0000R\u000e\u0010-\u001a\u00020,X\u0082.¢\u0006\u0002\n\u0000¨\u00068"},
        d2 = {"Lafb/elan/studio/LottieAlertDialog;", "Landroid/app/AlertDialog;", "context", "Landroid/content/Context;", "type", "", "title", "", "description", "positiveText", "negativeText", "noneText", "positiveListener", "Lcom/labters/lottiealertdialoglibrary/ClickListener;", "negativeListener", "noneListener", "positiveBtnColor", "positiveTextColor", "negativeBtnColor", "negativeTextColor", "noneBtnColor", "noneTextColor", "(Landroid/content/Context;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/labters/lottiealertdialoglibrary/ClickListener;Lcom/labters/lottiealertdialoglibrary/ClickListener;Lcom/labters/lottiealertdialoglibrary/ClickListener;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V", "animationFadeIn", "Landroid/view/animation/Animation;", "animationFadeOut", "btnNegative", "Landroid/widget/Button;", "btnNone", "btnPositive", "lAnimation", "Lcom/airbnb/lottie/LottieAnimationView;", "mContext", "mDescription", "mNegativeListener", "mNegativeText", "mNoneListener", "mNoneText", "mPositiveListener", "mPositiveText", "mTitle", "mType", "Ljava/lang/Integer;", "tvDescription", "Landroid/widget/TextView;", "tvTitle", "changeDialog", "", "builder", "Lafb/elan/studio/LottieAlertDialog$Builder;", "findView", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "setView", "Builder", "app_debug"}
)
public final class LottieDialog extends AlertDialog {
    private Context mContext;
    private Integer mType;
    private String mTitle;
    private String mDescription;
    private String mPositiveText;
    private String mNegativeText;
    private String mNoneText;
    private LottieDialog.LotieClickListener mPositiveListener;
    private LottieDialog.LotieClickListener mNegativeListener;
    private LottieDialog.LotieClickListener mNoneListener;
    private LottieAnimationView lAnimation;
    private TextView tvTitle;
    private TextView tvDescription;
    private Button btnPositive;
    private Button btnNegative;
    private Button btnNone;
    private Integer positiveBtnColor;
    private Integer positiveTextColor;
    private Integer negativeBtnColor;
    private Integer negativeTextColor;
    private Integer noneBtnColor;
    private Integer noneTextColor;
    private Animation animationFadeIn;
    private Animation animationFadeOut;

    public interface LotieClickListener {
        public void onClick();
    }


    protected void onCreate(@Nullable Bundle savedInstanceState) {
        this.requestWindowFeature(1);
        super.onCreate(savedInstanceState);
        this.setContentView(layout.view_alert_dialog);
        Window var10000 = this.getWindow();
        Intrinsics.checkNotNull(var10000);
        var10000.setBackgroundDrawable((Drawable)(new ColorDrawable(0)));
        this.findView();
    }

    private final void findView() {
        View var10001 = this.findViewById(id.lAnimation);
        Intrinsics.checkNotNullExpressionValue(var10001, "findViewById(R.id.lAnimation)");
        this.lAnimation = (LottieAnimationView)var10001;
        var10001 = this.findViewById(id.tvTitle);
        Intrinsics.checkNotNullExpressionValue(var10001, "findViewById(R.id.tvTitle)");
        this.tvTitle = (TextView)var10001;
        var10001 = this.findViewById(id.tvDescription);
        Intrinsics.checkNotNullExpressionValue(var10001, "findViewById(R.id.tvDescription)");
        this.tvDescription = (TextView)var10001;
        var10001 = this.findViewById(id.btnPositive);
        Intrinsics.checkNotNullExpressionValue(var10001, "findViewById(R.id.btnPositive)");
        this.btnPositive = (Button)var10001;
        var10001 = this.findViewById(id.btnNegative);
        Intrinsics.checkNotNullExpressionValue(var10001, "findViewById(R.id.btnNegative)");
        this.btnNegative = (Button)var10001;
        var10001 = this.findViewById(id.btnNone);
        Intrinsics.checkNotNullExpressionValue(var10001, "findViewById(R.id.btnNone)");
        this.btnNone = (Button)var10001;
        this.setView();
    }

    private final void setView() {
        LottieAnimationView var10000 = this.lAnimation;
        if (var10000 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("lAnimation");
        }

        Animation var10001 = this.animationFadeIn;
        if (var10001 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("animationFadeIn");
        }

        var10000.startAnimation(var10001);
        TextView var2;
        if (this.mTitle != null) {
            var2 = this.tvTitle;
            if (var2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("tvTitle");
            }

            var2.setText((CharSequence)this.mTitle);
            var2 = this.tvTitle;
            if (var2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("tvTitle");
            }

            var2.setVisibility(View.VISIBLE);
        } else {
            var2 = this.tvTitle;
            if (var2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("tvTitle");
            }

            var2.setVisibility(View.GONE);
        }

        if (this.mDescription != null) {
            var2 = this.tvDescription;
            if (var2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("tvDescription");
            }

            var2.setText((CharSequence)this.mDescription);
            var2 = this.tvDescription;
            if (var2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("tvDescription");
            }

            var2.setVisibility(View.VISIBLE);
        } else {
            var2 = this.tvDescription;
            if (var2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("tvDescription");
            }

            var2.setVisibility(View.GONE);
        }

        Button var4;
        if (this.mPositiveText != null) {
            var4 = this.btnPositive;
            if (var4 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("btnPositive");
            }

            var4.setText((CharSequence)this.mPositiveText);
            var4 = this.btnPositive;
            if (var4 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("btnPositive");
            }

            var4.setVisibility(View.VISIBLE);
            var4 = this.btnPositive;
            if (var4 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("btnPositive");
            }

            var4.setOnClickListener((new View.OnClickListener() {
                public final void onClick(View it) {
                    LottieDialog.LotieClickListener var10000 = mPositiveListener;
                    if (var10000 != null) {
                        var10000.onClick();
                    }

                }
            }));
        } else {
            var4 = this.btnPositive;
            if (var4 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("btnPositive");
            }

            var4.setVisibility(View.GONE);
        }

        if (this.mNegativeText != null) {
            var4 = this.btnNegative;
            if (var4 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("btnNegative");
            }

            var4.setText((CharSequence)this.mNegativeText);
            var4 = this.btnNegative;
            if (var4 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("btnNegative");
            }

            var4.setVisibility(View.VISIBLE);
            var4 = this.btnNegative;
            if (var4 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("btnNegative");
            }

            var4.setOnClickListener((new View.OnClickListener() {
                public final void onClick(View it) {
                    LottieDialog.LotieClickListener var10000 = mNegativeListener;
                    if (var10000 != null) {
                        var10000.onClick();
                    }

                }
            }));
        } else {
            var4 = this.btnNegative;
            if (var4 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("btnNegative");
            }

            var4.setVisibility(View.GONE);
        }

        if (this.mNoneText != null) {
            var4 = this.btnNone;
            if (var4 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("btnNone");
            }

            var4.setText((CharSequence)this.mNoneText);
            var4 = this.btnNone;
            if (var4 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("btnNone");
            }

            var4.setVisibility(View.VISIBLE);
            var4 = this.btnNone;
            if (var4 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("btnNone");
            }

            var4.setOnClickListener((new View.OnClickListener() {
                public final void onClick(View it) {
                    LottieDialog.LotieClickListener var10000 = mNoneListener;
                    if (var10000 != null) {
                        var10000.onClick();
                    }

                }
            }));
        } else {
            var4 = this.btnNone;
            if (var4 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("btnNone");
            }

            var4.setVisibility(View.GONE);
        }

        Integer var3;
        Drawable var5;
        if (this.positiveBtnColor != null) {
            var4 = this.btnPositive;
            if (var4 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("btnPositive");
            }

            var5 = var4.getBackground();
            var3 = this.positiveBtnColor;
            Intrinsics.checkNotNull(var3);
            var5.setColorFilter(var3, Mode.MULTIPLY);
        } else {
            var4 = this.btnPositive;
            if (var4 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("btnPositive");
            }

            var4.getBackground().clearColorFilter();
        }

        if (this.positiveTextColor != null) {
            var4 = this.btnPositive;
            if (var4 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("btnPositive");
            }

            var3 = this.positiveTextColor;
            Intrinsics.checkNotNull(var3);
            var4.setTextColor(var3);
        } else {
            var4 = this.btnPositive;
            if (var4 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("btnPositive");
            }

            var4.setTextColor(Color.parseColor("#000000"));
        }

        if (this.negativeBtnColor != null) {
            var4 = this.btnNegative;
            if (var4 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("btnNegative");
            }

            var5 = var4.getBackground();
            var3 = this.negativeBtnColor;
            Intrinsics.checkNotNull(var3);
            var5.setColorFilter(var3, Mode.MULTIPLY);
        } else {
            var4 = this.btnNegative;
            if (var4 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("btnNegative");
            }

            var4.getBackground().clearColorFilter();
        }

        if (this.negativeTextColor != null) {
            var4 = this.btnNegative;
            if (var4 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("btnNegative");
            }

            var3 = this.negativeTextColor;
            Intrinsics.checkNotNull(var3);
            var4.setTextColor(var3);
        } else {
            var4 = this.btnNegative;
            if (var4 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("btnNegative");
            }

            var4.setTextColor(Color.parseColor("#000000"));
        }

        if (this.noneBtnColor != null) {
            var4 = this.btnNone;
            if (var4 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("btnNone");
            }

            var5 = var4.getBackground();
            var3 = this.noneBtnColor;
            Intrinsics.checkNotNull(var3);
            var5.setColorFilter(var3, Mode.MULTIPLY);
        } else {
            var4 = this.btnNone;
            if (var4 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("btnNone");
            }

            var4.getBackground().clearColorFilter();
        }

        if (this.noneTextColor != null) {
            var4 = this.btnNone;
            if (var4 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("btnNone");
            }

            var3 = this.noneTextColor;
            Intrinsics.checkNotNull(var3);
            var4.setTextColor(var3);
        } else {
            var4 = this.btnNone;
            if (var4 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("btnNone");
            }

            var4.setTextColor(Color.parseColor("#000000"));
        }

        label277: {
            Integer var6 = this.mType;
            int var1 = DialogTypes.TYPE_LOADING;
            if (var6 != null) {
                if (var6 == var1) {
                    var10000 = this.lAnimation;
                    if (var10000 == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("lAnimation");
                    }

                    var10000.setAnimation("loading.json");
                    var10000 = this.lAnimation;
                    if (var10000 == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("lAnimation");
                    }

                    var10000.setRepeatCount(-1);
                    break label277;
                }
            }

            var6 = this.mType;
            var1 = DialogTypes.TYPE_SUCCESS;
            if (var6 != null) {
                if (var6 == var1) {
                    var10000 = this.lAnimation;
                    if (var10000 == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("lAnimation");
                    }

                    var10000.setAnimation("success.json");
                    var10000 = this.lAnimation;
                    if (var10000 == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("lAnimation");
                    }

                    var10000.setRepeatCount(0);
                    break label277;
                }
            }

            var6 = this.mType;
            var1 = DialogTypes.TYPE_ERROR;
            if (var6 != null) {
                if (var6 == var1) {
                    var10000 = this.lAnimation;
                    if (var10000 == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("lAnimation");
                    }

                    var10000.setAnimation("error.json");
                    var10000 = this.lAnimation;
                    if (var10000 == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("lAnimation");
                    }

                    var10000.setRepeatCount(0);
                    break label277;
                }
            }

            var6 = this.mType;
            var1 = DialogTypes.TYPE_WARNING;
            if (var6 != null) {
                if (var6 == var1) {
                    var10000 = this.lAnimation;
                    if (var10000 == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("lAnimation");
                    }

                    var10000.setAnimation("warning.json");
                    var10000 = this.lAnimation;
                    if (var10000 == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("lAnimation");
                    }

                    var10000.setRepeatCount(0);
                    break label277;
                }
            }

            var6 = this.mType;
            var1 = DialogTypes.TYPE_QUESTION;
            if (var6 != null) {
                if (var6 == var1) {
                    var10000 = this.lAnimation;
                    if (var10000 == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("lAnimation");
                    }

                    var10000.setAnimation("question.json");
                    var10000 = this.lAnimation;
                    if (var10000 == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("lAnimation");
                    }

                    var10000.setRepeatCount(-1);
                }
            }
        }

        var10000 = this.lAnimation;
        if (var10000 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("lAnimation");
        }

        var10000.playAnimation();
    }

    public final void changeDialog(@NotNull LottieDialog.Builder builder) {
        Intrinsics.checkNotNullParameter(builder, "builder");
        Context var10001 = builder.getContext();
        Intrinsics.checkNotNull(var10001);
        this.mContext = var10001;
        this.mType = builder.getType();
        this.mTitle = builder.getTitle();
        this.mDescription = builder.getDescription();
        this.mPositiveText = builder.getPositiveText();
        this.mNegativeText = builder.getNegativeText();
        this.mNoneText = builder.getNoneText();
        this.mPositiveListener = builder.getPositiveListener();
        this.mNegativeListener = builder.getNegativeListener();
        this.mNoneListener = builder.getNoneListener();
        this.positiveBtnColor = builder.getPositiveButtonColor();
        this.positiveTextColor = builder.getPositiveTextColor();
        this.negativeBtnColor = builder.getNegativeButtonColor();
        this.negativeTextColor = builder.getNegativeTextColor();
        this.noneBtnColor = builder.getNoneButtonColor();
        this.noneTextColor = builder.getNoneTextColor();
        LottieAnimationView var10000 = this.lAnimation;
        if (var10000 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("lAnimation");
        }

        Animation var2 = this.animationFadeOut;
        if (var2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("animationFadeOut");
        }

        var10000.startAnimation(var2);
        (new Handler()).postDelayed((Runnable)(new Runnable() {
            public final void run() {
                LottieDialog.this.setView();
            }
        }), 50L);
    }

    private LottieDialog(Context context, Integer type, String title, String description, String positiveText, String negativeText, String noneText, LottieDialog.LotieClickListener positiveListener, LottieDialog.LotieClickListener negativeListener, LottieDialog.LotieClickListener noneListener, Integer positiveBtnColor, Integer positiveTextColor, Integer negativeBtnColor, Integer negativeTextColor, Integer noneBtnColor, Integer noneTextColor) {
        super(context);
        this.mContext = context;
        this.mType = type;
        this.mTitle = title;
        this.mDescription = description;
        this.mPositiveText = positiveText;
        this.mNegativeText = negativeText;
        this.mNoneText = noneText;
        this.mPositiveListener = positiveListener;
        this.mNegativeListener = negativeListener;
        this.mNoneListener = noneListener;
        this.positiveBtnColor = positiveBtnColor;
        this.positiveTextColor = positiveTextColor;
        this.negativeBtnColor = negativeBtnColor;
        this.negativeTextColor = negativeTextColor;
        this.noneBtnColor = noneBtnColor;
        this.noneTextColor = noneTextColor;
        Animation var10001 = AnimationUtils.loadAnimation(context, anim.fade_in);
        Intrinsics.checkNotNullExpressionValue(var10001, "AnimationUtils.loadAnima…(context, R.anim.fade_in)");
        this.animationFadeIn = var10001;
        Animation var10000 = this.animationFadeIn;
        if (var10000 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("animationFadeIn");
        }

        var10000.setDuration(50L);
        var10001 = AnimationUtils.loadAnimation(context, anim.fade_out);
        Intrinsics.checkNotNullExpressionValue(var10001, "AnimationUtils.loadAnima…context, R.anim.fade_out)");
        this.animationFadeOut = var10001;
        var10000 = this.animationFadeOut;
        if (var10000 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("animationFadeOut");
        }

        var10000.setDuration(50L);
    }

    // $FF: synthetic method
    public LottieDialog(Context context, Integer type, String title, String description, String positiveText, String negativeText, String noneText, LottieDialog.LotieClickListener positiveListener, LottieDialog.LotieClickListener negativeListener, LottieDialog.LotieClickListener noneListener, Integer positiveBtnColor, Integer positiveTextColor, Integer negativeBtnColor, Integer negativeTextColor, Integer noneBtnColor, Integer noneTextColor, DefaultConstructorMarker $constructor_marker) {
        this(context, type, title, description, positiveText, negativeText, noneText, positiveListener, negativeListener, noneListener, positiveBtnColor, positiveTextColor, negativeBtnColor, negativeTextColor, noneBtnColor, noneTextColor);
    }

    // $FF: synthetic method
    public static final void access$setMPositiveListener$p(LottieDialog $this, LottieDialog.LotieClickListener var1) {
        $this.mPositiveListener = var1;
    }

    // $FF: synthetic method
    public static final void access$setMNegativeListener$p(LottieDialog $this, LottieDialog.LotieClickListener var1) {
        $this.mNegativeListener = var1;
    }

    // $FF: synthetic method
    public static final void access$setMNoneListener$p(LottieDialog $this, LottieDialog.LotieClickListener var1) {
        $this.mNoneListener = var1;
    }

    @Metadata(
            mv = {1, 6, 0},
            k = 1,
            xi = 2,
            d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b$\b\u0086\b\u0018\u00002\u00020\u0001B\u001d\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006J\u0006\u0010\u0018\u001a\u00020\u0019J\u000b\u0010\u001a\u001a\u0004\u0018\u00010\u0003HÂ\u0003J\u0010\u0010\u001b\u001a\u0004\u0018\u00010\u0005HÂ\u0003¢\u0006\u0002\u0010\u001cJ&\u0010\u001d\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005HÆ\u0001¢\u0006\u0002\u0010\u001eJ\u0013\u0010\u001f\u001a\u00020 2\b\u0010!\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\b\u0010\"\u001a\u0004\u0018\u00010\u0003J\b\u0010#\u001a\u0004\u0018\u00010\bJ\r\u0010$\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u001cJ\b\u0010%\u001a\u0004\u0018\u00010\fJ\b\u0010&\u001a\u0004\u0018\u00010\bJ\r\u0010'\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u001cJ\r\u0010(\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u001cJ\b\u0010)\u001a\u0004\u0018\u00010\fJ\b\u0010*\u001a\u0004\u0018\u00010\bJ\r\u0010+\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u001cJ\r\u0010,\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u001cJ\b\u0010-\u001a\u0004\u0018\u00010\fJ\b\u0010.\u001a\u0004\u0018\u00010\bJ\r\u0010/\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u001cJ\b\u00100\u001a\u0004\u0018\u00010\bJ\r\u00101\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u001cJ\t\u00102\u001a\u00020\u0005HÖ\u0001J\u0010\u00103\u001a\u00020\u00002\b\u0010\u0007\u001a\u0004\u0018\u00010\bJ\u0015\u00104\u001a\u00020\u00002\b\u00105\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u00106J\u0010\u00107\u001a\u00020\u00002\b\u0010\u000b\u001a\u0004\u0018\u00010\fJ\u0010\u00108\u001a\u00020\u00002\b\u0010\r\u001a\u0004\u0018\u00010\bJ\u0015\u00109\u001a\u00020\u00002\b\u00105\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u00106J\u0015\u0010:\u001a\u00020\u00002\b\u00105\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u00106J\u0010\u0010;\u001a\u00020\u00002\b\u0010\u0010\u001a\u0004\u0018\u00010\fJ\u0010\u0010<\u001a\u00020\u00002\b\u0010\u0011\u001a\u0004\u0018\u00010\bJ\u0015\u0010=\u001a\u00020\u00002\b\u00105\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u00106J\u0015\u0010>\u001a\u00020\u00002\b\u00105\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u00106J\u0010\u0010?\u001a\u00020\u00002\b\u0010\u0014\u001a\u0004\u0018\u00010\fJ\u0010\u0010@\u001a\u00020\u00002\b\u0010\u0015\u001a\u0004\u0018\u00010\bJ\u0015\u0010A\u001a\u00020\u00002\b\u00105\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u00106J\u0010\u0010B\u001a\u00020\u00002\b\u0010\u0017\u001a\u0004\u0018\u00010\bJ\t\u0010C\u001a\u00020\bHÖ\u0001R\u0010\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0082\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\t\u001a\u0004\u0018\u00010\u0005X\u0082\u000e¢\u0006\u0004\n\u0002\u0010\nR\u0010\u0010\u000b\u001a\u0004\u0018\u00010\fX\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\bX\u0082\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\u000e\u001a\u0004\u0018\u00010\u0005X\u0082\u000e¢\u0006\u0004\n\u0002\u0010\nR\u0012\u0010\u000f\u001a\u0004\u0018\u00010\u0005X\u0082\u000e¢\u0006\u0004\n\u0002\u0010\nR\u0010\u0010\u0010\u001a\u0004\u0018\u00010\fX\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0011\u001a\u0004\u0018\u00010\bX\u0082\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\u0012\u001a\u0004\u0018\u00010\u0005X\u0082\u000e¢\u0006\u0004\n\u0002\u0010\nR\u0012\u0010\u0013\u001a\u0004\u0018\u00010\u0005X\u0082\u000e¢\u0006\u0004\n\u0002\u0010\nR\u0010\u0010\u0014\u001a\u0004\u0018\u00010\fX\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0015\u001a\u0004\u0018\u00010\bX\u0082\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\u0016\u001a\u0004\u0018\u00010\u0005X\u0082\u000e¢\u0006\u0004\n\u0002\u0010\nR\u0010\u0010\u0017\u001a\u0004\u0018\u00010\bX\u0082\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0082\u0004¢\u0006\u0004\n\u0002\u0010\n¨\u0006D"},
            d2 = {"Lafb/elan/studio/LottieAlertDialog$Builder;", "", "context", "Landroid/content/Context;", "type", "", "(Landroid/content/Context;Ljava/lang/Integer;)V", "description", "", "negativeBtnColor", "Ljava/lang/Integer;", "negativeListener", "Lcom/labters/lottiealertdialoglibrary/ClickListener;", "negativeText", "negativeTextColor", "noneBtnColor", "noneListener", "noneText", "noneTextColor", "positiveBtnColor", "positiveListener", "positiveText", "positiveTextColor", "title", "build", "Lafb/elan/studio/LottieAlertDialog;", "component1", "component2", "()Ljava/lang/Integer;", "copy", "(Landroid/content/Context;Ljava/lang/Integer;)Lafb/elan/studio/LottieAlertDialog$Builder;", "equals", "", "other", "getContext", "getDescription", "getNegativeButtonColor", "getNegativeListener", "getNegativeText", "getNegativeTextColor", "getNoneButtonColor", "getNoneListener", "getNoneText", "getNoneTextColor", "getPositiveButtonColor", "getPositiveListener", "getPositiveText", "getPositiveTextColor", "getTitle", "getType", "hashCode", "setDescription", "setNegativeButtonColor", "color", "(Ljava/lang/Integer;)Lafb/elan/studio/LottieAlertDialog$Builder;", "setNegativeListener", "setNegativeText", "setNegativeTextColor", "setNoneButtonColor", "setNoneListener", "setNoneText", "setNoneTextColor", "setPositiveButtonColor", "setPositiveListener", "setPositiveText", "setPositiveTextColor", "setTitle", "toString", "app_debug"}
    )
    public static final class Builder {
        private String title;
        private String description;
        private String positiveText;
        private String negativeText;
        private String noneText;
        private LottieDialog.LotieClickListener positiveListener;
        private LottieDialog.LotieClickListener negativeListener;
        private LottieDialog.LotieClickListener noneListener;
        private Integer positiveBtnColor;
        private Integer positiveTextColor;
        private Integer negativeBtnColor;
        private Integer negativeTextColor;
        private Integer noneBtnColor;
        private Integer noneTextColor;
        private final Context context;
        private final Integer type;

        @NotNull
        public final LottieDialog.Builder setTitle(@Nullable String title) {
            LottieDialog.Builder $this$apply = (LottieDialog.Builder)this;
            $this$apply.title = title;
            return (LottieDialog.Builder)this;
        }

        @NotNull
        public final LottieDialog.Builder setDescription(@Nullable String description) {
            LottieDialog.Builder $this$apply = (LottieDialog.Builder)this;
            $this$apply.description = description;
            return (LottieDialog.Builder)this;
        }

        @NotNull
        public final LottieDialog.Builder setPositiveText(@Nullable String positiveText) {
            LottieDialog.Builder $this$apply = (LottieDialog.Builder)this;
            $this$apply.positiveText = positiveText;
            return (LottieDialog.Builder)this;
        }

        @NotNull
        public final LottieDialog.Builder setNegativeText(@Nullable String negativeText) {
            LottieDialog.Builder $this$apply = (LottieDialog.Builder)this;
            $this$apply.negativeText = negativeText;
            return (LottieDialog.Builder)this;
        }

        @NotNull
        public final LottieDialog.Builder setNoneText(@Nullable String noneText) {
            LottieDialog.Builder $this$apply = (LottieDialog.Builder)this;
            $this$apply.noneText = noneText;
            return (LottieDialog.Builder)this;
        }

        @NotNull
        public final LottieDialog.Builder setPositiveListener(@Nullable LottieDialog.LotieClickListener positiveListener) {
            LottieDialog.Builder $this$apply = (LottieDialog.Builder)this;
            $this$apply.positiveListener = positiveListener;
            return (LottieDialog.Builder)this;
        }

        @NotNull
        public final LottieDialog.Builder setNegativeListener(@Nullable LottieDialog.LotieClickListener negativeListener) {
            LottieDialog.Builder $this$apply = (LottieDialog.Builder)this;
            $this$apply.negativeListener = negativeListener;
            return (LottieDialog.Builder)this;
        }

        @NotNull
        public final LottieDialog.Builder setNoneListener(@Nullable LottieDialog.LotieClickListener noneListener) {
            LottieDialog.Builder $this$apply = (LottieDialog.Builder)this;
            $this$apply.noneListener = noneListener;
            return (LottieDialog.Builder)this;
        }

        @NotNull
        public final LottieDialog.Builder setPositiveButtonColor(@Nullable Integer color) {
            LottieDialog.Builder $this$apply = (LottieDialog.Builder)this;
            $this$apply.positiveBtnColor = color;
            return (LottieDialog.Builder)this;
        }

        @NotNull
        public final LottieDialog.Builder setPositiveTextColor(@Nullable Integer color) {
            LottieDialog.Builder $this$apply = (LottieDialog.Builder)this;
            $this$apply.positiveTextColor = color;
            return (LottieDialog.Builder)this;
        }

        @NotNull
        public final LottieDialog.Builder setNegativeButtonColor(@Nullable Integer color) {
            LottieDialog.Builder $this$apply = (LottieDialog.Builder)this;
            $this$apply.negativeBtnColor = color;
            return (LottieDialog.Builder)this;
        }

        @NotNull
        public final LottieDialog.Builder setNegativeTextColor(@Nullable Integer color) {
            LottieDialog.Builder $this$apply = (LottieDialog.Builder)this;
            $this$apply.negativeTextColor = color;
            return (LottieDialog.Builder)this;
        }

        @NotNull
        public final LottieDialog.Builder setNoneButtonColor(@Nullable Integer color) {
            LottieDialog.Builder $this$apply = (LottieDialog.Builder)this;
            $this$apply.noneBtnColor = color;
            return (LottieDialog.Builder)this;
        }

        @NotNull
        public final LottieDialog.Builder setNoneTextColor(@Nullable Integer color) {
            LottieDialog.Builder $this$apply = (LottieDialog.Builder)this;
            $this$apply.noneTextColor = color;
            return (LottieDialog.Builder)this;
        }

        @NotNull
        public final LottieDialog build() {
            Context var10002 = this.context;
            Intrinsics.checkNotNull(var10002);
            return new LottieDialog(var10002, this.type, this.title, this.description, this.positiveText, this.negativeText, this.noneText, this.positiveListener, this.negativeListener, this.noneListener, this.positiveBtnColor, this.positiveTextColor, this.negativeBtnColor, this.negativeTextColor, this.noneBtnColor, this.noneTextColor, (DefaultConstructorMarker)null);
        }

        @Nullable
        public final Context getContext() {
            return this.context;
        }

        @Nullable
        public final Integer getType() {
            return this.type;
        }

        @Nullable
        public final String getTitle() {
            return this.title;
        }

        @Nullable
        public final String getDescription() {
            return this.description;
        }

        @Nullable
        public final String getPositiveText() {
            return this.positiveText;
        }

        @Nullable
        public final String getNegativeText() {
            return this.negativeText;
        }

        @Nullable
        public final String getNoneText() {
            return this.noneText;
        }

        @Nullable
        public final LottieDialog.LotieClickListener getPositiveListener() {
            return this.positiveListener;
        }

        @Nullable
        public final LottieDialog.LotieClickListener getNegativeListener() {
            return this.negativeListener;
        }

        @Nullable
        public final LottieDialog.LotieClickListener getNoneListener() {
            return this.noneListener;
        }

        @Nullable
        public final Integer getPositiveButtonColor() {
            return this.positiveBtnColor;
        }

        @Nullable
        public final Integer getPositiveTextColor() {
            return this.positiveTextColor;
        }

        @Nullable
        public final Integer getNegativeButtonColor() {
            return this.negativeBtnColor;
        }

        @Nullable
        public final Integer getNegativeTextColor() {
            return this.negativeTextColor;
        }

        @Nullable
        public final Integer getNoneButtonColor() {
            return this.noneBtnColor;
        }

        @Nullable
        public final Integer getNoneTextColor() {
            return this.noneTextColor;
        }

        public Builder(@Nullable Context context, @Nullable Integer type) {
            this.context = context;
            this.type = type;
        }

        // $FF: synthetic method
        public Builder(Context var1, Integer var2, int var3, DefaultConstructorMarker var4) {
            this(var1, var2);
            if ((var3 & 1) != 0) {
                var1 = (Context)null;
            }

            if ((var3 & 2) != 0) {
                var2 = (Integer)null;
            }

        }

        public Builder() {
            this((Context)null, (Integer)null, 3, (DefaultConstructorMarker)null);
        }

        private final Context component1() {
            return this.context;
        }

        private final Integer component2() {
            return this.type;
        }

        @NotNull
        public final LottieDialog.Builder copy(@Nullable Context context, @Nullable Integer type) {
            return new LottieDialog.Builder(context, type);
        }

        // $FF: synthetic method
        public static LottieDialog.Builder copy$default(LottieDialog.Builder var0, Context var1, Integer var2, int var3, Object var4) {
            if ((var3 & 1) != 0) {
                var1 = var0.context;
            }

            if ((var3 & 2) != 0) {
                var2 = var0.type;
            }

            return var0.copy(var1, var2);
        }

        @NotNull
        public String toString() {
            return "Builder(context=" + this.context + ", type=" + this.type + ")";
        }

        public int hashCode() {
            Context var10000 = this.context;
            int var1 = (var10000 != null ? var10000.hashCode() : 0) * 31;
            Integer var10001 = this.type;
            return var1 + (var10001 != null ? var10001.hashCode() : 0);
        }

        public boolean equals(@Nullable Object var1) {
            if (this != var1) {
                if (var1 instanceof LottieDialog.Builder) {
                    LottieDialog.Builder var2 = (LottieDialog.Builder)var1;
                    if (Intrinsics.areEqual(this.context, var2.context) && Intrinsics.areEqual(this.type, var2.type)) {
                        return true;
                    }
                }

                return false;
            } else {
                return true;
            }
        }
    }
}

