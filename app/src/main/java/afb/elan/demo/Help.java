package afb.elan.demo;

import android.app.Activity;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources.NotFoundException;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Animation.AnimationListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class Help extends AppCompatActivity implements OnTouchListener, OnClickListener {

	TextView tv_help_add, tv_help_about, tv_help_edit_delete, tv_help_delete_all,tv_help_load,
	         tv_help_search,tv_help_login,tv_help_change_pass,tv_help_load_pic,tv_help_sync,tv_help_widget,tv_help_setting;
	int lastViewed = 0;
	LinearLayout llFade;
	Animation anim_expand_catalog,anim_close_catalog;
	
	Handler handler= new  Handler(){
		@Override
		public void handleMessage(Message msg) {

			super.handleMessage(msg);
			switch (msg.what){

				
				default:
					ScrollView sv=(ScrollView)findViewById(R.id.main_scroll);
					//Toast.makeText(Catalog.this, "123", 1000).show();
					sv.scrollTo(0, ((LinearLayout)findViewById(R.id.lv_help_widget)).getBottom());
			}		
		}		
	};

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.help);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar1);
		setSupportActionBar(toolbar);
		if (getSupportActionBar() != null) {
			getSupportActionBar().setDisplayShowHomeEnabled(true);
			getSupportActionBar().setHomeButtonEnabled(true);
			getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_TITLE | ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_HOME_AS_UP);
		}

		tv_help_add = (TextView) findViewById(R.id.tv_help_add);
		tv_help_add.setOnClickListener(this);
		tv_help_about = (TextView) findViewById(R.id.tv_help_about);
		tv_help_about.setOnClickListener(this);
		tv_help_edit_delete = (TextView) findViewById(R.id.tv_help_edit_delete);
		tv_help_edit_delete.setOnClickListener(this);
		tv_help_delete_all = (TextView) findViewById(R.id.tv_help_delete_all);
		tv_help_delete_all.setOnClickListener(this);
		tv_help_load = (TextView) findViewById(R.id.tv_help_load);
		tv_help_load.setOnClickListener(this);
		tv_help_search = (TextView) findViewById(R.id.tv_help_search);
		tv_help_search.setOnClickListener(this);
		tv_help_login = (TextView) findViewById(R.id.tv_help_login);
		tv_help_login.setOnClickListener(this);
		tv_help_change_pass = (TextView) findViewById(R.id.tv_help_change_pass);
		tv_help_change_pass.setOnClickListener(this);		
		tv_help_load_pic = (TextView) findViewById(R.id.tv_help_load_pic);
		tv_help_load_pic.setOnClickListener(this);	
		tv_help_sync = (TextView) findViewById(R.id.tv_help_sync);
		tv_help_sync.setOnClickListener(this);	
		tv_help_widget = (TextView) findViewById(R.id.tv_help_widget);
		tv_help_widget.setOnClickListener(this);
		tv_help_setting = (TextView) findViewById(R.id.tv_help_setting);
		tv_help_setting.setOnClickListener(this);
		TextView tv_version=(TextView)findViewById(R.id.tv_Version);
		try {
			tv_version.setText(getResources().getString(R.string.version)+" "+getPackageManager().getPackageInfo(getPackageName(), 0).versionName);
		} catch (NotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			tv_version.setVisibility(View.INVISIBLE);
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			tv_version.setVisibility(View.INVISIBLE);
		}

		anim_expand_catalog=AnimationUtils.loadAnimation(this,R.anim.anim_expand_catalog);
		anim_close_catalog=AnimationUtils.loadAnimation(this,R.anim.anim_close_catalog);
		
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				finish();
				break;


		}
		return super.onOptionsItemSelected(item);
	}


	@Override
	public void finish() {
		// we need to override this to performe the animtationOut on each
		// finish.

		super.finish();
		// disable default animation

	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		/*
		LinearLayout ll = null;
		switch (arg0.getId()) {
		case R.id.tv_help_about:
			ll = (LinearLayout) findViewById(R.id.lv_help_about);
			if (ll.getVisibility() == View.GONE) {
				tv_help_about.setTextColor(Color.RED);
				tv_help_about.setBackgroundColor(Color.argb(0x44, 0xaa, 0x66, 0x66));
				ll.setVisibility(View.VISIBLE);
				if (lastViewed != 0)
					((TextView) findViewById(lastViewed)).performClick();
				lastViewed = R.id.tv_help_about;
			} else {
				tv_help_about.setTextColor(getResources()
						.getColor(R.color.blue));
				tv_help_about.setBackgroundColor(Color.argb(0x44, 0x66, 0x66, 0xaa));
				ll.setVisibility(View.GONE);
				lastViewed = 0;
			}
			break;

		case R.id.tv_help_add:
			ll = (LinearLayout) findViewById(R.id.lv_help_add);
			if (ll.getVisibility() == View.GONE) {

				tv_help_add.setTextColor(Color.RED);
				tv_help_add.setBackgroundColor(Color.argb(0x44, 0xaa, 0x66, 0x66));
				ll.setVisibility(View.VISIBLE);
				if (lastViewed != 0)
					((TextView) findViewById(lastViewed)).performClick();
				lastViewed = R.id.tv_help_add;
			} else {
				tv_help_add.setTextColor(getResources().getColor(R.color.blue));
				tv_help_add.setBackgroundColor(Color.argb(0x44, 0x66, 0x66, 0xaa));
				ll.setVisibility(View.GONE);
				lastViewed = 0;
			}
			break;
			
		case R.id.tv_help_edit_delete:
			ll = (LinearLayout) findViewById(R.id.lv_help_edit_delete);
			if (ll.getVisibility() == View.GONE) {

				tv_help_edit_delete.setTextColor(Color.RED);
				tv_help_edit_delete.setBackgroundColor(Color.argb(0x44, 0xaa, 0x66, 0x66));
				ll.setVisibility(View.VISIBLE);
				if (lastViewed != 0)
					((TextView) findViewById(lastViewed)).performClick();
				lastViewed = R.id.tv_help_edit_delete;
			} else {
				tv_help_edit_delete.setTextColor(getResources().getColor(R.color.blue));
				tv_help_edit_delete.setBackgroundColor(Color.argb(0x44, 0x66, 0x66, 0xaa));
				ll.setVisibility(View.GONE);
				lastViewed = 0;
			}
			break;	
			
		case R.id.tv_help_delete_all:
			ll = (LinearLayout) findViewById(R.id.lv_help_delete_all);
			if (ll.getVisibility() == View.GONE) {

				tv_help_delete_all.setTextColor(Color.RED);
				tv_help_delete_all.setBackgroundColor(Color.argb(0x44, 0xaa, 0x66, 0x66));
				ll.setVisibility(View.VISIBLE);
				if (lastViewed != 0)
					((TextView) findViewById(lastViewed)).performClick();
				lastViewed = R.id.tv_help_delete_all;
			} else {
				tv_help_delete_all.setTextColor(getResources().getColor(R.color.blue));
				tv_help_delete_all.setBackgroundColor(Color.argb(0x44, 0x66, 0x66, 0xaa));
				ll.setVisibility(View.GONE);
				lastViewed = 0;
			}
			break;
			
		case R.id.tv_help_load:
			ll = (LinearLayout) findViewById(R.id.lv_help_load);
			if (ll.getVisibility() == View.GONE) {

				tv_help_load.setTextColor(Color.RED);
				tv_help_load.setBackgroundColor(Color.argb(0x44, 0xaa, 0x66, 0x66));
				ll.setVisibility(View.VISIBLE);
				if (lastViewed != 0)
					((TextView) findViewById(lastViewed)).performClick();
				lastViewed = R.id.tv_help_load;
			} else {
				tv_help_load.setTextColor(getResources().getColor(R.color.blue));
				tv_help_load.setBackgroundColor(Color.argb(0x44, 0x66, 0x66, 0xaa));
				ll.setVisibility(View.GONE);
				lastViewed = 0;
			}
			break;	
			
		case R.id.tv_help_search:
			ll = (LinearLayout) findViewById(R.id.lv_help_search);
			if (ll.getVisibility() == View.GONE) {

				tv_help_search.setTextColor(Color.RED);
				tv_help_search.setBackgroundColor(Color.argb(0x44, 0xaa, 0x66, 0x66));
				ll.setVisibility(View.VISIBLE);
				if (lastViewed != 0)
					((TextView) findViewById(lastViewed)).performClick();
				lastViewed = R.id.tv_help_search;
			} else {
				tv_help_search.setTextColor(getResources().getColor(R.color.blue));
				tv_help_search.setBackgroundColor(Color.argb(0x44, 0x66, 0x66, 0xaa));
				ll.setVisibility(View.GONE);
				lastViewed = 0;
			}
			break;	

		case R.id.tv_help_load_pic:
			ll = (LinearLayout) findViewById(R.id.lv_help_load_pic);
			if (ll.getVisibility() == View.GONE) {

				tv_help_load_pic.setTextColor(Color.RED);
				tv_help_load_pic.setBackgroundColor(Color.argb(0x44, 0xaa, 0x66, 0x66));
				ll.setVisibility(View.VISIBLE);
				if (lastViewed != 0)
					((TextView) findViewById(lastViewed)).performClick();
				lastViewed = R.id.tv_help_load_pic;
			} else {
				tv_help_load_pic.setTextColor(getResources().getColor(R.color.blue));
				tv_help_load_pic.setBackgroundColor(Color.argb(0x44, 0x66, 0x66, 0xaa));
				ll.setVisibility(View.GONE);
				lastViewed = 0;
			}
			break;	
			
		case R.id.tv_help_login:
			ll = (LinearLayout) findViewById(R.id.lv_help_login);
			if (ll.getVisibility() == View.GONE) {

				tv_help_login.setTextColor(Color.RED);
				tv_help_login.setBackgroundColor(Color.argb(0x44, 0xaa, 0x66, 0x66));
				ll.setVisibility(View.VISIBLE);
				if (lastViewed != 0)
					((TextView) findViewById(lastViewed)).performClick();
				lastViewed = R.id.tv_help_login;
			} else {
				tv_help_login.setTextColor(getResources().getColor(R.color.blue));
				tv_help_login.setBackgroundColor(Color.argb(0x44, 0x66, 0x66, 0xaa));
				ll.setVisibility(View.GONE);
				lastViewed = 0;
			}
			break;
			
		case R.id.tv_help_change_pass:
			ll = (LinearLayout) findViewById(R.id.lv_help_change_pass);
			if (ll.getVisibility() == View.GONE) {

				tv_help_change_pass.setTextColor(Color.RED);
				tv_help_change_pass.setBackgroundColor(Color.argb(0x44, 0xaa, 0x66, 0x66));
				ll.setVisibility(View.VISIBLE);
				if (lastViewed != 0)
					((TextView) findViewById(lastViewed)).performClick();
				lastViewed = R.id.tv_help_change_pass;
			} else {
				tv_help_change_pass.setTextColor(getResources().getColor(R.color.blue));
				tv_help_change_pass.setBackgroundColor(Color.argb(0x44, 0x66, 0x66, 0xaa));
				ll.setVisibility(View.GONE);
				lastViewed = 0;
			}
			break;	
			
		case R.id.tv_help_sync:
			ll = (LinearLayout) findViewById(R.id.lv_help_sync);
			if (ll.getVisibility() == View.GONE) {

				tv_help_sync.setTextColor(Color.RED);
				tv_help_sync.setBackgroundColor(Color.argb(0x44, 0xaa, 0x66, 0x66));
				ll.setVisibility(View.VISIBLE);
				if (lastViewed != 0)
					((TextView) findViewById(lastViewed)).performClick();
				lastViewed = R.id.tv_help_sync;
			} else {
				tv_help_sync.setTextColor(getResources().getColor(R.color.blue));
				tv_help_sync.setBackgroundColor(Color.argb(0x44, 0x66, 0x66, 0xaa));
				ll.setVisibility(View.GONE);
				lastViewed = 0;
			}
			break;
			
		case R.id.tv_help_setting:
			ll = (LinearLayout) findViewById(R.id.lv_help_setting);
			if (ll.getVisibility() == View.GONE) {

				tv_help_setting.setTextColor(Color.RED);
				tv_help_setting.setBackgroundColor(Color.argb(0x44, 0xaa, 0x66, 0x66));
				ll.setVisibility(View.VISIBLE);
				if (lastViewed != 0)
					((TextView) findViewById(lastViewed)).performClick();
				lastViewed = R.id.tv_help_setting;
			} else {
				tv_help_setting.setTextColor(getResources().getColor(R.color.blue));
				tv_help_setting.setBackgroundColor(Color.argb(0x44, 0x66, 0x66, 0xaa));
				ll.setVisibility(View.GONE);
				lastViewed = 0;
			}
			break;			
			
		case R.id.tv_help_widget:
			ll = (LinearLayout) findViewById(R.id.lv_help_widget);
			if (ll.getVisibility() == View.GONE) {

				tv_help_widget.setTextColor(Color.RED);
				tv_help_widget.setBackgroundColor(Color.argb(0x44, 0xaa, 0x66, 0x66));
				ll.setVisibility(View.VISIBLE);
				if (lastViewed != 0)
					((TextView) findViewById(lastViewed)).performClick();
				lastViewed = R.id.tv_help_widget;
			} else {
				tv_help_widget.setTextColor(getResources().getColor(R.color.blue));
				tv_help_widget.setBackgroundColor(Color.argb(0x44, 0x66, 0x66, 0xaa));
				ll.setVisibility(View.GONE);
				lastViewed = 0;
			}
			break;
			
		case R.id.bReturnFromAbout:
			finish();
			break;
		}
	*/
		
		
		//if (arg0.getTag()!=null){
			switch (arg0.getId()){
			
			
			  case R.id.tv_help_about:
				  llFade=(LinearLayout)findViewById(R.id.lv_help_about);
				break;
				
			  case R.id.tv_help_add:
				  llFade=(LinearLayout)findViewById(R.id.lv_help_add);
				break;
				
			  case R.id.tv_help_edit_delete:
				  llFade=(LinearLayout)findViewById(R.id.lv_help_edit_delete);
				break;
				
			  case R.id.tv_help_delete_all:
				  llFade=(LinearLayout)findViewById(R.id.lv_help_delete_all);
				break;
				
			  case R.id.tv_help_load:
				  llFade=(LinearLayout)findViewById(R.id.lv_help_load);
				break;
				
			  case R.id.tv_help_search:
				  llFade=(LinearLayout)findViewById(R.id.lv_help_search);
				break;
				
			  case R.id.tv_help_load_pic:
				  llFade=(LinearLayout)findViewById(R.id.lv_help_load_pic);
				break;	
				
			  case R.id.tv_help_login:
				  llFade=(LinearLayout)findViewById(R.id.lv_help_login);
				break;	
				
			  case R.id.tv_help_change_pass:
				  llFade=(LinearLayout)findViewById(R.id.lv_help_change_pass);
				break;	
				
			  case R.id.tv_help_sync:
				  llFade=(LinearLayout)findViewById(R.id.lv_help_sync);
				break;	
				
			  case R.id.tv_help_setting:
				  llFade=(LinearLayout)findViewById(R.id.lv_help_setting);
				break;	
				
			  case R.id.tv_help_widget:
				  llFade=(LinearLayout)findViewById(R.id.lv_help_widget);
				break;	
				
			}
			
			anim_close_catalog.setAnimationListener(new AnimationListener() {
				
				@Override
				public void onAnimationStart(Animation arg0) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onAnimationRepeat(Animation arg0) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onAnimationEnd(Animation arg0) {
					// TODO Auto-generated method stub
					llFade.setVisibility(View.GONE);
				}
			});
			
			
			
			if (llFade.getVisibility()==View.GONE){
				llFade.setVisibility(View.VISIBLE);
				((TextView)arg0).setTextColor(Color.RED);
				((TextView)arg0).setBackgroundColor(Color.argb(0x44, 0xaa, 0x66, 0x66));
				llFade.startAnimation(anim_expand_catalog);
				if (arg0.getId()==R.id.tv_help_widget){				
					Message msg7=new Message();
					msg7.what=0;
					handler.sendMessageDelayed(msg7, 30);
					

				}
			}
				
			else
			{
				((TextView)arg0).setTextColor(getResources().getColor(R.color.blue));
				((TextView)arg0).setBackgroundColor(Color.argb(0x44, 0x66, 0x66, 0xaa));
				llFade.startAnimation(anim_close_catalog);
				//ll.setVisibility(View.GONE);					

			}			

			/*
			
		}
		else // agar rooye title ha click nashode ast (dar natije rooye link ha click shode ast)
		{
			switch (arg0.getId()){
			
			
				
			}
		}
*/
		
	}

	@Override
	public boolean onTouch(View v, MotionEvent me) {
		// TODO Auto-generated method stub

		switch (me.getAction()) {
		case MotionEvent.ACTION_DOWN:
			((TextView) v).setTextColor(Color.RED);
			// ((TextView)v).setBackgroundColor(getResources().getColor(R.color.blue));

			break;
		case MotionEvent.ACTION_UP:
			((TextView) v).setTextColor(getResources().getColor(R.color.blue));
			// ((TextView)v).setBackgroundColor(Color.TRANSPARENT);

			break;

		}
		return false;
	}

}
