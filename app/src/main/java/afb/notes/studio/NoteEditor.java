package afb.notes.studio;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;


import java.util.List;

import afb.elan.demo.Db.NoteObject;
import afb.elan.demo.Db.SQLHelper;
import afb.elan.demo.MainActivity;
import afb.elan.demo.R;
import afb.elan.demo.Services.DemoService;
import afb.elan.demo.calendar.SolarDate;
import afb.elan.demo.calendar.SolarDateDialog;
import afb.notes.studio.widget.NoteUpdateWidget;
import dev.shreyaspatil.MaterialDialog.BottomSheetMaterialDialog;


public class NoteEditor extends AppCompatActivity {
	EditText eNoteBody,etTitle;
	boolean editing=false;
	boolean edited=false;
	ScrollView ss;
	NoteObject editingNote=null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.notesedit);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			Window window = getWindow();
			window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			window.setStatusBarColor(Color.rgb(110,73,32));
		}



        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);


        eNoteBody=(EditText)findViewById(R.id.eNoteBody);
		etTitle=(EditText) findViewById(R.id.etNoteTitle);
		String action = getIntent().getAction();

		if (action==null) action="null";

		if (action.equals(Intent.ACTION_VIEW)) {
			// throw new RuntimeException("Should not happen");
			Uri data = getIntent().getData();
			String scheme = data.getScheme(); // "elanapp"
			String host = data.getHost(); // "afb.elan.studio"
			List<String> paths = data.getPathSegments(); // 0:note
			int id=Integer.valueOf(data.getQueryParameter("id"));
			editing=true;
			setTitle("ویرایش یادداشت");
			SQLHelper cdbo = new SQLHelper(this);
			cdbo.openDb();
			editingNote=cdbo.fetchNoteRecord(id);
			eNoteBody.setText(editingNote.body);
			etTitle.setText(editingNote.title);
			if (!editingNote.date.isEmpty()){
				solarDate=SolarDate.newInstance(SolarDate.DATE_KIND_SOLAR);
				solarDate.setSolarDateFromString(editingNote.date);
				((TextView)findViewById(R.id.tvDate)).setText(solarDate.toString());
				((LinearLayout)findViewById(R.id.lvDate)).setVisibility(View.VISIBLE);
			}

			Log.e("editingNote", editingNote.note_id + "");
			cdbo.closeDb();
		}
		else {
			editing = getIntent().getStringExtra("mode").equals("edit");
			if (editing) {
				setTitle("ویرایش یادداشت");
				//showOption(R.id.action_share);
				editingNote = (NoteObject) getIntent().getSerializableExtra("note");
				//s=getIntent().getExtras().getString("Body").toString();
				eNoteBody.setText(editingNote.body);
				etTitle.setText(editingNote.title);
				if (!editingNote.date.isEmpty()){
					solarDate=SolarDate.newInstance(SolarDate.DATE_KIND_SOLAR);
					solarDate.setSolarDateFromString(editingNote.date);
					((TextView)findViewById(R.id.tvDate)).setText(solarDate.toString());
					((LinearLayout)findViewById(R.id.lvDate)).setVisibility(View.VISIBLE);
				}

				Log.e("editingNote", editingNote.note_id + "");
				// bLoad.setEnabled(false);


			} else
				setTitle("یادداشت جدید");
		}

		ss=(ScrollView)findViewById(R.id.mainscrolview);
		ss.setOnClickListener(new ScrollView.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				eNoteBody.requestFocus();
				eNoteBody.performClick();
				eNoteBody.setSelection(eNoteBody.getText().toString().length());
				
			}
		});

		TextWatcher tw=new TextWatcher() {

			@Override
			public void afterTextChanged(Editable s) {}

			@Override
			public void beforeTextChanged(CharSequence s, int start,
										  int count, int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start,
									  int before, int count) {
				edited=true;
			}
		};
		etTitle.addTextChangedListener(tw);
		eNoteBody.addTextChangedListener(tw);
		((TextView)findViewById(R.id.tvDate)).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				setAlarm(((TextView)findViewById(R.id.tvDate)).getText().toString());
			}
		});
	}

	public void showOption(int id) {
		MenuItem item = menu.findItem(id);
		item.setVisible(true);

	}

	public void hideOption(int id) {
		MenuItem item = menu.findItem(id);
		item.setVisible(false);
	}

	private Menu menu = null;

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub

		this.menu = menu;
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.note_edit, menu);
		return true;
	}
	
	

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch(item.getItemId()){
			case android.R.id.home:
				onBackPressed();
				break;

			case R.id.action_save:
				//setupSavingDialog(this,false,true);
				saveData(true,true);
				break;

			case R.id.action_share:
				Intent sharingIntent = new Intent(Intent.ACTION_SEND);
				sharingIntent.setType("text/plain");
				//sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, etTitle.getText().toString());
				sharingIntent.putExtra(Intent.EXTRA_TEXT, etTitle.getText().toString()+"\n\n"+eNoteBody.getText().toString());
				startActivity(Intent.createChooser(sharingIntent, getResources().getString(R.string.share_using)));
				break;

			case R.id.action_alarm:
				//setupSavingDialog(this,false,true);
				setAlarm();
				break;



		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onBackPressed() {
	    
		
		if (edited){

			setupSavingDialog(this,true,true);
		}
		else{
			updateList(null,true,false);
		}


	    	
	    }
	
	private void updateList(NoteObject no, boolean needExit, boolean needRefresh){
		if (needRefresh){
			Intent intent;
			if (editing)
				intent=new Intent(MainActivity.ELAN_UPDATE_NOTE);
			else
				intent=new Intent(MainActivity.ELAN_INSERT_NOTE);
			Bundle basket=new Bundle();
			basket.putBoolean("NeedRefresh", needRefresh);
			basket.putSerializable("NoteObject",no);
			intent.putExtras(basket);
			sendBroadcast(intent);
		}
		if (needExit) finish();
		

	}

	private boolean saveData(final boolean exit, boolean needRefresh){

		//final Dialog dg = Utils.prepareLoadingDialog(NoteEditor.this, true);
		//dg.show();
		String body = eNoteBody.getText().toString();
		String title = etTitle.getText().toString();

		SQLHelper cdbo = new SQLHelper(
				NoteEditor.this);
		String date=(solarDate!=null)?solarDate.toString():"";
		if (editing) {

			// editmode
			editingNote.body=body;
			editingNote.title=title;
			editingNote.date=date;
			cdbo.openDb();
			cdbo.UpdateNoteRecord(editingNote);
			cdbo.closeDb();

			Toast.makeText(
					NoteEditor.this,
					getResources().getString(
							R.string.recordUpdated), Toast.LENGTH_SHORT)
					.show();
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(
					etTitle.getWindowToken(), 0);
			updateList(editingNote,exit,needRefresh);
			updateWidget(editingNote);

		} else {
			SolarDate sd = new SolarDate();
			sd.changeDateKind(SolarDate.DATE_KIND_SOLAR);
			String created = sd.toString();


			NoteObject no=new NoteObject();
			no.setContent(-1,title,body,"",date,"",created);

			cdbo.openDb();
			cdbo.AddNoteRecoord(no);
			cdbo.closeDb();


			Toast.makeText(NoteEditor.this, getResources().getString(R.string.recordAdded),
					Toast.LENGTH_SHORT).show();
			updateList(no,exit,needRefresh);

		}

		Intent intent=new Intent(DemoService.AFB_ELAN_STUDIO_DEMORECEIVER);
		intent.putExtra("needCheck",true);
		sendBroadcast(intent);


		edited=false;
		return true;
	}


	private void setupSavingDialog(Context context, boolean needExit, boolean needRefresh) {
		String message =getResources().getString(R.string.savingMeesage);
		if (editing)
			message= getResources().getString(R.string.cancelSaveDialog);

		afb.elan.MaterialDialog.BottomSheetMaterialDialog mBottomSheetDialog = new afb.elan.MaterialDialog.BottomSheetMaterialDialog.Builder(this)
				.setTitle(getResources().getString(R.string.save))
				.setMessage(message)
				.setCancelable(true)
				.setAnimation(R.raw.questionmark)
				.setPositiveButton(getResources().getString(R.string.yes), R.drawable.ic_action_yes, new BottomSheetMaterialDialog.OnClickListener() {
					@Override
					public void onClick(dev.shreyaspatil.MaterialDialog.interfaces.DialogInterface dialogInterface, int which) {
						dialogInterface.dismiss();
						saveData(needExit,needRefresh);
					}
				})
				.setNegativeButton("صرف نظر", R.drawable.ic_action_no, new BottomSheetMaterialDialog.OnClickListener() {
					@Override
					public void onClick(dev.shreyaspatil.MaterialDialog.interfaces.DialogInterface dialogInterface, int which) {
						dialogInterface.dismiss();
						if (needExit) finish();

					}
				})
				.build();

		// Show Dialog
		mBottomSheetDialog.show();



	}


	private void updateWidget(NoteObject note){
    	/*SharedPreferences shp= getSharedPreferences("afbWidgetIDs", 0);
    	int awID=shp.getInt("getWidgetFromNote"+editingNote.note_id, -1);

		RemoteViews views = new RemoteViews(
				WidgetNoteOpen.this.getPackageName(), R.layout.widget_note);
		if (!Password_Protected) {
			views.setTextViewText(R.id.tvNoteBody2, eNoteBody.getText()
					.toString());
		} else
			views.setTextViewText(R.id.tvNoteBody2, "~Protected~");

		Intent in = new Intent(WidgetNoteOpen.this, WidgetNoteOpen.class);
		Bundle basket = new Bundle();

		basket.putInt(AppWidgetManager.EXTRA_APPWIDGET_ID, awID);
		in.putExtras(basket);
		in.setAction(awID + "");
		PendingIntent pi = PendingIntent.getActivity(WidgetNoteOpen.this, 0,
				in, PendingIntent.FLAG_UPDATE_CURRENT);
		views.setOnClickPendingIntent(R.id.tvNoteBody2, pi);
		AppWidgetManager awm = AppWidgetManager.getInstance(WidgetNoteOpen.this);
		awm.updateAppWidget(awID, views);*/

		NoteUpdateWidget uw=new NoteUpdateWidget(this, note.note_id, -1,null);
		uw.doUpdateFromNote(eNoteBody.getText().toString(), etTitle.toString(),editingNote.pass.length()>0,true,true,true);
	}


	private SolarDate solarDate=null;
	private void setAlarm(){
		if (solarDate!=null){
			solarDate=null;
			((LinearLayout)findViewById(R.id.lvDate)).setVisibility(View.GONE);
			edited=true;
			return;
		}
		SolarDateDialog sdd=new SolarDateDialog(this);
		sdd.setPersianDate((solarDate==null)?SolarDate.newInstance(SolarDate.DATE_KIND_SOLAR).toString():solarDate.toString());
		sdd.setOnSublitListener(new SolarDateDialog.onSubmitListener() {

			@Override
			public void onSubmit(String date) {
				// TODO Auto-generated method stub
					solarDate=SolarDate.newInstance(SolarDate.DATE_KIND_SOLAR);
					solarDate.setSolarDateFromString(date);
					((TextView)findViewById(R.id.tvDate)).setText(solarDate.toString());
					((LinearLayout)findViewById(R.id.lvDate)).setVisibility(View.VISIBLE);
				edited=true;
			}
		});
		sdd.show();

	}

	private void setAlarm(String changedate){
		SolarDateDialog sdd=new SolarDateDialog(this);
		sdd.setPersianDate((changedate==null)?SolarDate.newInstance(SolarDate.DATE_KIND_SOLAR).toString():changedate.toString());
		sdd.setOnSublitListener(new SolarDateDialog.onSubmitListener() {

			@Override
			public void onSubmit(String date) {
				// TODO Auto-generated method stub
				solarDate=SolarDate.newInstance(SolarDate.DATE_KIND_SOLAR);
				solarDate.setSolarDateFromString(date);
				((TextView)findViewById(R.id.tvDate)).setText(solarDate.toString());
				((LinearLayout)findViewById(R.id.lvDate)).setVisibility(View.VISIBLE);
				edited=true;
			}
		});
		sdd.show();

	}


}
