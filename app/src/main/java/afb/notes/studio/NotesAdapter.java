package afb.notes.studio;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import afb.elan.demo.Db.NoteObject;
import afb.elan.demo.Db.SQLHelper;
import afb.elan.demo.R;
import afb.elan.demo.calendar.SolarDate;
import afb.elan.demo.ui.SquareImageView;

public class NotesAdapter extends RecyclerView.Adapter<NotesAdapter.MyViewHolder> {

    private Context context;

    private ArrayList<NoteObject> notesList;
    private boolean animationEnabled = false;
    //int lastPosition=-1;
    int viewType=1;
    int currentDayValue=0;
    int kabise=0;



    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, date, body;
        public SquareImageView alarm,pass;
        public View parent;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            date = (TextView) view.findViewById(R.id.date);
            body = (TextView) view.findViewById(R.id.body);
            alarm=(SquareImageView)view.findViewById(R.id.iv_alarm);
            pass=(SquareImageView)view.findViewById(R.id.iv_pass);
            parent=(View) view.findViewById(R.id.selectedLayout);
        }
    }


    public NotesAdapter(Context context, int view_type) {
        this.context = context;
        viewType=view_type;
        SolarDate sd = SolarDate.newInstance(SolarDate.DATE_KIND_SOLAR);
        currentDayValue = sd.getDayValue();
        SQLHelper cdbo = new SQLHelper(context);

        notesList =new ArrayList<>();
        cdbo.openDb();
        notesList =cdbo.fetchAllNotesRecords();
        /*for (int i=0;i<50;i++){
            NoteObject br=new NoteObject(i,i+"-"+i,i+"-"+i);
            notesList.add(br);
        }*/
        cdbo.closeDb();

        kabise=(short) ((sd.getYear()%4==3)?1:0);


    }

    public ArrayList<NoteObject> getList(){
        return notesList;
    }

    public void setViewType(int viewType){
        this.viewType=viewType;
    }

    @Override
    public int getItemViewType(int position) {
        // Just as an example, return 0 or 2 depending on position
        // Note that unlike in ListView adapters, types don't have to be contiguous
        return viewType;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate((viewType== NotesFragment.VIEW_TYPE_LINEAR)?R.layout.note_node:R.layout.note_node, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onViewDetachedFromWindow(MyViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.itemView.clearAnimation();
    }



    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        NoteObject movie = notesList.get(position);
        holder.date.setText(movie.date_created);
        holder.title.setText(movie.toString());
        holder.body.setText(movie.body);
        holder.alarm.setVisibility(movie.hasAlarm()?View.VISIBLE:View.GONE);
        holder.pass.setVisibility(movie.isProtected()?View.VISIBLE:View.GONE);
        if (movie.getChecked())
            holder.parent.setVisibility(View.VISIBLE);
        else
            holder.parent.setVisibility(View.GONE);

        //Picasso.with(context).load(movie.profile_image).resize(256, 256).into(holder.image);

        /*if (animationEnabled){
            Animation animation = AnimationUtils.loadAnimation(context,
                    (position > lastPosition) ? R.anim.up_from_bottom
                            : R.anim.down_from_top);
            holder.itemView.startAnimation(animation);
        }

        lastPosition = position;*/

    }

    public void setAnimationEnabled(boolean _anim){
        animationEnabled=_anim;
        //Log.e("enabledanim","anim"+animationEnabled);

    }


    @Override
    public int getItemCount() {
        return notesList.size();
    }

    public void reloadFromDataBase(){
        SolarDate sd = SolarDate.newInstance(SolarDate.DATE_KIND_SOLAR);
        currentDayValue = sd.getDayValue();
        SQLHelper cdbo = new SQLHelper(context);

        notesList.clear();
        cdbo.openDb();
        notesList =cdbo.fetchAllNotesRecords();
        cdbo.closeDb();

        notifyDataSetChanged();

    }

    public  void deleteSelectedList(){
        SQLHelper cdbo = new SQLHelper(context, SQLHelper.KEY_ID);
        cdbo.openDb();
        for (int i=notesList.size()-1;i>=0;i--){
            if (notesList.get(i).getChecked()){
                cdbo.DeleteNoteRecord(notesList.get(i).note_id);
                notesList.remove(i);
            }
        }
        cdbo.closeDb();
        notifyDataSetChanged();
    }




}
