package afb.notes.studio.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.widget.RemoteViews;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import afb.elan.demo.Db.NoteObject;
import afb.elan.demo.Db.SQLHelper;
import afb.elan.demo.R;

public class NoteWidgetShow extends AppWidgetProvider {


	// Activity newactivity;

	@Override
	public void onDeleted(Context context, int[] appWidgetIds) {
		// TODO Auto-generated method stub
		super.onDeleted(context, appWidgetIds);
		SharedPreferences shp = context.getSharedPreferences("afbWidgetIDs", 0);
		
		for (int awID:appWidgetIds){
			int row_id=shp.getInt(String.valueOf(awID), -1);
			Editor e = shp.edit();
			e.remove(String.valueOf(awID));
			if (row_id!=-1) 
				e.remove("getWidgetFromNote" + String.valueOf(row_id));
			e.commit();
			
		}
		

	}

	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager,
			int[] appWidgetIds) {
		// TODO Auto-generated method stub
		super.onUpdate(context, appWidgetManager, appWidgetIds);
		// newactivity = new Activity();
		for (int i = 0; i < appWidgetIds.length; i++) {
			// int awID = appWidgetIds[appWidgetIds.length - 1];
			int awID = appWidgetIds[i];

			RemoteViews v = new RemoteViews(context.getPackageName(),
					R.layout.studio_widget_note);
			//Bundle basket = new Bundle();
			//basket.putInt(AppWidgetManager.EXTRA_APPWIDGET_ID, awID);
			//in.putExtras(basket);

			
			ContextWrapper cw = new ContextWrapper(context);
			SharedPreferences shp = cw.getSharedPreferences("afbWidgetIDs", 0);
			int noteId = shp.getInt(String.valueOf(awID), -1);
			if (noteId != -1) {
				SQLHelper sqlHelper;
				NoteObject ndr = new NoteObject();

				sqlHelper = new SQLHelper(context);
				sqlHelper.openDb();
				ndr = sqlHelper.fetchNoteRecord(noteId);
				sqlHelper.closeDb();
				
				String notebody;
				if (ndr.pass.length()>0)
					notebody="~Protected~";
				  else
				    notebody=ndr.body;
				
				v.setTextViewText(R.id.tvNoteBody2, notebody);
				v.setTextViewText(R.id.tvNoteTitle2, ndr.title);

			} else {

				v.setTextViewText(R.id.tvNoteBody2, "Not Assigned");
				//v.setTextColor(R.id.tvNoteBody2, Color.BLUE);
				v.setTextViewText(R.id.tvNoteTitle2, "...");

			}
			
			

			//Toast.makeText(context, awID + "", 1000).show();
			Intent in = new Intent(context, NoteWidgetOpen.class);

			in.setAction(awID + ""); // chon extra dar pendingintenthaye ghabli
			// harbar tavassote FLAG_UPDATE_CURRENT
			// update mishavad va meghdare ghabli az
			// beyn miravad be hamin dalil meghdare
			// awID ra dar ACTIONe intent gozashtam

			PendingIntent pi = PendingIntent.getActivity(context, 0, in,
					PendingIntent.FLAG_UPDATE_CURRENT);
			v.setOnClickPendingIntent(R.id.tvNoteBody2, pi);
			
			appWidgetManager.updateAppWidget(awID, v);

		}
	}

}
