package afb.notes.studio.widget;

import afb.elan.demo.Db.NoteObject;
import afb.elan.demo.Db.SQLHelper;
import afb.elan.demo.R;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.RemoteViews;
import android.content.ContextWrapper;

public class NoteUpdateWidget {
	Context context;
	int rowID = -1, wdigetID = -1;
	NoteObject noteObject=null;

	public NoteUpdateWidget(Context c, int Row_ID, int widget_ID, NoteObject note) {
		context = c;
		rowID = Row_ID;
		wdigetID = widget_ID;
		noteObject=note;
	}

	public void doUpdateFromNote(String eNoteBody, String tvTitle,
			boolean Password_Protected, boolean updateBody, boolean updateTitle,boolean getInfoFromDatabase) {

		ContextWrapper cw = new ContextWrapper(context);
		SharedPreferences shp = cw.getSharedPreferences("afbWidgetIDs", 0);
		int awID = shp.getInt("getWidgetFromNote" + rowID, -1);
		if (awID > -1) {
			
			if (getInfoFromDatabase){
				SQLHelper sqlHelper;

				sqlHelper = new SQLHelper(context);
				sqlHelper.openDb();
				NoteObject ndr = sqlHelper.fetchNoteRecord(rowID);
				sqlHelper.closeDb();

				tvTitle=ndr.title;
				eNoteBody=ndr.body;
				Password_Protected=(ndr.pass.length()>0);
			}
			else
			{
				tvTitle=noteObject.title;
				eNoteBody=noteObject.body;
				Password_Protected=(noteObject.pass.length()>0);

			}
			
			RemoteViews views = new RemoteViews(context.getPackageName(),
					R.layout.studio_widget_note);
			if (updateBody)
				if (!Password_Protected) {
					views.setTextViewText(R.id.tvNoteBody2, eNoteBody);
				} else
					views.setTextViewText(R.id.tvNoteBody2, "~Protected~");
			if (updateTitle)
				views.setTextViewText(R.id.tvNoteTitle2, tvTitle);
			Intent in = new Intent(context, NoteWidgetOpen.class);
			Bundle basket = new Bundle();

			basket.putInt(AppWidgetManager.EXTRA_APPWIDGET_ID, awID);
			in.putExtras(basket);
			in.setAction(awID + "");
			PendingIntent pi = PendingIntent.getActivity(context, 0, in,
					PendingIntent.FLAG_UPDATE_CURRENT);
			views.setOnClickPendingIntent(R.id.tvNoteBody2, pi);
			AppWidgetManager awm = AppWidgetManager.getInstance(context);
			awm.updateAppWidget(awID, views);
		} // if (awID>-1)
	}

}
