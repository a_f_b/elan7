package afb.notes.studio.widget;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;


import afb.elan.demo.Db.NoteObject;
import afb.elan.demo.Db.SQLHelper;
import afb.elan.demo.MainActivity;
import afb.elan.demo.R;
import afb.elan.demo.calendar.SolarDate;
import afb.elan.demo.calendar.SolarDateDialog;
import dev.shreyaspatil.MaterialDialog.BottomSheetMaterialDialog;


public class NoteWidgetOpen extends AppCompatActivity {
    EditText eNoteBody,etTitle;
    boolean editing=false;
    boolean edited=false;
    ScrollView ss;
    NoteObject editingNote=null;
    int awID = -1;
    SharedPreferences shp;
    private int noteId;
    int REQUEST_PASS = 10222324;
    boolean password_protected = false, password_Opened = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notesedit);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.rgb(110,73,32));
        }



        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);


        eNoteBody=(EditText)findViewById(R.id.eNoteBody);
        etTitle=(EditText) findViewById(R.id.etNoteTitle);
        awID = Integer.valueOf(getIntent().getAction());


        if (awID != AppWidgetManager.INVALID_APPWIDGET_ID) {
            shp = getSharedPreferences("afbWidgetIDs", 0);
            noteId = shp.getInt(String.valueOf(awID), -1);
            if (noteId != -1) {
                editing=true;
                SQLHelper sqlHelper;

                sqlHelper = new SQLHelper(this);
                sqlHelper.openDb();
                editingNote = sqlHelper.fetchNoteRecord(noteId);
                eNoteBody.setText(editingNote.body);
                etTitle.setText(editingNote.title);
                if (!editingNote.date.isEmpty()){
                    solarDate=SolarDate.newInstance(SolarDate.DATE_KIND_SOLAR);
                    solarDate.setSolarDateFromString(editingNote.date);
                    ((TextView)findViewById(R.id.tvDate)).setText(solarDate.toString());
                    ((LinearLayout)findViewById(R.id.lvDate)).setVisibility(View.VISIBLE);
                }

                sqlHelper.closeDb();
                setTitle("ویرایش یادداشت");

                /*try {
                    password_protected = (editingNote.pass.length() > 0);
                    etTitle.setText(editingNote.title);
                    if (!password_protected)
                        eNoteBody.setText(editingNote.body);
                    else {// eNoteBody.setText("~Protected~");
                        Intent i3 = new Intent("my.app.NOTESETPASS");
                        Bundle basket3 = new Bundle();
                            basket3.putString("tvText", "لطفا رمز عبور را وارد نمایید");
                        basket3.putInt("lunchMode", 2);
                        basket3.putBoolean("setPass", false);
                        basket3.putInt("R_ID", noteId);
                        basket3.putString("currentPass",
                                editingNote.pass);
                        i3.putExtras(basket3);
                        startActivityForResult(i3, REQUEST_PASS);
                    }

                } catch (Exception e) {
                    finish();
                }*/
            }
            else
            {
                editing=false;
                setTitle("یادداشت جدید");
            }
        } else
            finish();


        ss=(ScrollView)findViewById(R.id.mainscrolview);
        ss.setOnClickListener(new ScrollView.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                eNoteBody.requestFocus();
                eNoteBody.performClick();
                eNoteBody.setSelection(eNoteBody.getText().toString().length());

            }
        });

        TextWatcher tw=new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                edited=true;
            }
        };
        etTitle.addTextChangedListener(tw);
        eNoteBody.addTextChangedListener(tw);
    }


    private Menu menu = null;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // TODO Auto-generated method stub

        this.menu = menu;
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.note_edit, menu);
        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        switch(item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                break;

            case R.id.action_save:
                //setupSavingDialog(this,false,true);
                saveData(true,true);
                break;

            case R.id.action_share:
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                //sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, etTitle.getText().toString());
                sharingIntent.putExtra(Intent.EXTRA_TEXT, etTitle.getText().toString()+"\n\n"+eNoteBody.getText().toString());
                startActivity(Intent.createChooser(sharingIntent, getResources().getString(R.string.share_using)));
                break;
            case R.id.action_alarm:
                //setupSavingDialog(this,false,true);
                setAlarm();
                break;


        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {


        if (edited){

            setupSavingDialog(this,true,true);
        }
        else{
            updateList(null,true,false);
        }



    }

    private void updateList(NoteObject no, boolean needExit, boolean needRefresh){
        if (needRefresh){
            Intent intent;
            if (editing)
                intent=new Intent(MainActivity.ELAN_UPDATE_NOTE);
            else
                intent=new Intent(MainActivity.ELAN_INSERT_NOTE);
            Bundle basket=new Bundle();
            basket.putBoolean("NeedRefresh", needRefresh);
            basket.putSerializable("NoteObject",no);
            intent.putExtras(basket);
            sendBroadcast(intent);
        }
        if (needExit) finish();


    }

    private boolean saveData(final boolean exit, boolean needRefresh){

        //final Dialog dg = Utils.prepareLoadingDialog(WidgetNoteOpen.this, true);
        //dg.show();
        String body = eNoteBody.getText().toString();
        String title = etTitle.getText().toString();

        SQLHelper cdbo = new SQLHelper(
                NoteWidgetOpen.this);
        String date=(solarDate!=null)?solarDate.toString():"";

        if (editing) {

            // editmode
            editingNote.body=body;
            editingNote.title=title;
            editingNote.date=date;
            cdbo.openDb();
            cdbo.UpdateNoteRecord(editingNote);
            cdbo.closeDb();

            Toast.makeText(
                    NoteWidgetOpen.this,
                    getResources().getString(
                            R.string.recordUpdated), Toast.LENGTH_SHORT)
                    .show();
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(
                    etTitle.getWindowToken(), 0);
            updateList(editingNote,exit,needRefresh);

        } else {
            SolarDate sd = new SolarDate();
            sd.changeDateKind(SolarDate.DATE_KIND_SOLAR);
            String created = sd.toString();


            editingNote=new NoteObject();
            editingNote.setContent(-1,title,body,"",date,"",created);

            cdbo.openDb();
            cdbo.AddNoteRecoord(editingNote);
            int maxId = cdbo.getNoteMaxId();
            cdbo.closeDb();
            editingNote.note_id=maxId;


            Toast.makeText(NoteWidgetOpen.this, getResources().getString(R.string.recordAdded),
                    Toast.LENGTH_SHORT).show();
            updateList(editingNote,exit,needRefresh);
            SharedPreferences.Editor e = shp.edit();

            e.putInt(String.valueOf(awID), maxId);
            e.putInt("getWidgetFromNote" + maxId, awID);
            e.commit();


        }
        updateWidget(editingNote);
        edited=false;
        return true;
    }


    private void setupSavingDialog(Context context, boolean needExit, boolean needRefresh) {
        String message =getResources().getString(R.string.savingMeesage);
        if (editing)
            message= getResources().getString(R.string.cancelSaveDialog);

        afb.elan.MaterialDialog.BottomSheetMaterialDialog mBottomSheetDialog = new afb.elan.MaterialDialog.BottomSheetMaterialDialog.Builder(this)
                .setTitle(getResources().getString(R.string.save))
                .setMessage(message)
                .setCancelable(true)
                .setAnimation(R.raw.questionmark)
                .setPositiveButton(getResources().getString(R.string.yes), R.drawable.ic_action_yes, new BottomSheetMaterialDialog.OnClickListener() {
                    @Override
                    public void onClick(dev.shreyaspatil.MaterialDialog.interfaces.DialogInterface dialogInterface, int which) {
                        dialogInterface.dismiss();
                        saveData(needExit,needRefresh);
                    }
                })
                .setNegativeButton("صرف نظر", R.drawable.ic_action_no, new BottomSheetMaterialDialog.OnClickListener() {
                    @Override
                    public void onClick(dev.shreyaspatil.MaterialDialog.interfaces.DialogInterface dialogInterface, int which) {
                        dialogInterface.dismiss();
                        if (needExit) finish();

                    }
                })
                .build();

        // Show Dialog
        mBottomSheetDialog.show();



    }


    private void updateWidget(NoteObject note){
    	/*SharedPreferences shp= getSharedPreferences("afbWidgetIDs", 0);
    	int awID=shp.getInt("getWidgetFromNote"+editingNote.note_id, -1);
    	
		RemoteViews views = new RemoteViews(
				WidgetNoteOpen.this.getPackageName(), R.layout.widget_note);
		if (!Password_Protected) {
			views.setTextViewText(R.id.tvNoteBody2, eNoteBody.getText()
					.toString());
		} else
			views.setTextViewText(R.id.tvNoteBody2, "~Protected~");

		Intent in = new Intent(WidgetNoteOpen.this, WidgetNoteOpen.class);
		Bundle basket = new Bundle();

		basket.putInt(AppWidgetManager.EXTRA_APPWIDGET_ID, awID);
		in.putExtras(basket);
		in.setAction(awID + "");
		PendingIntent pi = PendingIntent.getActivity(WidgetNoteOpen.this, 0,
				in, PendingIntent.FLAG_UPDATE_CURRENT);
		views.setOnClickPendingIntent(R.id.tvNoteBody2, pi);
		AppWidgetManager awm = AppWidgetManager.getInstance(WidgetNoteOpen.this);
		awm.updateAppWidget(awID, views);*/

        NoteUpdateWidget uw=new NoteUpdateWidget(NoteWidgetOpen.this, note.note_id, -1,null);
        uw.doUpdateFromNote(eNoteBody.getText().toString(), etTitle.toString(),editingNote.pass.length()>0,true,true,true);
    }

    private SolarDate solarDate=null;
    private void setAlarm(){
        if (solarDate!=null){
            solarDate=null;
            ((LinearLayout)findViewById(R.id.lvDate)).setVisibility(View.GONE);
            edited=true;
            return;
        }
        SolarDateDialog sdd=new SolarDateDialog(this);
        sdd.setPersianDate((solarDate==null)?SolarDate.newInstance(SolarDate.DATE_KIND_SOLAR).toString():solarDate.toString());
        sdd.setOnSublitListener(new SolarDateDialog.onSubmitListener() {

            @Override
            public void onSubmit(String date) {
                // TODO Auto-generated method stub
                solarDate=SolarDate.newInstance(SolarDate.DATE_KIND_SOLAR);
                solarDate.setSolarDateFromString(date);
                ((TextView)findViewById(R.id.tvDate)).setText(solarDate.toString());
                ((LinearLayout)findViewById(R.id.lvDate)).setVisibility(View.VISIBLE);
                edited=true;
            }
        });
        sdd.show();

    }


}
