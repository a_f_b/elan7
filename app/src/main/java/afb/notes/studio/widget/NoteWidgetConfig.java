package afb.notes.studio.widget;

import java.util.ArrayList;


import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import afb.elan.demo.Db.NoteObject;
import afb.elan.demo.Db.SQLHelper;
import afb.elan.demo.R;
import afb.elan.demo.RecyclerTouchListener;

public class NoteWidgetConfig extends AppCompatActivity {
	
	AppWidgetManager awm;
	Context c;
	int awID;
	ArrayList<NoteObject> notesList;
	RecyclerView recyclerView;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.note_widget_config);
		Log.e("new widget","");

		c= NoteWidgetConfig.this;
		setTitle("انتخاب یادداشت");
		Intent i=getIntent();
		Bundle extras=i.getExtras();
		Log.e("extras",extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID,AppWidgetManager.INVALID_APPWIDGET_ID)+"");
		if (extras!=null)
		{
			awID=extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID,AppWidgetManager.INVALID_APPWIDGET_ID);
		}
		else
			finish();
		awm=AppWidgetManager.getInstance(c);
		notesList =new ArrayList<NoteObject>();
		SQLHelper sqlHelper=new SQLHelper(c);

		sqlHelper.openDb();
		notesList =sqlHelper.fetchAllNotesRecords();
		NoteObject note=new NoteObject();
		note.note_id=-1;
		note.title="یادداشت جدید";
		note.body="";
		notesList.add(0,note);
		sqlHelper.closeDb();
		WidgetAdapter adapter=new WidgetAdapter();
		recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
		recyclerView.setHasFixedSize(true);
		GridLayoutManager mGridLayoutManager = new GridLayoutManager(this,2, LinearLayoutManager.VERTICAL,false);
		//supportInvalidateOptionsMenu();
		recyclerView.setLayoutManager(mGridLayoutManager);

		recyclerView.setAdapter(adapter);
		recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this,recyclerView, new RecyclerTouchListener.ClickListener() {
			@Override
			public void onClick(View view, int position) {
				if (position!=0){
					NoteObject ndr= notesList.get(position);

					SharedPreferences shp;
					shp = getSharedPreferences("afbWidgetIDs", 0);

					int oldawID=shp.getInt("getWidgetFromNote" + String.valueOf(ndr.note_id), -1);
					if (oldawID!=-1){
						String text="Widget Number "+oldawID+" is exist for this note";
						text="ویجت به شماره "+oldawID+" برای این یادداشت وجود دارد ";
						Toast.makeText(c, text , Toast.LENGTH_LONG).show();
						Intent result = new Intent();
						result.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, awID);
						setResult(RESULT_CANCELED, result);
						return;

					}

					Editor e = shp.edit();

					e.putInt(String.valueOf(awID), ndr.note_id);
					e.putInt("getWidgetFromNote" + String.valueOf(ndr.note_id), awID);
					e.commit();


					RemoteViews views = new RemoteViews(c.getPackageName(), R.layout.studio_widget_note);
					views.setTextViewText(R.id.tvNoteTitle2, ndr.toString());
					if (ndr.pass.length()>0)
						views.setTextViewText(R.id.tvNoteBody2, "~Protected~");
					else
						views.setTextViewText(R.id.tvNoteBody2, ndr.body);

					Intent in = new Intent(c, NoteWidgetOpen.class);
					Bundle basket = new Bundle();

					basket.putInt(AppWidgetManager.EXTRA_APPWIDGET_ID, awID);
					in.putExtras(basket);
					in.setAction(awID + "");
					PendingIntent pi = PendingIntent.getActivity(c, 0,
							in, PendingIntent.FLAG_UPDATE_CURRENT);
					views.setOnClickPendingIntent(R.id.tvNoteBody2, pi);
					awm.updateAppWidget(awID, views);
				}



				Intent result = new Intent();
				result.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, awID);
				setResult(RESULT_OK, result);
				finish();

			}

			@Override
			public void onLongClick(View view, int position) {

			}
		}));

	}






	private class WidgetAdapter extends RecyclerView.Adapter<WidgetAdapter.Holder> {


		private boolean animationEnabled = false;
		//int lastPosition=-1;



		public class Holder extends RecyclerView.ViewHolder {
			public TextView title, body;

			public Holder(View view) {
				super(view);
				title = (TextView) view.findViewById(R.id.tvname);
				body = (TextView) view.findViewById(R.id.tvdate);
			}
		}


		public WidgetAdapter() {

		}

		public ArrayList<NoteObject> getList(){
			return notesList;
		}


		@Override
		public int getItemViewType(int position) {
			// Just as an example, return 0 or 2 depending on position
			// Note that unlike in ListView adapters, types don't have to be contiguous
			return position;
		}

		@Override
		public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
			View itemView = LayoutInflater.from(parent.getContext())
					.inflate(R.layout.grid_node, parent, false);

			return new Holder(itemView);
		}


		@Override
		public void onViewDetachedFromWindow(Holder holder) {
			super.onViewDetachedFromWindow(holder);
			holder.itemView.clearAnimation();
		}



		@Override
		public void onBindViewHolder(Holder holder, int position) {
			NoteObject movie = notesList.get(position);
			holder.title.setText(movie.title);
			//holder.body.setText(movie.body);


		}

		public void setAnimationEnabled(boolean _anim){
			animationEnabled=_anim;
			//Log.e("enabledanim","anim"+animationEnabled);

		}


		@Override
		public int getItemCount() {
			return notesList.size();
		}






	}



	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
	}









}
