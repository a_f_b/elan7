package afb.notes.studio;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;


import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.HashMap;
import java.util.LinkedHashMap;

import afb.elan.demo.Db.NoteObject;
import afb.elan.demo.MainActivity;
import afb.elan.demo.R;
import afb.elan.demo.RecyclerTouchListener;
import afb.elan.demo.ui.DatesFragment;

public class NotesFragment extends Fragment implements OnClickListener{

	//ArrayList<NoteObject> notes;
	ImageButton bAddNote;
	Button bBackup,bRestore;
	RecyclerView recyclerView;
	View loading;
	NotesAdapter notesAdapter;
	int editing_pos=-1;
	GridLayoutManager mGridLayoutManager;
	LinearLayoutManager mLayoutManager;



	public View onCreateView(@NonNull LayoutInflater inflater,
							 ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View root= inflater.inflate(R.layout.noteslist, container, false);











		//bAddNote = (ImageButton) findViewById(R.id.bAddNote);
		//bAddNote.setOnClickListener(this);

		//((RelativeLayout)findViewById(R.id.ll_main)).addView(loading);
		SharedPreferences someData = getActivity().getSharedPreferences("data", 0);
		int showMode=someData.getInt("showMode",R.id.nav_row);
		Log.e("showMode",R.id.nav_row+"="+showMode+","+R.id.nav_grid+"="+showMode);
		showMode= (showMode==R.id.nav_row)? VIEW_TYPE_LINEAR:VIEW_TYPE_GRID;

		recyclerView = (RecyclerView) root.findViewById(R.id.recycler_view);

		notesAdapter=new NotesAdapter(getActivity(),showMode);
		notesAdapter.setAnimationEnabled(false);
		//mAdapter.setHasStableIds(true);


		recyclerView.setHasFixedSize(true);
		//mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
		// = new GridLayoutManager(getActivity(),1,LinearLayoutManager.HORIZONTAL,false);
		reLoad(showMode);
		recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new RecyclerTouchListener.ClickListener() {
			@Override
			public void onClick(View view, int position) {

				if (selectedItemCount >0) {
					if (!notesAdapter.getList().get(position).getChecked()){
						selectedItems.put(notesAdapter.getList().get(position).note_id,notesAdapter.getList().get(position));
						notesAdapter.getList().get(position).setChecked(true);
						selectedItemCount++;
					}
					else
					{
						selectedItems.remove(notesAdapter.getList().get(position).note_id);
						notesAdapter.getList().get(position).setChecked(false);
						selectedItemCount--;
					}
					notesAdapter.notifyItemChanged(position);
					notifySelectModeChanged();
				} else {
					editing_pos=position;
					Intent mainintent = new Intent(getActivity(), afb.notes.studio.NoteEditor.class);
					mainintent.putExtra("mode", "edit");
					mainintent.putExtra("note", notesAdapter.getList().get(position));
					startActivity(mainintent);


				}


			}

			@Override
			public void onLongClick(View view, int position) {

				if (!notesAdapter.getList().get(position).getChecked()){
					selectedItems.put(notesAdapter.getList().get(position).note_id,notesAdapter.getList().get(position));
					notesAdapter.getList().get(position).setChecked(true);
					selectedItemCount++;
				}
				else
				{
					selectedItems.remove(notesAdapter.getList().get(position).note_id);
					notesAdapter.getList().get(position).setChecked(false);
					selectedItemCount--;
				}
				notesAdapter.notifyItemChanged(position);
				notifySelectModeChanged();


			}
		}));

		recyclerView.setAdapter(notesAdapter);

		// notesAdapter = new ArrayAdapter<String>(NotesList.this,
		// android.R.layout.simple_list_item_1, menulabels);

		/*if (lang.equals("2")) {
			bAddNote.setText("یادداشت جدید");			
		}*/

		return  root;

	}

    private Menu menu=null;

    public void hideOption(int id)
    {
        MenuItem item = menu.findItem(id);
        item.setVisible(false);
    }

    public void showOption(int id)
    {
        MenuItem item = menu.findItem(id);
        item.setVisible(true);
    }

/*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu=menu;

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.note_list, menu);

        return true;

    }
*/
	public final static int VIEW_TYPE_LINEAR=1;
	public final static int VIEW_TYPE_GRID=2;

	public void reLoad(int theme){
		notesAdapter.setAnimationEnabled(false);
		//recyclerView.showShimmerAdapter();

		switch (theme) {
			case VIEW_TYPE_GRID:
				mGridLayoutManager = new GridLayoutManager(getActivity(),2, LinearLayoutManager.VERTICAL,false);
				//supportInvalidateOptionsMenu();
				recyclerView.setLayoutManager(mGridLayoutManager);
				notesAdapter.setViewType(VIEW_TYPE_GRID);
				notesAdapter.notifyDataSetChanged();

				break;
			case VIEW_TYPE_LINEAR:
				mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
				recyclerView.setLayoutManager(mLayoutManager);
				notesAdapter.setViewType(VIEW_TYPE_LINEAR);
				notesAdapter.notifyDataSetChanged();
				break;

		}
		recyclerView.postDelayed(new Runnable() {
			@Override
			public void run() {
				//recyclerView.hideShimmerAdapter();
				//mAdapter.setAnimationEnabled(true);

			}
		},1000);
	}



    public void showLoading(){
		/*Animation anim=AnimationUtils.loadAnimation(getActivity(),
				R.anim.fade_in_simple);
		anim.setAnimationListener(new Animation.AnimationListener() {

			@Override
			public void onAnimationStart(Animation arg0) {
				// TODO Auto-generated method stub
				((ImageButton) findViewById(R.id.bAddNote)).setEnabled(false);

			}

			@Override
			public void onAnimationRepeat(Animation arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationEnd(Animation arg0) {
				// TODO Auto-generated method stub
				loading.setVisibility(ViewGroup.VISIBLE);
			}
		});
		loading.startAnimation(anim);*/
	}


	public void hideLoading(){
		/*Animation anim=AnimationUtils.loadAnimation(getActivity(),
				R.anim.fade_out_simple);
		anim.setAnimationListener(new Animation.AnimationListener() {

			@Override
			public void onAnimationStart(Animation arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationRepeat(Animation arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationEnd(Animation arg0) {
				// TODO Auto-generated method stub
				loading.setVisibility(ViewGroup.GONE);
				((ImageButton) findViewById(R.id.bAddNote)).setEnabled(true);
			}
		});
		loading.startAnimation(anim);*/
	}



	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		switch (arg0.getId()) {

		}

	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		// TODO Auto-generated method stub
		super.onCreateContextMenu(menu, v, menuInfo);

		MenuInflater mi = getActivity().getMenuInflater();
		switch (v.getId()) {
		/*case R.id.lvNotes:
			if (lang.equals("2"))
				mi.inflate(R.menu.menu_context_notelistper, menu);
			else
				mi.inflate(R.menu.menu_context_notelist, menu);
			break;*/
		}

	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item
				.getMenuInfo();

		switch (item.getItemId()) {

		}
		return super.onContextItemSelected(item);

	}


	public void refreshList(String ACTION, NoteObject no) {
		// TODO Auto-generated method stub

			if (ACTION.equals(MainActivity.ELAN_INSERT_NOTE)) {
				notesAdapter.getList().add(0,no);
				notesAdapter.notifyDataSetChanged();
			}
			else
				
			if (ACTION.equals(MainActivity.ELAN_UPDATE_NOTE)) {
				/*notesAdapter.getList().get(editing_pos).title=no.title;
				notesAdapter.getList().get(editing_pos).body=no.body;
				notesAdapter.notifyItemChanged(editing_pos);*/
				notesAdapter.reloadFromDataBase();
			}





	}

	public void reLoad(){
		notesAdapter.reloadFromDataBase();
	}


	private DatesFragment.OnNotifySelectedItemChanged notifySelectedItemChangedListener=null;
	public interface OnNotifySelectedItemChanged {
		public void notifySelectedItemChanged(int selectedCount);
	}
	public void setOnNotifySelectedItemChanged(DatesFragment.OnNotifySelectedItemChanged listener){
		notifySelectedItemChangedListener = listener;
	}

	HashMap<Integer, NoteObject> selectedItems =new LinkedHashMap<Integer,NoteObject>();
	public int selectedItemCount =0;
	private void notifySelectModeChanged(){
		notifySelectedItemChangedListener.notifySelectedItemChanged(selectedItemCount);
	}
	public void performEdit(){
		int itemId= (int) selectedItems.keySet().toArray()[0];
		Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("elanapp://afb.elan.demo/note/?id="+itemId));
		startActivity(browserIntent);
		exitEditMode();
	}

	public void performDelete(){
		notesAdapter.deleteSelectedList();
		selectedItems.clear();
		selectedItemCount=0;
		notifySelectModeChanged();

	}


	public void exitEditMode(){
		selectedItems.clear();
		selectedItemCount=0;
		for (int i=0;i<notesAdapter.getList().size();i++) notesAdapter.getList().get(i).setChecked(false);
		notesAdapter.notifyDataSetChanged();
		notifySelectModeChanged();

	}






}
